<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes V1.1
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


 Route::group(['prefix' => 'session','middleware' => 'auth:api'], function() {
 			Route::post('/list/*', 'SessionController@filterSession'); //8
 			Route::post('/list/filter/custom', 'SessionController@filterSession'); //9
 			Route::post('/list/filter/provider', 'SessionController@GetSessionAttachedToProvider'); //10
 			Route::post('/get/details', 'SessionController@GetSessionDetails'); //14
 			Route::post('/review/submit', 'ReviewController@AddReview');	//15
 			Route::post('/review/list/*', 'ReviewController@ListReviewAttachedToSession');	//16
        });

  Route::group(['prefix' => 'testing','middleware' => 'auth:api'], function() {
 		//	Route::post('/list/*', 'SessionController@filterSessionTemp'); //8
        });

 

 Route::group(['prefix' => 'city','middleware' => 'auth:api'], function() {
            Route::post('/list/*', 'CityController@ListCity'); 	//3
            Route::post('/list/district', 'DistrictController@ListDistrictAttachedToCity');	//54
        });

Route::group(['prefix' => 'sport','middleware' => 'auth:api'], function() {
            Route::post('/list/*', 'SportController@ListSport'); 	//5
            Route::post('/list/featured', 'SportController@ListSportNavBar'); 	//7
        });

Route::group(['prefix' => 'my','middleware' => 'auth:api'], function() {
             Route::post('/points', 'BalanceController@MyPoints'); 	//21

             Route::post('/sport/interest/get', 'SportController@ListSportInterest'); 	//6
             Route::post('/sport/interest/add','UserSportInterestController@AddUserInterest');	//37

             Route::post('/profile/image/upload', 'UploadController@uploadProfile');	
             Route::post('/profile/edit', 'UserDetailsController@editUserDetails');	//1
             Route::post('/profile/entity/edit', 'UserDetailsController@editEntityPassword');	//1
             

             Route::post('/id', 'UserController@GetUserID');	//4

             Route::post('/details','UserDetailsController@GetUserDetails');	//45

             Route::post('/booking/list/past','BookController@PastBookedSession');	//39
			 Route::post('/booking/list/upcoming','BookController@UpcomingBookedSession');	//40
			 Route::post('/booking/cancel','PointTransactionController@CancelSession');	//44

			 Route::post('/session/interest','SessionController@FilterSessionByUserInterest');	//41

			 Route::post('/apn/add', 'ApntokenController@AddApnToken');	//48
			 Route::post('/apn/delete', 'ApntokenController@RemoveApn');	//49

        });

Route::group(['prefix' => 'payment','middleware' => 'auth:api'], function() {
                Route::post('/token/get','FrontTransController@fortToken');	//42
        });



Route::group(['prefix' => 'plan'], function() {
			Route::post('/list','PlanController@ListPlan'); //32
			Route::post('/reserve','PointTransactionController@ReservePlan');	//33
			Route::post('/get','PlanController@GetPlanAttachedToRegPromoID');	//36
        });



Route::group(['prefix' => 'complaint','middleware' => 'auth:api'], function() {
            Route::post('/submit', 'ComplainController@AddComplain'); 	//22
        });

Route::group(['prefix' => 'entity/session','middleware' => 'auth:api'], function() {
            Route::post('/list/active', 'SessionController@GetSessionAttachedToProviderByTokan'); 	//11
            Route::post('/list/upcoming', 'SessionController@GetSessionUpComingAttachedToProviderByTokan'); 	//12
            Route::post('/list/past', 'SessionController@GetSessionUpComingAttachedToProviderByTokan'); 	//13
            Route::post('/image/upload', 'UploadController@uploadSession');

            Route::Post('/cancel','SessionController@ProviderUpdateSessionStatus');	//46
            Route::Post('/create','SessionController@AddSessionByProvider');	//51
            Route::post('/update', 'SessionController@UpdateSessionByProvider');	//57


            Route::Post('/attendant/scan','BookController@UpdateBookStatusByVerficationCode');	//52
            Route::Post('/attendant/outside/add','OutSideCustomerController@AddOutSideCustomer');	//53
            Route::post('/attendant/list/*', 'BookController@getAllParticipent');	//56



        });


Route::group(['prefix' => 'entity/review','middleware' => 'auth:api'], function() {
            Route::post('/list/*', 'ReviewProviderController@ListReviewAttachedToProviderByToken'); 	//17
            Route::post('/get','ReviewProviderController@ListReviewAttachedToProvider');	//43

        });

Route::group(['prefix' => 'entity/request','middleware' => 'auth:api'], function() {
            Route::post('/list/past', 'BookController@ListProviderPastRequestByToken'); 	//18
            Route::post('/list/upcoming', 'BookController@ListProviderUpcomingRequestByToken'); 	//19
        });

Route::group(['prefix' => 'entity/booking','middleware' => 'auth:api'], function() {
           Route::Post('/status/update','BookController@UpdateBookStatus');	//47

           


        });
Route::group(['prefix' => 'entity/location','middleware' => 'auth:api'], function() {
			Route::post('/get', 'LocationController@ListLocationAttachedToProvider'); //34
			Route::post('/list/*', 'LocationController@ListLocationAtttachedToProviderByToken');	//55

			
        });

Route::group(['prefix' => 'entity/details','middleware' => 'auth:api'], function() {
			Route::post('/get', 'ProviderController@GetProviderByID');	//35
			
        });

Route::group(['prefix' => 'rule'], function() {
			Route::post('/cancellation/list/*', 'CancellationTypeController@ListCancellationRule');	//50
        });    


Route::group(['prefix' => 'settings'], function() {
           Route::post('/registeration/status','SettingController@IsRegPromoCodeActive');	//38

        });         

Route::group(['prefix' => 'validate'], function() {
            Route::post('/code/invitation', 'ValidationController@CheckInvitationCodeAvailability'); 	//23
            Route::post('/code/promo', 'PromoCodeController@CheckPromoCode'); 	//2
            Route::post('/phone', 'ValidationController@CheckPhoneAvailability'); 	//24
            Route::post('/code/registeration','ValidationController@CheckRegisterCodeAvailability');	//25
			Route::post('/email','ValidationController@CheckEmailAvailability');	//26
			Route::post('/email-invitation','ValidationController@CheckEmailAndInviationCode');	//27
			Route::post('/reset/token','ResetPasswordController@CheckValidation'); //28
        });

Route::group(['prefix' => 'user'], function() {
           Route::post('/password/reset', 'ResetPasswordController@AddResetPassword');	//29
           Route::post('/login', 'UserController@login');	//30	
		   Route::post('/signup','UserController@register');	//31
        });







Route::post('/server/cache/clear', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('view:clear');
    print('Cache Cleared');
});



/*
|--------------------------------------------------------------------------
| API Routes V1.0
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('checkInvitationCode','ValidationController@CheckInvitationCodeAvailability'); //23
Route::post('checkPhone','ValidationController@CheckPhoneAvailability');  //24
Route::post('checkRegisterPromo','ValidationController@CheckRegisterCodeAvailability'); //25


Route::post('checkEmail','ValidationController@CheckEmailAvailability'); //26
Route::post('checkEmailAndInvitationCode','ValidationController@CheckEmailAndInviationCode');	//27
Route::post('/resetPassword', 'ResetPasswordController@AddResetPassword');	//29
Route::post('/checkTokenForResetPassword','ResetPasswordController@CheckValidation');	//28

Route::post('/changePassword','ResetPasswordController@ChangePassword');	//29




Route::post('login', 'UserController@login');	//30
Route::post('register','UserController@register');	//31
//Route::post('getPlans','RegisterPromoController@GetPlanAttachedToRegPromo');
Route::post('/getPlans','PlanController@ListPlan');	//32
Route::post('/isRegPromoCodeActive','SettingController@IsRegPromoCodeActive');	//38
Route::get('/checkValidationToken','ResetPasswordController@CheckValidation');	//28
 
/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('/reservePlan','PointTransactionController@ReservePlan');	//33

Route::post('/donotHavePromoCode','PromoCodeController@DonotHavePromoCode');

Route::post('clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('view:clear');
    print('Cache Cleared');
    // return what you want
});
Route::post('generateReceipt','ReceiptController@GenerateReceipt');

Route::post('revokedReceipt','ReceiptController@RevokedReceipt');

 


Route::group(['middleware' => 'auth:api'], function(){
	Route::post('/editUserProfile', 'UserDetailsController@EditUserProfile');	//1
	Route::post('/checkPromoCode', 'PromoCodeController@CheckPromoCode');	//2
	Route::post('/listCity', 'CityController@ListCity');	//3
	Route::post('/getID', 'UserController@GetUserID'); //4
	Route::post('/listSport', 'SportController@ListSport'); //5
	Route::post('/listSportinterest', 'SportController@ListSportInterest'); //6
	Route::post('/listSportnavbar', 'SportController@ListSportNavBar'); //7
	
	Route::post('/listLocation', 'LocationController@ListLocation');
	Route::post('/listLocationAttachedToProvider', 'LocationController@ListLocationAttachedToProvider'); //34

	Route::post('/listProvider', 'ProviderController@listProvider');
	Route::post('/getProviderDetails', 'ProviderController@GetProviderByID'); //35


	
	Route::post('/listSession', 'SessionController@listSession'); //8
	Route::post('/filterSession', 'SessionController@filterSession'); //9
	Route::post('/listSessionAttachedToProvider', 'SessionController@GetSessionAttachedToProvider'); //10
	Route::post('/listSessionAttachedToProviderByToken', 'SessionController@GetSessionAttachedToProviderByTokan'); //11
	Route::post('/listUpcomingSessionAttachedToProviderByToken', 'SessionController@GetSessionUpComingAttachedToProviderByTokan'); //12
	Route::post('/listPastSessionAttachedToProviderByToken', 'SessionController@GetSessionPastAttachedToProviderByTokan'); //13
	Route::post('/getSessionDetails','SessionController@GetSessionDetails');	//14

	Route::post('/addPointTransaction','PointTransactionController@RegPromo'); 

	Route::post('/addReview','ReviewController@AddReview'); //15
	Route::post('/listReviewAttachedToSession','ReviewController@ListReviewAttachedToSession'); //16
	Route::post('/listReviewAttachedToProviderByToken','ReviewProviderController@ListReviewAttachedToProviderByToken'); //17

	Route::post('/listProviderPastRequestByToken','BookController@ListProviderPastRequestByToken');	//18
	Route::post('/listProviderUpcomingRequestByToken','BookController@ListProviderUpcomingRequestByToken');	//19
	
	Route::post('/bookSession','BookController@BookSession');	//20

	Route::post('/myPoints','BalanceController@MyPoints'); //21
	Route::post('/getNumberofBookSession','BookController@getTotalBookPerSession');

	Route::post('/getAvailbleSlots','SessionController@getAvailbleSlots'); 

	Route::post('/submitComplain','ComplainController@AddComplain'); //22


	//Route::post('/getPlanAttachedToRegPromo','PlanController@PlanAttachedToRegPromoID');

	Route::post('/getPlanAttachedToRegPromo','PlanController@GetPlanAttachedToRegPromoID');	//36


	Route::post('/addUserSportInterest','UserSportInterestController@AddUserInterest');	//37
	Route::post('/isRegisterPromoCodeActive','SettingController@IsRegPromoCodeActive');	//38

	Route::post('/listPastBookedSession','BookController@PastBookedSession');	//39
	Route::post('/listUpcomingBookedSession','BookController@UpcomingBookedSession');	//40
	Route::post('/listReviewAttachedToProvider','ReviewProviderController@ListReviewAttachedToProvider');	//43
	Route::post('/filterSessionByUserInterest','SessionController@FilterSessionByUserInterest');	//41

	

	Route::post('/Gift','PointTransactionController@Gift');
	Route::post('/cancelSession','PointTransactionController@CancelSession');	//44
	Route::post('/getUserDetails','UserDetailsController@GetUserDetails');	//45
	Route::Post('/updateBookStatus','BookController@UpdateBookStatus');	//47
	Route::Post('/cancelSessionByProvider','SessionController@ProviderUpdateSessionStatus');	//46
	Route::Post('/updateBookStatusByVerficationCode','BookController@UpdateBookStatusByVerficationCode');	//52
	Route::Post('/addSessionByProvider','SessionController@AddSessionByProvider');	//51
	Route::Post('/addOutSideCustomerByProvider','OutSideCustomerController@AddOutSideCustomer');	//53
	Route::post('/listDistrictAttachedToCity', 'DistrictController@ListDistrictAttachedToCity');	//54
	Route::post('/listLocationAttachedToProviderByToken', 'LocationController@ListLocationAtttachedToProviderByToken');//55
	
	Route::post('/listCancellationRule', 'CancellationTypeController@ListCancellationRule');	//50
	Route::post('/getAllParticipent', 'BookController@getAllParticipent');	//56
	Route::post('/updateSessionByProvider', 'SessionController@UpdateSessionByProvider');	//57
	
	Route::post('/apnToken', 'ApntokenController@AddApnToken');	//48
	Route::post('/removeApn', 'ApntokenController@RemoveApn');	//49

	Route::post('/notifiBook', 'NotifiController@AddNotifi');

		

    
});
    Route::post('/fortToken','FrontTransController@fortToken');	//42
    Route::group(['prefix' => 'voucher','middleware' => 'auth:api'], function() {
        Route::post('/apply','VoucherController@ApplyVoucher');
        
    });
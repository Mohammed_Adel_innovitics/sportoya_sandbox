@extends('voyager::master')

@section('css')

    @include('voyager::compass.includes.styles')
    
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-ticket"></i>
        <p> Bookings</p>
           </h1>
        
            @include('partials.bulk-generate-receipt')

           
@stop
@section('content')
 <div class="col-md-12">
<div class="panel panel-bordered">
<div class="panel-body">
<div class="table-responsive">
    <table id="dataTable" class="table table-hover">
        <thead>
            <tr>
               <th></th>
<!--                <th>bookID</th> -->
<!--                <th>User ID</th> -->
               <th>User Name</th>
               <th>Session Name</th>
               <th>Provider Name</th>
               <th>Session Date</th>
               <th>Session Time</th>
               <th>Session Cost</th>
               <th>Entity Payment</th>
               <th>Book status</th>
               <th>Promocode</th>
            </tr>
        </thead>
        <tbody>
            @foreach($arrBooked as $data)
            
            <tr>
                <td>
                    <input type="checkbox" name="row_id" id="checkbox_{{ $data->pkBookID}}" value="{{ $data->pkBookID }}">
                </td>
<!--            		<td> -->
<!--            			{{$data->pkBookID}} -->
<!--            		</td> -->
<!--            		<td> -->
<!--            			{{$data->user->user_id}} -->
<!--            		</td> -->
           		<td>
           			{{$data->user->fldfullname}}
           		</td>
           		<td>
           			{{$data->session->fldsessionname}}
           		</td>
           		<td>
           			{{$provider->fldprovidername}}
           		</td>
           		<td>
           			{{$data->session->fldsessiondate}}
           		</td>
           		<td>
           			{{$data->session->fldstarttime}}
           		</td>
           		<td>
           			{{$data->session->fldsessioncost}}
           		</td>
           		<td>
           			{{$data->provider_payment}}
           		</td>
           		<td>
           			{{$data->BookStatus->fldbookstatusname}}
           		</td>
           		<td>
           			{{$data->promocode}}
           		</td>
            </tr>
         	@endforeach
        </tbody>
	</table>
</div>
</div>
</div>
<?php $isServerSide = true?>

	@if ($isServerSide)
            <div class="pull-left">
                <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
                    'voyager.generic.showing_entries', $arrBooked->total(), [
                        'from' => $arrBooked->firstItem(),
                        'to' => $arrBooked->lastItem(),
                        'all' => $arrBooked->total()
                    ]) }}</div>
            </div>
            <div class="pull-right">
                {{ $arrBooked->appends([
                                                        'intProviderID' => $provider->id,
                    
                ])->links() }}
            </div>
        @endif
        
@stop
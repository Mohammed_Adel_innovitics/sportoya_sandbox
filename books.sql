/*
Navicat MySQL Data Transfer

Source Server         : sportoyasb
Source Server Version : 50505
Source Host           : 104.152.168.37:3306
Source Database       : sandbox_sportoya

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-05-22 15:10:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for books
-- ----------------------------
DROP TABLE IF EXISTS `books`;
CREATE TABLE `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fldpointcost` int(11) DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `bookStatus_id` int(11) DEFAULT NULL,
  `pointTransaction_id` longtext,
  `fldverificationcode` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `provider_payment` double(11,2) DEFAULT NULL,
  `complain_id` int(11) DEFAULT NULL,
  `receipt_id` int(11) DEFAULT NULL,
  `sportoya_payment` double(11,2) DEFAULT NULL,
  `book_type` varchar(255) DEFAULT 'Pass',
  `fldcashcost` int(11) DEFAULT '0',
  `cashTransaction_id` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PhpParser\CodeTestParser;
use Illuminate\Http\Request;


class Voucher extends Model
{
    protected $fillable = [
        'id','text', 'usage','revoked','points'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'created_at','deleted_at'
    ];
    
    public function checkVoucherLimit($voucherText){
        $voucherObj = $this->where('text',$voucherText)->where('revoked',0)->first();
        $objCode = new CodeTransaction();
        $noofusers = $objCode->where('fldcodetransactionType','=','Voucher')->where('code_id',$voucherObj['id'])->count();
//         $voucher = $this->where('text',$voucherText)->where('revoked','=','0')->where('usage','<=',$noofusers)->first();
        if($voucherObj['usage'] > $noofusers){
            return 'true' ; 
        }else{
            return 'false' ; 
        }
    }
    
    public function checkUserAvailability($userId,$voucherText){
        $objCode = new CodeTransaction();
        $res = $objCode->where('fldcodetransactionType','=','Voucher')->where('user_id',$userId)->first();
        if(!$res){
            return 'true' ; 
        }else{
            return 'false' ;
        }
    }
    
    public function checkVoucherExpiry($voucherText){
        $voucherObj = $this->where('text',$voucherText)->where('revoked',0)->first();
        if($voucherObj){
            return 'true';
        }else{
            return 'false';
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Cashtransaction extends Model
{
    protected $table = 'cashtransactions';
    protected $fillable = ['id','amount','user_id','transactiontype_id','note','merchant_reference'];
    protected $hidden = ['created_at','updated_at','deleted_at'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Gallery extends Model
{
     protected $table = 'galleries';

	protected $fillable = ['id','fldimg','fkItemID','fkTypeID','updated_at','created_at'];

	
	public function listGallery($intItemID,$intTypeID){
    	$arrGallery = Note::select('id','fldimg','fkItemID','fkTypeID','updated_at','created_at')->where('fkItemID',$intItemID)->where('fkTypeID',$intTypeID)->get();

    	return $arrGallery;
    }

    public function deleteGallery($intItemID,$intTypeID){
    	$objGallery = DB::table('tbl_gallery')->where('fkItemID',$intItemID)->where('fkTypeID',$intTypeID)->delete();
    	return $objGallery;
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Phone extends Model
{
    protected $table = 'phones';

    protected $fillable = ['id','fldnumbertype','fldphonenumber','user_id'];

    public function checkIsNewPhone($intPhoneNumber){
    	
    	$objPhone = $this->where('fldphonenumber',$intPhoneNumber)->get();
    	//dd(count($objPhone));
    	if(count($objPhone)>0){
    		return false;
    	}
    	return true;
    }

public function getUserPhoneNumber($intUserID)
{
            $objPhone = Phone::select('fldphonenumber')->where('user_id',$intUserID)->where('fldnumbertype','phoneNumber')->first();
            if($objPhone != NULL)
                { return $objPhone['fldphonenumber']; }
            else
                { return false; }

}

public function checkIfPhoneBelongsToUser($intPhoneNumber,$intUserID){
        
        $objPhone = $this->where('fldphonenumber',$intPhoneNumber)->where('user_id',$intUserID)->get();
        //dd(count($objPhone));
        if(count($objPhone)>0){
            return true;
        }
        return false;
    }

 public function checkIsPhoneExsit($intUserID){
        
        $objPhone = $this->where('fldnumbertype','emergancyNumber')->where('user_id',$intUserID)->first();
//dd($objPhone);        
//dd(count($objPhone));
        if(count($objPhone)>0){
            return false;
        }
        return true;
    }


    public function updatePhoneEmergency($intPhoneNumber,$intUserID){
      $objPhone =  DB::table('phones')->where('user_id', $intUserID)->where('fldnumbertype','emergancyNumber')->update(['fldphonenumber' => $intPhoneNumber]);
        //dd($objPhone);
        return $objPhone;      
    }

    public function updatePhone($intPhoneNumber,$intUserID){
      $objPhone =  DB::table('phones')->where('user_id', $intUserID)->where('fldnumbertype','phoneNumber')->update(['fldphonenumber' => $intPhoneNumber]);
        //dd($objPhone);
        return $objPhone;      
    }

 public function deleteEmergPhone($intUserID){
        $objPhone = $this->where('fldnumbertype','emergancyNumber')->where('user_id',$intUserID)->delete();
        return $objPhone;

    }
  public function deletePhone($intUserID){
        $objPhone = $this->where('fldnumbertype','phoneNumber')->where('user_id',$intUserID)->delete();
        return $objPhone;
  }  

}

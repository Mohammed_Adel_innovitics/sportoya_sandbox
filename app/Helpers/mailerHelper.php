<?php

namespace App\Helpers;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Mail;
class mailerHelper 
{

    static function MailerSender($emailTitle,$bodyMessage,$userEmail,$userName,$companyEmail,$obj,$type){
          
        $arr = array();
            $data = array('userName'=>$userName,'usermail'=>$userEmail,'Bodymessage'=>$bodyMessage,'type'=>$type,'obj'=>$obj);
            Mail::send('mail', $data, function($message) use ($userEmail,$userName,$companyEmail,$emailTitle) {
                $message->to($userEmail,$userName)->subject($emailTitle);
                $message->from($companyEmail);
            });
                //dd($userEmail);  
                return true;           
    }
    
}    
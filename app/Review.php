<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'reviews';

    protected $fillable = ['id','fldreviewrate','fldreviewtext','user_id','session_id','created_at','updated_at'];

    public function listReviewAttachedToSession(){
    	$arrReview = Review::select('id','fldreviewrate','fldreviewtext','user_id','session_id','created_at','updated_at')->where('session_id')->with('user')->with('session')->pagination(10);
        return $arrReview;
    }

    public function getReviewAttachedToProvider($arrSession){
    	$arrReview = Review::whereIn('session_id', $arrSession)->select('id','fldreviewrate','fldreviewtext','user_id','session_id','created_at','updated_at')->paginate(10);
        return $arrReview;
    }
    public function getUserReviewAttachedToSession($intSessionID,$intUserID,$intBookID){
        $objReview = Review::select('id','fldreviewrate','fldreviewtext','user_id','session_id','created_at','updated_at')->where('session_id' , $intSessionID)->where('user_id',$intUserID)->where('book_id',$intBookID)->count();

        if($objReview > 0)
        {
            return "1";
        }
        
        return "0";
    }


    

    public function user(){
    	return $this->belongsTo('App\User','user_id','id');
    }
    public function sesseion(){
    	return $this->belongsTo('App\Session','session_id','id');
    }
}

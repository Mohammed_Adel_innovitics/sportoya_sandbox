<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CancellationType extends Model
{
    protected $table = 'cancellationtypes';

    protected $fillable = ['id','fldcancellationname','fldcancellationcolor','fldmaxtime','flddescription','fldpointsdeducted','created_at','updated_at'];


    public function listCancellationType(){
        $arrCancellationType = $this->get();
        return $arrCancellationType;
    }


    public function loadCancellationTypeByID($intCancellationTypeID){
        $objCancellationType = $this->where('id',$intCancellationTypeID)->get();
        return $objCancellationType;
    }
    public function getCancellationTypeID($strCancellationName){
        $objCancellationType = CancellationRule::select('id')->where('fldcancellationname',$strCancellationName)->first();

        return  $objCancellationType['id'];
    }

    public function  session(){
         return $this->hasMany('App\Session');
    }

}

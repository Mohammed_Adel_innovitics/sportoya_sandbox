<?php

namespace App;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\PointTransactionController;
use Illuminate\Support\Facades\DB;
use App\BookStatus;
use App\Session;
use App\Review;
use App\Provider;
use App\PointTransaction;
use DateTime;
use App\Mailer;
use Exception;
use Carbon\Carbon;
use App\SMSGateway;
use App\Phone;


class Book extends Model
{
    protected $table = 'books';
    protected $fillable = ['id','fldpointcost','session_id','user_id','bookStatus_id','pointTransaction_id','fldverificationcode','updated_at','created_at','provider_payment','complain_id','receipt_id','sportoya_payment','fldcashcost','cashTransaction_id','book_type'];

    
    public function getBookAttacehedtoReceiptTotal($intReceiptID){
        $total = DB::table('books')
        ->leftJoin('sessions', 'books.session_id', '=', 'sessions.id')
        ->select('provider_payment')
        ->where('receipt_id',$intReceiptID)
        
        ->sum('books.provider_payment');
        return  $total;
    }
    
    
    public function getBookAttacehedtoReceipt($intReceiptID){
        $objBookStatus = new BookStatus();
        $objReview = new Review();
        $objSession = new Session();
        $currentDateTime = date('Y-m-d');
        
        
        $arrPastBookedSession = DB::table('books')
        ->leftJoin('sessions', 'books.session_id', '=', 'sessions.id')
        ->select('books.id as pkBookID','pointTransaction_id as fkTransactionID','fldverificationcode as fldVerificationCode','sessions.id as pkSessionID','user_id as fkUserID','bookStatus_id as fkBookStatusID','images','provider_payment')
        ->where('receipt_id',$intReceiptID)
        ->orderBy('books.created_at', 'Desc')
        ->get();
        //dd($arrPastBookedSession);
        foreach ($arrPastBookedSession as $obj) {
            $objProvider = new Provider();
            $objBookStatus = new BookStatus();
            $objUser = new UserDetails();
            $objCancellationType = new CancellationType();
            $obj->session = $objSession->getSession($obj->pkSessionID);
            $obj->CancellationType = $objCancellationType->loadCancellationTypeByID($obj->session->cancellationType_id);
            $obj->user = $objUser->getUserDetailsByID($obj->fkUserID);
//             $date = new DateTime($objSession->fldsessiondate);
            
//             $obj->session->fldsessiondate = $date->format("D,j M Y");
//             $obj->session->fldstarttime = $date->format("H:i");
            //$this->fkBookStatusID  = $obj->fkBookStatusID;
            $obj->providerName = $objProvider->getProvider($obj->session->location_id)['fldprovidername'];
//             dd($obj->session->providerName);
            $obj->session->booking = $this->userBookedSession($obj->fkUserID,$obj->fldVerificationCode);
            $obj->BookStatus = $objBookStatus->getBookStatusByID($obj->fkBookStatusID);
            //dd($obj->BookSatus);
            unset($obj->fldVerificationCode);
            
        }
        return $arrPastBookedSession;
    }
    public function recepitsCreated($arrBookIDs,$ReceiptID){
        //dd($ReceiptID);
        $objBook = DB::table('books')->whereIn('id', $arrBookIDs)
        ->update(['receipt_id' => $ReceiptID]);
        //dd($objBook);
        return $objBook;
    }
    public function revokedReceiptInBook($arrBookIDs){
        //dd($arrBookIDs);
        $objBook = DB::table('books')->whereIn('id', $arrBookIDs)
        ->update(['receipt_id' => null]);
        //dd($objBook);
        return $objBook;
    }
    public function AllBookWithBookedStatus($intSessionID){
        $arrBook = Book::select('id','fldpointcost','session_id','user_id','bookStatus_id','pointTransaction_id','fldverificationcode','updated_at','created_at')->with('userDetails')->with('Phone')->with('BookStatus')->where('session_id',$intSessionID)->whereIn('bookStatus_id',[2,3])->get();
        
        return $arrBook;
    }
    
    public function getLastBook($intUserID){
        $objBook = $this->where('user_id',$intUserID)->whereIn('bookStatus_id',[4])->latest()->get();
        if(count($objBook)>0){
            $objSession = DB::table('sessions')->where('id',$objBook[0]->session_id)->get();
            return $objSession;
        }else{
            return null;
        }
        
    }
    
    public function AddTobook($arrTransactionIDs,$intSessionID,$intPoints,$IsAutoBook){
        $objUser = new UserController();
        $objSession = new Session();
        $session = $objSession->getSession($intSessionID);
        //$objSession = new Session();
        //$Session = $objSession->getSession();
        $this->fldverificationcode = str_random(4).$arrTransactionIDs[0];
        $this->pointTransaction_id = json_encode($arrTransactionIDs);
        //dd($IsAutoBook);
        if($IsAutoBook == 1){
            
            $this->bookStatus_id = 2;
            $this->sportoya_payment = (0.3*$session->fldsessioncost)+$session->sportoyaSessionMargin;
            $this->provider_payment = (0.7*$session->fldsessioncost);
            
        }else if($IsAutoBook == 0){
            $this->bookStatus_id = 1;
        }
        $this->user_id = $objUser->GetUserID();
        $this->session_id = $intSessionID;
        $this->fldpointcost = $intPoints;
        $result = $this->save();
        
        ///
        
        $Book = $this->getBook($this->id);
        
        $objLocation = new Location();
        $Provider = $objLocation->getProvider($session->location_id);
        $user = new User();
        $UserData = $user->getUser($objUser->GetUserID());
        $session['book'] = $Book;
        
        $mailer = new Mailer();

        //SEND SMS TO PROVIDER
        $SMSContent = 'NEW BOOKING! Please, check Sportoya business app to find your booking';

        $SMS = new SMSGateway();
        $Phone = new Phone();
       
        $phoneNumber = $Phone->getUserPhoneNumber($Provider->user_id);

        if($phoneNumber != false)
        {
            $SMS->send($phoneNumber,$SMSContent);
        }
        //--------------------
        
        if($IsAutoBook == 1){
            
            
            $result = $mailer->send('customer-care@sportoya.com',$UserData['email'],'9151970',[
                "product_name" => "Sportoya",
                "name" => $UserData['name'],
                "session_name" => $session->fldsessionname,
                "provider_name" => $Provider->fldprovidername,
                "confirmation_code" => $this->fldverificationcode,
                "support_url" => "mailto:customer-care@sportoya.com"
            ]);
            
        }
        
        $resultRep = $mailer->send('customer-care@sportoya.com','customer-care@sportoya.com','9151970',[
            "product_name" => "Sportoya",
            "name" => $UserData['name'],
            "session_name" => $session->fldsessionname,
            "provider_name" => $Provider->fldprovidername,
            "confirmation_code" => $this->fldverificationcode,
            "support_url" => "mailto:customer-care@sportoya.com"
        ]);
        
        if($result){
            return true;
        }else{
            return false;
        }
        
        
    }
    
    public function getBook($intBookID){
        $objBook = $this->Where('id',$intBookID)->first();
        return $objBook;
    }
    public function loadBookByID($intBookID){
        $objBook = Book::select('pointTransaction_id')->Where('id',$intBookID)->first();
        return $objBook;
        
    }
    public function updateBookStatus($intBookID,$intBookStatusID,$Session){
        if($intBookStatusID == 2){
            $sportoya_payment = (0.3*$Session->fldsessioncost)+$Session->sportoyaSessionMargin;
            $provider_paymnet = ($Session->fldsessioncost*0.7);
            $objBook = DB::table('books')->where('id', $intBookID)
            ->update(['bookStatus_id' => $intBookStatusID,'provider_payment'=>$provider_paymnet,'sportoya_payment'=>$sportoya_payment]);
            return $objBook;
        }else if($intBookStatusID == 3){
            
            $sportoya_payment = $Session->sportoyaSessionMargin;
            $provider_paymnet = $Session->fldsessioncost;
            $objBook = DB::table('books')->where('id', $intBookID)
            ->update(['bookStatus_id' => $intBookStatusID,'provider_payment'=>$provider_paymnet,'sportoya_payment'=>$sportoya_payment]);
            return $objBook;
        }else if($intBookStatusID == 4){
            //check cancellation rule
            $objSession = new Session();
            $objSessionWithCancellation = $objSession->getSessionWithCancellation($Session->id);
            
            $mydate = date('Y-m-d H:i:s');
            
            /*$daystosum = $intExpiryDays;
             $datesum = date('Y-m-d', strtotime($mydate.' + '.$daystosum.' days'));       */
            
            $orig_timezone = new \DateTimeZone('UTC');
            
            $schedule_date = new \DateTime($mydate,$orig_timezone);
            $schedule_date->setTimeZone(new \DateTimeZone('Africa/Cairo'));
            $CurrentDate =  $schedule_date->format('Y-m-d H:i:s');
            
            $equation = date('Y-m-d', strtotime($objSessionWithCancellation->fldsessiondate.' - '.$objSessionWithCancellation['cancellationType']->fldmaxtime.' days'));
            
//             dd($equation.$CurrentDate);
            if( $CurrentDate>$equation){
                if($objSessionWithCancellation['cancellationType']->fldcancellationname == 'Strict'){
                    $sportoya_payment = $Session->sportoyaSessionMargin + (0.3*$Session->fldsessioncost);
                    $provider_paymnet = 0.7*$Session->fldsessioncost;
                    $objBook = DB::table('books')->where('id', $intBookID)
                    ->update(['bookStatus_id' => $intBookStatusID,'provider_payment'=>$provider_paymnet,'sportoya_payment'=>$sportoya_payment]);
                    return $objBook;
                }else if($objSessionWithCancellation['cancellationType']->fldcancellationname == 'Flexible'){
                    $SessionCancelRule = $Session->fldsessioncost * ($objSessionWithCancellation['cancellationType']->fldpointsdeducted)/100;
                    $sportoya_payment = ($Session->sportoyaSessionMargin*0.2) + (0.3*($SessionCancelRule));
                    $provider_paymnet = 0.7*$SessionCancelRule;
                    // dd($sportoya_payment);
                    $objBook = DB::table('books')->where('id', $intBookID)
                    ->update(['bookStatus_id' => $intBookStatusID,'provider_payment'=>$provider_paymnet,'sportoya_payment'=>$sportoya_payment]);
                    return $objBook;
                }
                
            }else{
                $sportoya_payment = 0;
                $provider_paymnet = 0;
                
                $objBook = DB::table('books')->where('id', $intBookID)
                ->update(['bookStatus_id' => $intBookStatusID,'provider_payment'=>$provider_paymnet,'sportoya_payment'=>$sportoya_payment]);
                return $objBook;
            }
        }else{
            $objBook = DB::table('books')->where('id', $intBookID)
            ->update(['bookStatus_id' => $intBookStatusID]);
            return $objBook;
        }
        
    }
    public function allBookAttachedToSession($intSessionID){
        $arrBookIDs = DB::table('books')->where('session_id',$intSessionID)->pluck('id');
        
        return $arrBookIDs;
        
    }
    public function allTransAttachedBooksSession($intSessionID){
        $arrTransIDs2 = array();
        $arrTransIDs = Book::where('session_id',$intSessionID)->select('pointTransaction_id')->get();
        //dd($arrTransIDs);
        foreach($arrTransIDs as $obj){
            $arrTransIDs2[]=json_decode($obj->pointTransaction_id);
        }
        return $arrTransIDs2;
        
    }
    public function updateBookStatusByVerficationCode($strverficationCode,$SessionPrice,$margin){
        $objBook = Book::where('fldverificationcode',$strverficationCode)->update(['bookStatus_id'=>3,'provider_payment'=>$SessionPrice,'sportoya_payment'=>$margin]);
        //dd($objBook);
        return $objBook;
    }
    public function getBookByVerficationCode($intSessionID,$strverficationCode){
        $objBook = Book::where('fldverificationcode',$strverficationCode)->where('session_id',$intSessionID)->first();
        if(!$objBook){
            return false;
        }
        return $objBook;
    }
    
    public function updateBookedSessionToCancel($arrBookIDs){
        
        $objBook = Book::whereIn('id',$arrBookIDs)->update(['bookStatus_id'=>4]);
        return $objBook;
    }
    public function getTransactionID($intBookID){
        $intTransactionID = Book::select('pointTransaction_id')->Where('id',$intBookID)->first();
        
        return $intTransactionID;
        
    }
    
    public function getTotalBookedSessions($intSessionID){
        $objBookStatus = new BookStatus();
        
        
        $arrBook = Book::select('id','fldpointcost','session_id','user_id','bookStatus_id','pointTransaction_id','fldverificationcode','updated_at','created_at')->where('session_id',$intSessionID)->whereNotIn('bookStatus_id',[6,5,4])->get();
        //  dd($arrBook);
        
        return $arrBook;
    }
    
    public function userIsBookedThisSession($intUserID,$intSessionID){
        $objBook = Book::select('id','fldpointcost','session_id','user_id','bookStatus_id','pointTransaction_id','fldverificationcode','updated_at','created_at')->where('session_id',$intSessionID)->where('user_id',$intUserID)->first();
        //dd($objBook);
        if($objBook != null){
            return true;
        }else{
            return false;
        }
    }
    
    public function userBookedSession($intUserID,$fldVerificationCode){
        $objBook = Book::select('id','fldpointcost','session_id','user_id','bookStatus_id','pointTransaction_id','fldverificationcode','updated_at','created_at')->where('fldverificationcode',$fldVerificationCode)->where('user_id',$intUserID)->first();
        //dd($objBook);
        if($objBook != null){
            return $objBook;
        }else{
            return false;
        }
    }
    
    public function getPastBookedSession($intUserID){
        $objBookStatus = new BookStatus();
        $objReview = new Review();
        $objSession = new Session();
        $currentDateTime = date('Y-m-d');
        
        
        $arrPastBookedSession = DB::table('books')
        ->leftJoin('sessions', 'books.session_id', '=', 'sessions.id')
        ->select('books.book_type as pkBookType','books.id as pkBookID','pointTransaction_id as fkTransactionID','fldverificationcode as fldVerificationCode','sessions.id as pkSessionID','user_id as fkUserID','bookStatus_id as fkBookStatusID','location_id as fkLocationID')
        ->where('user_id',$intUserID)
        ->whereIn('fldStatus',['Live','Cancelled'])
        ->whereIn('bookStatus_id',[$objBookStatus->getBookStatusID('cancelled'),$objBookStatus->getBookStatusID('attended'),$objBookStatus->getBookStatusID('PROVIDERCANCELLATION')])
        ->orderBy('books.created_at', 'Desc')
        ->paginate(10);
        //dd($arrPastBookedSession);
        foreach ($arrPastBookedSession as $obj) {
            $objBookStatus = new BookStatus();
            
            $objProvider = new Provider();
            
            $obj->session = $objSession->getSession($obj->pkSessionID);
            $obj->provider12 = '';
            
            $obj->provider12 = $objProvider->getProvider($obj->fkLocationID);
            unset($objSession->provider);
            
            $obj->session->provider = $obj->provider12;
            unset($obj->provider12);
            
            $obj->IsUserReview = $objReview->getUserReviewAttachedToSession($obj->pkSessionID,$obj->fkUserID,$obj->pkBookID);
            
            $obj->session->booking = $this->userBookedSession($intUserID,$obj->fldVerificationCode);
            $obj->session->booking->BookStatus = $objBookStatus->getBookStatusByID($obj->fkBookStatusID);
            
            unset($obj->fldVerificationCode);
            
        }
        //dd($arrPastBookedSession);
        return $arrPastBookedSession;
    }
    
    public function getPastBookedSessionAttachedToProvider($arrSessionIDs){
        $objBookStatus = new BookStatus();
        $objReview = new Review();
        $objSession = new Session();
        $currentDateTime = date('Y-m-d');
        
        
        $arrPastBookedSession = DB::table('books')
        ->leftJoin('sessions', 'books.session_id', '=', 'sessions.id')
        ->select('books.id as pkBookID','pointTransaction_id as fkTransactionID','fldverificationcode as fldVerificationCode','sessions.id as pkSessionID','user_id as fkUserID','bookStatus_id as fkBookStatusID','images')
        ->whereIn('session_id', $arrSessionIDs)
        ->where('fldStatus','Live')
        ->whereIn('bookStatus_id',[$objBookStatus->getBookStatusID('attended'),$objBookStatus->getBookStatusID('booked')])
        ->orderBy('books.created_at', 'Desc')
        ->paginate(10);
        //dd($arrPastBookedSession);
        foreach ($arrPastBookedSession as $obj) {
            $objBookStatus = new BookStatus();
            $objUser = new UserDetails();
            $obj->session = $objSession->getSession($obj->pkSessionID);
            $obj->user = $objUser->getUserDetailsByID($obj->fkUserID);
          
            //$this->fkBookStatusID  = $obj->fkBookStatusID;
            $obj->session->booking = $this->userBookedSession($obj->fkUserID,$obj->fldVerificationCode);
            $obj->session->booking->BookStatus = $objBookStatus->getBookStatusByID($obj->fkBookStatusID);
            //dd($obj->BookSatus);
            unset($obj->fldVerificationCode);
            
        }
        return $arrPastBookedSession;
    }
    
    public function getUpcomingBookedSessionAttachedToProvider($arrSessionIDs){
        $objBookStatus = new BookStatus();
        $objReview = new Review();
        $objSession = new Session();
        $currentDateTime = date('Y-m-d');
        
        
        $arrUpcomingBookedSession = DB::table('books')
        ->leftJoin('sessions', 'books.session_id', '=', 'sessions.id')
        ->select('books.id as pkBookID','pointTransaction_id as fkTransactionID','fldverificationcode as fldVerificationCode','session_id as pkSessionID','user_id as fkUserID','bookStatus_id as fkBookStatusID')
        ->whereIn('session_id', $arrSessionIDs)
        ->where('fldStatus','Live')
        ->whereIn('bookStatus_id',[$objBookStatus->getBookStatusID('pending')])
        ->orderBy('books.created_at', 'Desc')
        ->paginate(10);
        //dd($arrUpcomingBookedSession);
        foreach ($arrUpcomingBookedSession  as $obj) {
            //dd($obj);
            $objBookStatus = new BookStatus();
            $objUser = new UserDetails();
            $obj->session = $objSession->getSession($obj->pkSessionID);
            $obj->user = $objUser->getUserDetailsByID($obj->fkUserID);
            //$this->fkBookStatusID  = $obj->fkBookStatusID;
            $obj->session->booking = $this->userBookedSession($obj->fkUserID,$obj->fldVerificationCode);
            $obj->session->booking->BookStatus = $objBookStatus->getBookStatusByID($obj->fkBookStatusID);
       
            //dd($obj->BookSatus);
            unset($obj->fldVerificationCode);
            
        }
        return $arrUpcomingBookedSession;
    }
    
    
    
    public function getTotalBook($intSessionID){
        $arrBook = Book::select('id as pkBookID','fldPointCost','session_id as fkSessionID','user_id as fkUserID','bookStatus_id as fkBookStatusID','pointTransaction_id as fkTransactionID','fldverificationcode as fldVerificationCode','updated_at','created_at')->where('session_id',$intSessionID)->where('bookStatus_id',2)->get();
        return $arrBook;
    }
    public function getTotalBookUserIDs($intSessionID){
        //dd($intSessionID);
        $arrBookUserIDs = Book::select('user_id')->where('session_id',$intSessionID)->whereIn('bookStatus_id',[1,2])->pluck('user_id')->toArray();
        //dd($arrBookUserIDs);
        return $arrBookUserIDs;
    }
    public function getCountBookedOnSession($intSessionID){
        $objBookStatus = new BookStatus();
        
        
        $BookCount = $this->where('session_id',$intSessionID)->where('bookStatus_id','!=',$objBookStatus->getBookStatusID('cancelled'))->where('bookStatus_id','!=',$objBookStatus->getBookStatusID('rejected'))->count();
        //dd($arrBook);
        
        return $BookCount;
    }
    
    public function getUpcomingBookedSession($intUserID){
        $objBookStatus = new BookStatus();
        $objReview = new Review();
        $objSession = new Session();
        $currentDateTime = date('Y-m-d');
        $arrUpComingBookedSession = DB::table('books')
        ->leftJoin('sessions', 'books.session_id', '=', 'sessions.id')
        ->select('books.book_type as pkBookType','books.id as pkBookID','pointTransaction_id as fkTransactionID','fldverificationcode as fldVerificationCode','sessions.id as pkSessionID','user_id as fkUserID','bookStatus_id as fkBookStatusID','location_id as fkLocationID')
        ->where('user_id',$intUserID)
        ->where('fldStatus','Live')
        ->where('fldSessionDate' ,'>=',$currentDateTime)
        ->whereIn('bookStatus_id',[$objBookStatus->getBookStatusID('booked'),$objBookStatus->getBookStatusID('pending')])
        ->orderBy('books.created_at', 'Desc')
        ->paginate(10);
        //dd('mirette');
        //print_r($arrUpComingBookedSession);
        foreach ($arrUpComingBookedSession as $obj) {
            //print($obj->pkBookID ." - ");
            //$obj->session = '';
            $obj->IsUserReview = '';
            $objProvider = new Provider();
            
            //print($objSession);die;
            $obj->session = $objSession->getSession($obj->pkSessionID);
            //print($obj->session->provider->pkProviderID);
            //print($obj->pkSessionID .'-');
            
            //  $date = new DateTime($obj->session->fldsessiondate);
            
            //     $obj->session->fldsessiondate = $date->format("D,j M Y");
            //    $obj->session->fldstarttime = $date->format("H:i");
            $obj->provider12 = '';
            //print($obj->fkLocationID);
            $obj->provider12 = $objProvider->getProvider($obj->fkLocationID);
            unset($objSession->provider);
            
            $obj->session->provider = $obj->provider12;
            unset($obj->provider12);
            
            //dd($obj->session->provider12);
            //unset($obj->session->provider);
            
            
            
            
            $obj->IsUserReview = $objReview->getUserReviewAttachedToSession($obj->pkSessionID,$obj->fkUserID,$obj->pkBookID);
            $obj->session->booking = $this->userBookedSession($intUserID,$obj->fldVerificationCode);
            $obj->session->booking->BookStatus = '';
            unset($obj->fldVerificationCode);
            
            $obj->session->booking->BookStatus = $objBookStatus->getBookStatusByID($obj->fkBookStatusID);
            
            //dd($obj->session->booking->BookStatus);
        }
        return $arrUpComingBookedSession;
    }
    public function checkBookStatus($intBookID){
        $book = Book::where('id',$intBookID)->select('bookStatus_id')->first();
        //dd($intBookID);die;
        if($book->bookStatus_id == 2){
            return true;
        }else{
            return false;
        }
    }
    
    public function getBookStatus($intBookID){
        $book = Book::where('id',$intBookID)->select('bookStatus_id')->first();
        //dd($intBookID);die;
        return $book->bookStatus_id ;
    }
    public function SessionPassed(){
        $currentDateTime = date('Y-m-d H:i:s');
        return $this->hasMany('App\Session','id','session_id')->where('fldSessionDate','<=',$currentDateTime);
    }
    
    public function Session(){
        $currentDateTime = date('Y-m-d H:i:s');
        //dd($this->hasMany('App\Session','pkSessionID','fkSessionID')->where('created_at','>=',$currentDateTime)->toSql());
        return $this->belongsTo('App\Session','session_id','id');
    }
    
    public function Review(){
        
        //dd($this->fkUserID);
        return $this->belongsTo('App\Review','session_id','session_id')->where('session_id',$this->session_id)->where('fkUserID',$this->user_id);
        
    }
    
    public function BookStatus(){
        return $this->belongsTo('App\BookStatus','bookStatus_id','id');
    }
    
    public function userDetails(){
        return $this->belongsTo('App\UserDetails','user_id','user_id');
    }
    public function Phone(){
        return $this->hasMany('App\Phone','user_id','user_id')->where('fldNumberType','phoneNumber');
    }
    
    public function getBookAttendedTo($intSessionID){
        $book = Book::where('session_id',$intSessionID)->where('bookStatus_id','3')->get();
        //dd($intBookID);die;
        return $book;
    }
    
    /******************************* CPanel *****************************************/
    /***********************************************************************************/
    /***********************************************************************************/
    /***********************************************************************************/
    /***********************************************************************************/
    
    
    public function save(array $options = [])
    {
        if(isset($this->original) && count($this->original) > 0)
        {
            $request = new Request();
            
            $request->pkBookID = $this->id;
            $request->fkBookStatusID = $this->bookStatus_id;
            
            switch ($this->original['bookStatus_id']) {
                case "1":
                    
                    if($this->bookStatus_id == 2)
                    {
                        
                        
                        $bookController = new BookController();
                        $bookController->UpdateBookStatus($request);
                    }
                    else if($this->bookStatus_id == 4 || $this->bookStatus_id == 5 )
                    {
                        
                        
                        $this->cancelBookingNoFare($this->pointTransaction_id,$request);
                        
                    }
                    
                    break;
                    
                case "2":
                    
                    if($this->bookStatus_id == 3)
                    {
                        
                        
                        $bookController = new BookController();
                        $bookController->UpdateBookStatus($request);
                    }
                    
                    else if($this->bookStatus_id == 4)
                    {
                        
                        
                        
                        $request->skipPass = 'Sk!P@SportAdmIn';
                        $request->intUserID = $this->user_id;
                        $request->intBookID = $this->id;
                        
                        
                        $pointTransactionController = new PointTransactionController();
                        $pointTransactionController->CancelSession($request);
                        
                        //  dd($pointTransactionController);
                        
                        $bookController = new BookController();
                        $bookController->UpdateBookStatus($request);
                    }
                    else if($this->bookStatus_id == 6)
                    {
                        //$this->cancelBookingNoFare($this->pointTransaction_id,$request);
                        
                    }
                    
                    
                    
                    break;
                    
                default:
                    break;
            }
            
        }
        
        
        
        
        parent::save();
        
        
    }
    
    public function cancelBookingNoFare($pointTransaction_id,$request)
    {
        $objPointTrans = new PointTransaction();
        $arrTransIDs[]=json_decode($pointTransaction_id);
        
        $objPointTrans->updateGroupPointTrans($arrTransIDs);
        
        $bookController = new BookController();
        $bookController->UpdateBookStatus($request);
    }
    
    
    
    public function getPastBookedSessionAttachedToProviderControlPanel($arrSessionIDs){
        $objBookStatus = new BookStatus();
        $objReview = new Review();
        $objSession = new Session();
        $currentDateTime = date('Y-m-d');
        
        
        $arrPastBookedSession = DB::table('books')
        ->leftJoin('sessions', 'books.session_id', '=', 'sessions.id')
        ->select('books.id as pkBookID','pointTransaction_id as fkTransactionID','cashTransaction_id as fkCashTransactionID','fldverificationcode as fldVerificationCode','sessions.id as pkSessionID','user_id as fkUserID','bookStatus_id as fkBookStatusID','images','provider_payment')
        ->whereIn('session_id', $arrSessionIDs)
        ->where('fldStatus','Live')
        ->whereIn('bookStatus_id',[$objBookStatus->getBookStatusID('cancelled'),$objBookStatus->getBookStatusID('attended'),$objBookStatus->getBookStatusID('booked')])
        ->where('receipt_id',null)
        ->orderBy('books.created_at', 'Desc')
        ->paginate(10);
//         dd($arrPastBookedSession);
        foreach ($arrPastBookedSession as $obj) {
            $objCodeTrans = new CodeTransaction();
            $objBookStatus = new BookStatus();
            $objUser = new UserDetails();
            $obj->session = $objSession->getSession($obj->pkSessionID);
            $obj->user = $objUser->getUserDetailsByID($obj->fkUserID);
            $obj->session->booking = $this->userBookedSession($obj->fkUserID,$obj->fldVerificationCode);
            $obj->BookStatus = $objBookStatus->getBookStatusByID($obj->fkBookStatusID);
            if($obj->fkTransactionID != null){
                $obj->promocode = $objCodeTrans->checkBookPromo($obj->fkTransactionID);
            }else if($obj->fkCashTransactionID != null){
                $obj->promocode = $objCodeTrans->checkBookPromo($obj->fkCashTransactionID);
            }else{
                $obj->promocode = 'No';
            }
            unset($obj->fldVerificationCode);
            
        }
        return $arrPastBookedSession;
    }
    
    
    /***********************************************************************************/
    /***********************************************************************************/
    /***********************************************************************************/
    /***********************************************************************************/
    /******************************END CPanel**************************************/
    public function bookSessionByCash($sessionId,$user_id,$promocode,$noofParticipiants,$merchant_ref,$objFort){
        $arrTransIds = array();
        $arr = array();
        $objSession = new Session();
        if(count($objSession->checkSession($sessionId))==0){
            $arr = Status::mergeStatus($arr, 5001);
            return $arr;
        }
        $IsSessionLive = $objSession->checkSessionStatus($sessionId);
        if(!$IsSessionLive){
            $arr = Status::mergeStatus($arr, 4025);
            return $arr;
        }
        $objInvitataionCodeTrans = new InvitationCodeTrans();
        $InvitationCodeTrans = $objInvitataionCodeTrans->getInvitationCodeTrans($user_id);
        //                 dd($InvitationCodeTrans);
        if($InvitationCodeTrans!== null){
            //                     $objPointTransaction = new PointTransactionController();
            $objInvitationCode = new InvitationCode();
            $strInvitationCode = $objInvitationCode->getInvitationCodeOfUser($user_id);
            //dd($InvitationCodeTrans->owner_user_id);
            $resultPointTransaction = $this->InvitationCode($strInvitationCode,$InvitationCodeTrans->owner_user_id);
            $resultofRevoked = $InvitationCodeTrans->revokeInvitationCodeTrans($user_id);
            
        }
        $objBook = new Book();
        $IsBookedByUser =  $objBook->userIsBookedThisSession($user_id,$sessionId);
        if(!$IsBookedByUser){
            $objBookCont = new BookController();
            if(!$objBookCont->CheckParticipentAvailability($noofParticipiants,$sessionId)['result']){
                $arr = Status::mergeStatus($arr,4011);
                $arr['results'] = $objBookCont->CheckParticipentAvailability($noofParticipiants,$sessionId)['number'];
                return $arr;
            }
        }else{
            $arr = Status::mergeStatus($arr,4008);
            return $arr;
        }
        $objSession = new Session();
        $ResultobjSession = $objSession->where('id',$sessionId)->where('fldstatus','Live')->first();
        if($ResultobjSession){
            $objCashTransaction = new Cashtransaction();
            $objTransactionType = new TransactionType();
            $objCashTransaction->user_id = $user_id;
            $objCashTransaction->amount = -($ResultobjSession->fldsessionprice);
            $objCashTransaction->transactiontype_id = $objTransactionType->getTarnsactionID('Cash Purchase');
            $objCashTransaction->note = 'Book session by cash';
            $objCashTransaction->merchant_reference = $merchant_ref;
            $objCashTransaction->noofparticipants = $noofParticipiants;
//             dd($promocode);
            try{
                $resultCashTransaction = $objCashTransaction->save();
                $arrTransIds []= $objCashTransaction->id ; 
            }
            catch (Exception $e){
                
                $errorCode = $e->errorInfo[1];
                if($errorCode == 1062){
                }
            }
            if($promocode !=null && $promocode!=""){
                $objPromo = new PromoCode();
                $resultPromo = $objPromo->getPromoCodeByID($promocode,$sessionId);
                if($resultPromo != null){
                $objCashTransaction = new Cashtransaction();
                $objTransactionType = new TransactionType();
                $objCashTransaction->user_id = $user_id;
                $objCashTransaction->amount = $resultPromo->fldpromocodePrice ;
                $objCashTransaction->transactiontype_id = $objTransactionType->getTarnsactionID('PromoCode');
                $objCashTransaction->note = 'PromoCode';
                $objCashTransaction->merchant_reference = 'null'; 
                $objCashTransaction->noofparticipants = $noofParticipiants;
//                 dd($promocode);
                try{
                    $resultCashTransaction = $objCashTransaction->save();
                    $arrTransIds [] = $objCashTransaction->id ; 
                    $objCodeTransaction = new CodeTransaction();
                    $addCode = $objCodeTransaction->addTransactionForCash('PromoCode', $objCashTransaction->id, $resultPromo->id);
//                     dd($addBook);
                }
                catch (Exception $e){
//                     dd($e);
                    $resultCashTransaction = $objCashTransaction->update();                    
                    $errorCode = $e->errorInfo[1];
                    if($errorCode == 1062){
                    }
                }
                
                }else{
                    
                }
            }
            for($x = 0 ; $x < $noofParticipiants;$x++){
                $obj = new Book() ;
                $addBook = $obj->AddTobookCash($user_id, $arrTransIds, $sessionId, $ResultobjSession->fldsessionprice, $ResultobjSession->isautobooked);
            }
            //             $addBook = $this->AddTobookCash($user_id, $arrTransIds, $sessionId, $ResultobjSession->fldsessionprice, $ResultobjSession->isautobooked);
            if($objFort->card_number != NULL && strlen($objFort->card_number) > 4){
                $carbon_now = Carbon::now();
                $date = $carbon_now->toFormattedDateString();
                $card_ending = substr($objFort->card_number, -4);
                
                $user = new User();
                $UserData = $user->getUser($objFort->user_id);
                
                $mailer = new Mailer();
                $result = $mailer->send('customer-care@sportoya.com',$UserData['email'],'9151212',[
                    "product_name" => "Sportoya",
                    "name" => $objFort->card_holder_name,
                    "credit_card_statement_name" => "SPORTOYA",
                    "credit_card_brand" => $objFort->payment_option,
                    "credit_card_last_four" => $card_ending,
                    "receipt_id" => $objFort->authorization_code,
                    "date" => $date,
                    "receipt_details" => [
                        [
                            "description" => $ResultobjSession->fldsessionname,
                            "amount" => $ResultobjSession->fldsessionprice.".00 EGP"
                        ]
                    ],
                    "total" => $ResultobjSession->fldsessionprice.".00 EGP",
                    "support_url" => "mailto:moh.adel1993.ma@gmail.com"
                     
                ]);
            }
            return '200';
        }else{
            $arr = Status::mergeStatus($arr,4012);
            return '4013';
        }
    }
    
    public function AddTobookCash($user_id,$arrTransactionIDs,$intSessionID,$intPrice,$IsAutoBook){
        $objUser = new UserController();
        $objSession = new Session();
        $session = $objSession->where('id',$intSessionID)->where('fldstatus','Live')->first();
        $this->fldverificationcode = str_random(4).$arrTransactionIDs[0];
        $this->cashTransaction_id = json_encode($arrTransactionIDs);
//         dd($session);
        if($IsAutoBook == 1){
            
            $this->bookStatus_id = 2;
            $this->sportoya_payment = (0.3*$session->fldsessioncost)+$session->sportoyaSessionMarginPayNow;
            $this->provider_payment = (0.7*$session->fldsessioncost);
            
        }else if($IsAutoBook == 0){
            $this->bookStatus_id = 1;
        }
        $this->user_id = $user_id ;
        $this->session_id = $intSessionID;
        $this->fldcashcost = $intPrice;
        $this->book_type = 'Cash';
        $result = $this->save();

         $mailer = new Mailer();
         $Book = $this->getBook($this->id);
         $objLocation = new Location();
         $Provider = $objLocation->getProvider($session->location_id);
         $user = new User();
         $UserData = $user->getUser($user_id);
         $session['book'] = $Book;

        if($IsAutoBook == 1){
           
//             $UserData['email']
            $result = $mailer->send('customer-care@sportoya.com',$UserData['email'],'9151970',[
                "product_name" => "Sportoya",
                "name" => $UserData['name'],
                "session_name" => $session->fldsessionname,
                "provider_name" => $Provider->fldprovidername,
                "confirmation_code" => $this->fldverificationcode,
                "support_url" => "mailto:customer-care@sportoya.com"
            ]);
             
        }

        $resultRep = $mailer->send('customer-care@sportoya.com','customer-care@sportoya.com','9151970',[
            "product_name" => "Sportoya",
            "name" => $UserData['name'],
            "session_name" => $session->fldsessionname,
            "provider_name" => $Provider->fldprovidername,
            "confirmation_code" => $this->fldverificationcode,
            "support_url" => "mailto:customer-care@sportoya.com"
        ]);

        if($result){
            return true;
        }else{
            return false;
        }
        
        
    }
    public function cancelSessionForCash($session_id,$book_id){
        //check cancellation rule
        $objSession = new Session();
        $objSessionWithCancellation = $objSession->getSessionWithCancellation($session_id);
        $CurrentDate = Carbon::now()->toDateString();
        $equation = date('Y-m-d', strtotime($objSessionWithCancellation->fldsessiondate.' - '.$objSessionWithCancellation['cancellationType']->fldmaxtime.' days'));
        $objBookStatus = new BookStatus();
        $intBookStatusID  =$objBookStatus->getBookStatusID('CANCELLED');
        $session = $objSession->where('id',$session_id)->first();
        if( $CurrentDate>$equation){
            if($objSessionWithCancellation['cancellationType']->fldcancellationname == 'Strict'){
                $sportoya_payment = $session->sportoyaSessionMarginPayNow + (0.3*$session->fldsessioncost);
                $provider_paymnet = 0.7*$session->fldsessioncost;
                $objBook = DB::table('books')->where('id', $book_id)
                ->update(['bookStatus_id' => $intBookStatusID,'provider_payment'=>$provider_paymnet,'sportoya_payment'=>$sportoya_payment]);
                return $objBook;
            }else if($objSessionWithCancellation['cancellationType']->fldcancellationname == 'Flexible'){
                $SessionCancelRule = $session->fldsessioncost * ($objSessionWithCancellation['cancellationType']->fldpointsdeducted)/100;
                $sportoya_payment = ($session->sportoyaSessionMarginPayNow*0.2) + (0.3*($SessionCancelRule));
                $provider_paymnet = 0.7*$SessionCancelRule;
                $objBook = DB::table('books')->where('id', $book_id)
                ->update(['bookStatus_id' => $intBookStatusID,'provider_payment'=>$provider_paymnet,'sportoya_payment'=>$sportoya_payment]);
                return $objBook;
            }
            
        }else{
            $sportoya_payment = 0;
            $provider_paymnet = 0;
            $objBook = DB::table('books')->where('id', $book_id)
            ->update(['bookStatus_id' => $intBookStatusID,'provider_payment'=>$provider_paymnet,'sportoya_payment'=>$sportoya_payment]);
            return $objBook;
        }
    }
    public function updateBookStatusCash($intBookID,$intBookStatusID,$Session){
        if($intBookStatusID == 2){
            $sportoya_payment = (0.3*$Session->fldsessioncost)+$Session->sportoyaSessionMarginPayNow;
            $provider_paymnet = ($Session->fldsessioncost*0.7);
            $objBook = DB::table('books')->where('id', $intBookID)
            ->update(['bookStatus_id' => $intBookStatusID,'provider_payment'=>$provider_paymnet,'sportoya_payment'=>$sportoya_payment]);
            return $objBook;
        }else if($intBookStatusID == 3){
            
            $sportoya_payment = $Session->sportoyaSessionMarginPayNow;
            $provider_paymnet = $Session->fldsessioncost;
            $objBook = DB::table('books')->where('id', $intBookID)
            ->update(['bookStatus_id' => $intBookStatusID,'provider_payment'=>$provider_paymnet,'sportoya_payment'=>$sportoya_payment]);
            return $objBook;
        }else if($intBookStatusID == 4){
            $objBook = $this->cancelSessionForCash($Session->id, $intBookID);
            return $objBook ;
        }else{
            $objBook = DB::table('books')->where('id', $intBookID)
            ->update(['bookStatus_id' => $intBookStatusID]);
            return $objBook;
        }
        
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResetPassword extends Model
{
    protected $table = 'resetpasswords';

    protected $fillable = ['id','user_id','fldemail','fldtoken','fldexpirydate','revoked','created_at','updated_at'];

    public function checkValidationToken($strToken){
    	$orig_timezone = new \DateTimeZone('UTC');
    	$CurrentDate = date("Y-m-d");
        $schedule_date = new \DateTime($CurrentDate,$orig_timezone);
        $schedule_date->setTimeZone(new \DateTimeZone('Africa/Cairo'));
        $CurrentDate =  $schedule_date->format('Y-m-d H:i:s');
        //print($CurrentDate);die;
    	$objResetPassword = ResetPassword::where('fldtoken',$strToken)->where('fldexpirydate','>',$CurrentDate)->where('revoked',0)->first();
    	
    	if(!$objResetPassword){
    			$this->revokedToken($strToken);
    	}
    	return $objResetPassword;
    }	

    public function revokedToken($strToken){
    	$obj = ResetPassword::where('fldtoken',$strToken)->update(['revoked'=>1]);
    	return $obj;
    }






}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterPromo extends Model
{
   	protected $table = 'regpromos';

    protected $fillable = ['id','fldregpromoname','fldregpromotext','fldregpromopoints','fldexpiryday'];

    
    public function listRegPromo(){
       $arrRegisterPromo = RegisterPromo::select('id','fldregpromoname','fldregpromotext','fldregpromopoints','fldexpiryday')->get();
        //dd($arrSession->toSql());
        return $arrRegisterPromo; 
    }


    public function getRegisterPromo($strRegisterPromo){
    	//dd($strRegisterPromo);
    	$objRegisterPromo = RegisterPromo::select('id','fldregpromoname','fldregpromotext','fldregpromopoints','fldexpiryday')->where('fldregpromotext',$strRegisterPromo)->first();
        //dd($arrSession->toSql());
    	return $objRegisterPromo;
    }

    public function getPlanAttachedToRegPromo($intRegisterPromoID){
    	//dd($strRegisterPromo);
    	$objRegisterPromo = RegisterPromo::select('id','fldregpromoname','fldregpromotext','fldregpromopoints','fldexpiryday')->where('id',$intRegisterPromoID)->get();
        //dd($arrSession->toSql());
    	return $objRegisterPromo;
    }


    public function deleteRegPromo($intRegisterPromoID){
        $objRegisterPromo = RegisterPromo::select('id','fldregpromoname','fldregpromotext','fldregpromopoints','fldexpiryday')->where('id',$intRegisterPromoID)->delete();
        return $objRegisterPromo;
    }
    


    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Note extends Model
{
	protected $table = 'notes';

    protected $fillable = ['id','fldtitlenote','fldnotecontent','fkItemID','type_id'];


    public function listNote($intItemID,$intTypeID){
    	$arrNote = Note::select('id','fldtitlenote','fldnotecontent','fkItemID','fkTypeID')->where('fkItemID',$intItemID)->where('type_id',$intTypeID)->get();

    	return $arrNote;
    }

    public function deleteNote($intItemID,$intTypeID){
    	$objNote = DB::table('notes')->where('fkItemID',$intItemID)->where('type_id',$intTypeID)->delete();
    	return $objNote;
    }

    
}

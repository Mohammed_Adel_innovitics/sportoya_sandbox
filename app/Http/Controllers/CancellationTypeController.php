<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CancellationType;
use App\Status;
use validator;

class CancellationTypeController extends Controller
{
       public function ListCancellationRule(){
		$objCancellationRule = new CancellationType();
		$arrCancellationRule = $objCancellationRule->listCancellationType();

		//return $arrSport;	
		$arr['results'] = $arrCancellationRule;
		$arr = Status::mergeStatus($arr,200);
		return $arr;
	}

	public function AddCancellationRule(Request $request){

		$arr = array();
		/*$validator = Validator::make($request->all(), [
            'fldCancellationName' => 'required|unique:tbl_cancellation_rule',
            
        ]);

        if ($validator->fails()) {
            $arr = Status::mergeStatus($arr,4007);
			return $arr;
		}*/
		
		$input = $request->all();

        $CancellationType = CancellationType::create($input);
		$arr['results'] = $CancellationType;
		$arr = Status::mergeStatus($arr,200);

		return $arr;
        	
	}

	public function DeleteCancellationRule(Request $request){
    	$arr = array();
    	$intCancellationTypeID = $request->pkCancellationTypeID;

    	$objCancellationType = new CancellationType();

    	$result = $objCancellationType->deleteCancellationType($intCancellationTypeID);	
    	if($result){
	    	$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,404);
		}
		return $arr;
    }

    public function UpdateCancellationRule(Request $request){
		$arr = array();
		/*$validator = Validator::make($request->all(), [
            'fldCancellationName' => 'required|unique:tbl_sport',
            
        ]);

        if ($validator->fails()) {
        	$arr = Status::mergeStatus($arr,4007);   

			return $arr;

        }*/

		$intCancellationTypeID = $request->pkCancellationTypeID;
		$strCancellationName = $request->fldCancellationName;
		$intCancellationColor = $request->fldCancellationColor;
		$strDescription = $rrequest->fldDescription;
		$intMaxTime = $request->fldMaxTime;
		
		$objCancellationType = new CancellationType();
		$result = $objCancellationType->updateCancellationType($intCancellationTypeID,$strCancellationName,$intCancellationColor,$intMaxTime,$strDescription);    	
		if($result){
	    	$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,404);
		}
		return $arr;
    }

    public function GetCancellationTypeByID(Request $request){
    	$arr = array();
    	$intCancellationTypeID = $request->pkCancellationTypeID;

    	$objCancellationType = new CancellationType();
		$result = $objCancellationType->loadCancellationTypeByID($intCancellationTypeID);    	
		if($result){
	    	
			$arr['status']['object'] = $result;
			$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,404);
		}
		return $arr;	
    }

}


<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voucher;
use Illuminate\Support\Facades\Auth;
use App\PointTransaction;
use Illuminate\Support\Facades\DB;
use App\CodeTransaction;
use App\Status;

class VoucherController extends Controller
{
    //
    public function ApplyVoucher(Request $request){
        $arr = array();
        $objVoucher = new Voucher();
        $userId = Auth::user()->id ; 
        $expiry = $objVoucher->checkVoucherExpiry($request->code);
        $limit = $objVoucher->checkVoucherLimit($request->code);
        $availability = $objVoucher->checkUserAvailability($userId,$request->code);
//         dd($availability.$limit.$expiry);
        if($limit == 'true' && $availability == 'true' && $expiry=='true'){
            $voucher = $objVoucher->where('text',$request->code)->where('revoked',0)->first();
            $objPointTrans = new PointTransaction();
            $add = DB::table('pointtransactions')->insertGetId(['revoked'=>'0','fldpoint'=>$voucher['points'],'transactionType_id'=>'5','fldtransactionnote'=>'Voucher Gift','user_id'=>$userId]);
            $objCodeTrans = new CodeTransaction();
            $addTrans = $objCodeTrans->addTransactionForVoucher('Voucher', $add,$voucher['id'],$userId);
            $arr = Status::printStatus(200);
        }elseif ($expiry == 'false'){
            $arr = Status::printStatus(5010);
        }elseif($limit == 'false'){
            $arr = Status::printStatus(5011);
        }elseif($availability == 'false'){
            $arr = Status::printStatus(5012);
        }
        return $arr ; 
    }
}

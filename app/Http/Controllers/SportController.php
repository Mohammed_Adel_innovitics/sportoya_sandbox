<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Sport;
use App\User;
use Validator;
use App\Status;

class SportController extends Controller
{
    //

	public function ListSport(){
		$arr = array();
		$objSport = new Sport();
		$arrSport = $objSport->ListSport();

		$arr['results'] = $arrSport;
		$arr = Status::mergeStatus($arr,200);

		return $arr;
	}

	public function ListSportInterest(){
		$arr = array();
		$objSport = new Sport();
		$arrSport = $objSport->ListSportInterest();

		//return $arrSport;


		
		$arr['results'] = $arrSport;
		$arr = Status::mergeStatus($arr,200);

		return $arr;}

	public function ListSportNavBar(){
		$arr = array();
		$objSport = new Sport();
		$arrSport = $objSport->ListSportNavBar();

		//return $arrSport;

		$arr['results'] = $arrSport;
		$arr = Status::mergeStatus($arr,200);

		return $arr;
	}
	public function AddSport(Request $request){
		$arr = array();
		$validator = Validator::make($request->all(), [
            'fldSportName' => 'required|unique:tbl_sport',
            
        ]);

        if ($validator->fails()) {
        	$arr = Status::mergeStatus($arr,4007);
            return $arr;
        }
		
		$input = $request->all();
        $sport = Sport::create($input);

       
		$arr['results'] = $sport;
		$arr = Status::mergeStatus($arr,200);

		return $arr;
        	
	}


	public function DeleteSport(Request $request){
    	$arr = array();
    	$intSportID = $request->sport_id;

    	$objSport = new Sport();

    	$result = $objSport->deleteSport($intSportID);	
    	if($result){
	    	$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,4012);
		}
		return $arr;
    }

    public function UpdateSport(Request $request){
		$validator = Validator::make($request->all(), [
            'fldSportName' => 'required|unique:tbl_sport',
            
        ]);

        if ($validator->fails()) {
        	$arr = Status::mergeStatus($arr,4007);
            return $arr;            
        }

		$intSportID = $request->sport_id;
		$strSportName = $request->fldSportName;
		$strImg = $request->fldImg;
		$strIcon = $request->fldIcon;
		$strDescription = $request->fldDescription;
		$strInterestVisible = $request->fldInterestVisible;
		$IsBarVisible = $request->fldIsBarVisible;


		$objSport = new Sport();
		$result = $objSport->updateSport($intSportID,$strSportName,$strImg,$strIcon,$strDescription,$strInterestVisible,$IsBarVisible);    	
		if($result){
	    	$arr = Status::mergeStatus($arr,200);
		}else{
			$arr = Status::mergeStatus($arr,4012);
		}
		return $arr;
    }

    public function GetSportByID(Request $request){
    	$arr = array();
    	$intSportID = $request->sport_id;

    	$objSport = new Sport();
		$result = $objSport->loadSportByID($intSportID);    	
		if($result){
	    	
			$arr['status']['object'] = $result;
			$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,4012);
		}
		return $arr;	
    }

    public function SportUserInterest(){

    }
}

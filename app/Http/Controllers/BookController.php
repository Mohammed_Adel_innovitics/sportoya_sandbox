<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BalanceController;
use Validator;
use App\Balance;
use App\PromoCode;
use App\Session;
use App\Book;
use App\Status;
use App\Http\Controllers\UserController;
use App\Provider;
use App\PointTransaction;
use App\OutSideCustomer;
use App\Location;
use App\InvitationCodeTrans;
use App\InvitationCode;
use App\Http\Controllers\NotifiController;
use Carbon\Carbon;
use App\Cashtransaction;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{

	public function CheckParticipentAvailability($intNumberOfParticipent,$intSessionID){

		$objSession = new Session();
		
		$NumberAvailableSlots = $objSession->availableSlots($intSessionID);
		if($intNumberOfParticipent <= $NumberAvailableSlots){
			return array('number'=>$NumberAvailableSlots,'result'=>true);
		}else{
			
			return array('number'=>$NumberAvailableSlots,'result'=>false);
		}

	}


	public function BookSession(Request $request){
		$objUser = new UserController();
		$intUserID = $objUser->GetUserID();
		 $validator = Validator::make($request->all(), [
            'pkSessionID' => 'required',
            'fldNumberOfParticipent' => 'required',
            ]);
		 
		if($validator->fails()) {
            $arr = Status::printValidator("Validation Failed");
            return $arr;
        } 


		$arr = array();
		$objSession = new Session();
		if(count($objSession->checkSession($request->pkSessionID))==0){
			$arr = Status::mergeStatus($arr, 5001);
			return $arr;
		}
		$IsSessionLive = $objSession->checkSessionStatus($request->pkSessionID);	
		if(!$IsSessionLive){
			$arr = Status::mergeStatus($arr, 4025);
			return $arr;	
		}	
		$objInvitataionCodeTrans = new InvitationCodeTrans();
		$InvitationCodeTrans = $objInvitataionCodeTrans->getInvitationCodeTrans($intUserID);
		//dd($InvitationCodeTrans);
		if($InvitationCodeTrans!== null){
			$objPointTransaction = new PointTransactionController();
			$objInvitationCode = new InvitationCode();
			$strInvitationCode = $objInvitationCode->getInvitationCodeOfUser($intUserID);
			//dd($InvitationCodeTrans->owner_user_id);
            $resultPointTransaction = $objPointTransaction->InvitationCode($strInvitationCode,$InvitationCodeTrans->owner_user_id);
            $resultofRevoked = $InvitationCodeTrans->revokeInvitationCodeTrans($intUserID); 
        
		}
		$objBook = new Book();
		
		$intUserConfirmedBook = $request->intUserConfirmedBook;
		$resultOfPromoCode = null;
		//dd($intUserID);
		$IsBookedByUser =  $objBook->userIsBookedThisSession($intUserID,$request->pkSessionID);
		if($intUserConfirmedBook != 0 || !$IsBookedByUser){
			
			$intNumberOfParticipent = $request->fldNumberOfParticipent;
			$intSessionID = $request->pkSessionID;
			
			if(!$this->CheckParticipentAvailability($intNumberOfParticipent,$intSessionID)['result']){	
				//$arr['status']['code'] = '200';
					$arr = Status::mergeStatus($arr,4011);
					$arr['results'] = $this->CheckParticipentAvailability($intNumberOfParticipent,$intSessionID)['number'];
					return $arr;	 
			}
		
			
			$resultObjSession = $objSession->getSession($intSessionID);
			
			
			$objBalance = new balance();
			$objBalanceController = new BalanceController();
			if(isset($request->fldPromoCode) && $request->fldPromoCode !== null){ 
				$strPromoCode = $request->fldPromoCode;
				$objPromoCode = new PromoCode();
				$resultOfPromoCode = $objPromoCode->getPromoCode($strPromoCode,$intSessionID);
				
				if($resultOfPromoCode == NULL){
					$arr = Status::mergeStatus($arr,4010);
					return $arr;

				}

				$totalPoints = $objBalanceController->GetBalanceWithPromoCode($intSessionID,$resultOfPromoCode->id,$intNumberOfParticipent);

				$resultIsSufficientBalance = $objBalanceController->BalanceSufficiencyWithPromoCode($intSessionID,$totalPoints,$intNumberOfParticipent);
				//print($resultIsSufficientBalance);
				if(!$resultIsSufficientBalance){
					$arr = Status::mergeStatus($arr,4009);
					return $arr;
				}
			}else{
				$resultOfSufficientBalance = $objBalanceController->BalanceSufficiency($intSessionID,$intNumberOfParticipent);
			
				if(!$resultOfSufficientBalance){
					$arr = Status::mergeStatus($arr,4009);

					return $arr;
				}
			}
			for($x = 0 ; $x < $intNumberOfParticipent;$x++){
			if(isset($request->fldPromoCode) && $request->fldPromoCode !== null){
						$promoCode = $request->fldPromoCode;
			}
			
			$objPointTransaction = new PointTransactionController();

			

	        $arrPointTransactionIDs = $objPointTransaction->BookSession($resultObjSession,$resultOfPromoCode);	
	        //dd($resultPointTransaction);	
			$objBook = new Book();
			$resultBook = $objBook->AddTobook($arrPointTransactionIDs,$intSessionID,$resultObjSession->fldsessionpoint,$resultObjSession->isautobooked);




			/*if(isset($request->fldPromoCode) && $request->fldPromoCode !== null){
				$objPointTransaction = new PointTransactionController();
	            $resultPointTransaction = $objPointTransaction->PromoCode($request->fldPromoCode,$intSessionID,$resultPointTransaction);
			}
*/
			}	
			$objExtendExpiry = new PointTransaction();
			$resultOfExtend = $objExtendExpiry->extendExpiryDateOfBouns($intUserID);		
			$arr = Status::mergeStatus($arr,200);
			
			return $arr;
		}else{
			$arr = Status::mergeStatus($arr,4008);
			return $arr;
		}	
	}
	
	public function getTotalBookPerSession(Request $request){
		$arr = array();
		$objBook = new Book();

		$arrBook = $objBook->getTotalBookedSessions($request->pkSessionID);

		return count($arrBook);

	}

	public function getAllParticipent(Request $request){
		$objBook = new Book();
		$intSessionID = $request->pkSessionID;
		$arrInSidePaticipent = $objBook->AllBookWithBookedStatus($intSessionID);
		
		$objOutSide = new OutSideCustomer();
		$arrOutSide = $objOutSide->getAlloutSide($intSessionID);

		$arr['result']['InSide'] = $arrInSidePaticipent;
		$arr['result']['outSide'] = $arrOutSide;
		
		$arr = Status::mergeStatus($arr,200);
		return $arr;
	}

	public function UpcomingBookedSession(){
		$objUser = new UserController();
		$intUserID = $objUser->GetUserID();
		$arr = array();
		$objBook = new Book();
		$arrBooked = $objBook->getUpcomingBookedSession($intUserID);
		 
		$arr['results'] = $arrBooked;
		$arr = Status::mergeStatus($arr,200);
		return $arr;
	}

	public function PastBookedSession(){
		
		$objUser = new UserController();
		$intUserID = $objUser->GetUserID();
		$arr = array();
		$objBook = new Book();
		 $arrBooked = $objBook->getPastBookedSession($intUserID);
		 
		$arr['results'] = $arrBooked;
		$arr = Status::mergeStatus($arr,200);
		return $arr;
	}


	public function ListProviderPastRequestByToken(){
		$objUser = new UserController();
		$intUserID = $objUser->GetUserID();

        $objProvider = new Provider();
        
        $intProviderID = $objProvider->getProviderIDByUserID($intUserID);
        $objLocation = new Location();
        $arrLocationIDs = $objLocation->getLocationIDs($intProviderID);

        $objSession = new Session();

        $arrSessionIDs = $objSession->getSessionAttachedToProviderIDs($arrLocationIDs);

        $objBook = new Book();
		$arrBooked = $objBook->getPastBookedSessionAttachedToProvider($arrSessionIDs);
		 
		$arr['results'] = $arrBooked;
		$arr = Status::mergeStatus($arr,200);
		return $arr;
	}

	public function ListProviderUpcomingRequestByToken(){
		$objUser = new UserController();
		$intUserID = $objUser->GetUserID();

        $objProvider = new Provider();
        //dd($intUserID);
        $intProviderID = $objProvider->getProviderIDByUserID($intUserID);

        $objLocation = new Location();
        $arrLocationIDs = $objLocation->getLocationIDs($intProviderID);
        //dd($arrLocationIDs);
        $objSession = new Session();

        $arrSessionIDs = $objSession->getSessionAttachedToProviderIDs($arrLocationIDs);
 //print( $arrSessionIDs);die;
        $objBook = new Book();
		$arrBooked = $objBook->getUpcomingBookedSessionAttachedToProvider($arrSessionIDs);
		 
		$arr['results'] = $arrBooked;
		$arr = Status::mergeStatus($arr,200);
		return $arr;
	}
	
	public function UpdateBookStatus(Request $request){
		 $arr = array();
		 $objNotifi = new NotifiController();
         
		 $objBook = new Book();
		 $objPointTransaction = new PointTransaction();
//dd($request->fkBookStatusID);
		 $Book = $objBook->getBook($request->pkBookID);
	     if($request->fkBookStatusID == 5){
	         if($Book->book_type == 'Cash' && $Book->bookStatus_id != '4'){
	             $totalamount = 0;
	             $objCashTrans = new Cashtransaction();
	             $Book->cashTransaction_id = json_decode($Book->cashTransaction_id);
	             $transids = $Book->cashTransaction_id;
	             foreach ($transids as $index=>$trans){
	                 $objCash = new Cashtransaction();
	                 $transids[$index] = $objCash->where('id',$trans)->first();
	                 $totalamount += $transids[$index]['amount'];
	             }
	             $objCashTrans->user_id = $Book->user_id;
	             $objCashTrans->amount = abs($totalamount);
	     	     $objCashTrans->transactiontype_id = '14' ;
	     	     $objCashTrans->note = "Cash Refund for session rejected by provider";
	     	     $objCashTrans->merchant_reference = $transids[0]['merchant_reference'];
	     	     $refundResult = $objCashTrans->save() ;
	     	}else{
			$ResultOfPointsBack = $objPointTransaction->updatePointTransByTransID(json_decode($Book->pointTransaction_id));
	     	}
	     }
	     $objSession = new Session();
	     $Session = $objSession->getSessionDetails($Book->session_id);
	     if($Book->book_type == 'Cash'){
	         $objUpdateBookStatus = $objBook->updateBookStatusCash($request->pkBookID,$request->fkBookStatusID,$Session);
	     }else{
	         $objUpdateBookStatus = $objBook->updateBookStatus($request->pkBookID,$request->fkBookStatusID,$Session);
	     }

	     if($objUpdateBookStatus){
	     	$Notifi = $objNotifi->notifiMsg($Session->id,$request->pkBookID,$request->fkBookStatusID);
	     	$arr = Status::mergeStatus($arr,200);
			return $arr;
	     }else{
	     	$arr = Status::mergeStatus($arr,4012);
			return $arr;
	     }

	}

	public function UpdateBookStatusByVerficationCode(Request $request){
	    $objSession = new Session();
	    $Session = $objSession->getSessionDetails($request->pkSessionID);
	    $dateTimeNow = Carbon::now();
	    
	    $dateTimeSession = new Carbon($Session->fldsessiondate." " .$Session->fldstarttime);
	    //print($dateTimeSession."  No:".$dateTimeNow);
	    //dd($dateTimeNow->diffInHours($dateTimeSession,false));
	    $r1 = $dateTimeNow->diffInMinutes($dateTimeSession,false);
	   
	   
	    if($r1 > 60 || $r1 < -(24*60)){
	        $arr = array();
	        $arr = Status::mergeStatus($arr,5009);
	        return $arr;	
	    }
	    //dd($r1 );
		$strVerifcationCode = $request->fldVerificationCode;

		$arr = array();
		$objSession = new Session();
		$objBook = new Book();
		$BookObject = $objBook->getBookByVerficationCode($request->pkSessionID,$strVerifcationCode);

		if($BookObject != false){

		if(!$objSession->checkSessionStatus($request->pkSessionID)){
			$arr = Status::mergeStatus($arr,4025);
			return $arr;	

		}	
		//dd($objBook->getBookStatus($BookObject->id));
		if($objBook->getBookStatus($BookObject->id) == '3'){
		    $arr = Status::mergeStatus($arr,2026);
			return $arr;	

		}else if($objBook->getBookStatus($BookObject->id) == '4'){
		    $arr = Status::mergeStatus($arr,5007);
		    return $arr;
		}
	
        $objUser = new UserController();
        $intUserID = $objUser->GetUserID();
        
        $objProvider = new Provider();
        $objLocation = new Location();
        $provider_id = $objProvider->getProviderIDByUserID($intUserID);
		$arrLocationIDs= $objLocation->getLocationIDs($provider_id);
		//dd($arrLocationIDs);
		$arrSession = $objSession->getSessionAttachedToProviderwithoutPagnation($arrLocationIDs,"1");	
	//dd($arrSession .$request->pkSessionID);
		//dd($arrSession);
		foreach($arrSession as $objSession){
			if($objSession->id == $request->pkSessionID){
			    if($BookObject->book_type == 'Pass'){
			        $result = $objBook->updateBookStatusByVerficationCode($strVerifcationCode,$objSession->fldsessioncost,$objSession->sportoyaSessionMargin);
			    }else{
			        $result = $objBook->updateBookStatusByVerficationCode($strVerifcationCode,$objSession->fldsessioncost,$objSession->sportoyaSessionMarginPayNow);
			    }
				$arr = Status::mergeStatus($arr,200);
				return $arr;			
			}	
		}
		$arr = Status::mergeStatus($arr,4028);
				return $arr;
	}else{
		$arr = Status::mergeStatus($arr,2027);
		return $arr;
	}
	
	}

	public function GetBookedSessionAttachedtoProvider(Request $request){
	    //dd($request);
	    $arr = array();
	    $objLocation = new Location();
	    $arrLocationIDs = $objLocation->getLocationIDs($request->intProviderID);
	    $provider = DB::table('providers')->where('id',$request->intProviderID)->first();
	    $objSession = new Session();
	    
	    $arrSessionIDs = $objSession->getSessionAttachedToProviderIDsTimeIgnored($arrLocationIDs);
	    $objBook = new Book();
	    $arrBooked = $objBook->getPastBookedSessionAttachedToProviderControlPanel($arrSessionIDs);
	    $data = array('arrBooked'=>$arrBooked,'provider'=>$provider);
	    return view('voyager::test.index',$data);
	    
	    
	    $arr['results'] = $arrBooked;
	    $arr = Status::mergeStatus($arr,200);
	    return $arr;
	}
	
}
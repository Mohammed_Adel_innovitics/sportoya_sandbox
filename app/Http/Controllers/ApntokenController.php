<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\Http\Controllers\UserController;
use App\Apntoken;

class ApntokenController extends Controller
{
  public function AddApnToken(Request $request){
	  $arr = array();
  	$objUser = new UserController();
  	$intUserID = $objUser->GetUserID();
  	$objApnToken = new Apntoken();
  	$checkApnToken = $objApnToken->checkApn($request->apntoken,$intUserID);
  	if($checkApnToken == true){
  		$arr = Status::mergeStatus($arr,200);
  		return $arr;
  	}

  	$objApnToken->user_id = $intUserID;
  	$objApnToken->token = $request->apntoken;

  	$result = $objApnToken->save();
  	if($result){
  		$arr = Status::mergeStatus($arr,200);
  	}else{
  		$arr = Status::mergeStatus($arr,4012);
  	}
  	return $arr;
  }


  public function RemoveApn(Request $request){
    $arr = array();
    $objApnToken = new Apntoken();
    $result = $objApnToken->deleteApn($request->strToken);
    //dd($result);
    if($result==1){
      $arr = Status::mergeStatus($arr,200);
    }else{
     $arr = Status::mergeStatus($arr,4012); 
    }
    return $arr;
  }


}

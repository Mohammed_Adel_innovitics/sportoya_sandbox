<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\Http\Controllers\UserController;
use App\UserSportInterest;


class UserSportInterestController extends Controller
{
    

    public function AddUserInterest(Request $request){
    	$arr = array();
    	$objUser = new UserController();
    	$intUserID = $objUser->GetUserID();
        $objUserSportInterest = new UserSportInterest();
        $ResultSportInterestIds = $objUserSportInterest->getUserSportIDs($intUserID);
        if(count($ResultSportInterestIds) >0){
            $ResultDeleteInterest = $objUserSportInterest->deleteUserSportInterest($intUserID);
            if(!$ResultDeleteInterest){
                $arr = Status::mergeStatus($arr,4012);
                return $arr;
            }
        }
        //dd($request->arrSportIDs); 
    	foreach($request->arrSportIDs as $obj){
            
    	    $objUserSportInterest = new UserSportInterest();
        	$objUserSportInterest->sport_id = $obj;
    		$objUserSportInterest->user_id = $intUserID;
    		$result = $objUserSportInterest->save();
        }

        if($result != false){
          $arr = Status::mergeStatus($arr,200);      
        }else{
          $arr = Status::mergeStatus($arr,4012);      
        }    
    	
    	return $arr;

    }

    
   
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Balance;
use App\Session;
use App\PromoCode;
use App\Status;
use App\PointTransaction;
use App\Http\Controllers\UserController;
use App\Book;
use DateTime;

class BalanceController extends Controller
{
	
	//get my points in the application
	public function MyPoints(){

		$objUser = new UserController();
		$intUserID = $objUser->GetUserID();
		$objPointTransaction = new PointTransaction();
		
		$objBook = new Book();
		$myLastBook = $objBook->getLastBook($intUserID);
		$objBalance = new Balance(); 
		if($objBalance->getBalaceOfBouns()>0){
		    $resultBalance = $objBalance->checkexpiryBounsPoint();
		    if($resultBalance){
		        $objPointTransaction->addTrans($objBalance->getBalaceOfBouns(),$intUserID);
		    }
		}
		
		
		$intMyPoints = $objBalance->getBalance();
		$arr['results']['totalPoints'] = $intMyPoints;
		$arr['results']['RegularPoints'] = $objBalance->getBalanceWithoutBouns(); 
		$arr['results']['BounsPoints'] = $objBalance->getBalaceOfBouns();
		if($myLastBook != null && count($myLastBook) >0){
			$date = new DateTime($myLastBook[0]->fldsessiondate);
		
			$arr['results']['myLastSession'] = $date->format("D,j M Y");
		}else{

			$arr['results']['myLastSession'] = null;
		}
		$arr = Status::mergeStatus($arr,200);
		return $arr;
	}
    //return that there is balance meet the action book
	public function BalanceSufficiency($intSessionID,$intNumberParticipent){
		
		$objSession = new Session();
		$Session = $objSession->getSession($intSessionID);
		
		$objBalance = new Balance(); 

	

		if(($Session->fldsessionpoint* $intNumberParticipent) <= $objBalance->getBalance() &&  $intNumberParticipent > 0){
			return true;
		}else{
			return false;
		}
	}
	//return balance after use the promo code
	public function GetBalanceWithPromoCode($intSessionID,$intPromoCodeID,$intNumberParticipent){

		$objSession = new Session();
		$Session = $objSession->getSession($intSessionID);
		$objPromoCode = new PromoCode();
		$resultPromoCode =  $objPromoCode->getPromoCodeByID($intPromoCodeID);


		$objBalance = new Balance();
		
		$TotalPoints = (($resultPromoCode-> fldpromocodepoints*$intNumberParticipent) + $objBalance->getBalance()) ;
		//dd($TotalPoints); 
		return $TotalPoints;
	}

	public function GetBalanceWithPromoCodeWithoutBouns($intSessionID,$intPromoCodeID){

		$objSession = new Session();
		$Session = $objSession->getSession($intSessionID);
		$objPromoCode = new PromoCode();
		$resultPromoCode =  $objPromoCode->getPromoCodeByID($intPromoCodeID);


		$objBalance = new Balance();
		//dd($resultPromoCode); 
		$TotalPoints = (($resultPromoCode-> fldpromocodepoints) + $objBalance->getBalanceWithoutBouns()) ;
		return $TotalPoints;
	}	



		
	//return that there is balance meet the action book after use the promo code
	public function BalanceSufficiencyWithPromoCode($intSessionID,$TotalPoints,$intNumberParticipent){

		$objSession = new Session();
		$objBalance = new Balance();
		$Session = $objSession->getSession($intSessionID);
		$TotalPointsWithParticipents =  $TotalPoints;
		
		if(($Session->fldsessionpoint * $intNumberParticipent)  <= $TotalPointsWithParticipents){
			return true;

		}else{
			return false;
		}
	}
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InvitationCode;
use Validator;
use App\Status;

class InvitationCodeController extends Controller
{
    public function CheckInvitationCodeAvailability(Request $request){
      $arr = array();
      $validator = Validator::make($request->all(), [
            'fldInvitationCode' => 'required|unique:invitationcodes',
        ]);

        if ($validator->fails()) {
            
            $arr = Status::mergeStatus($arr,4007);
        }else{
            $arr = Status::mergeStatus($arr,200);
        }  
        return $arr;

    }

    public function AddInvitationCode($intUserID,$strName){
    	$arr = array();
        $objInvitationCode = new InvitationCode();
    	$result = $objInvitationCode->addInvitationCode($intUserID,$strName);
    	if ($result) {
            $arr = Status::mergeStatus($arr,200);
        }else{
            $arr = Status::mergeStatus($arr,4012);
        } 
        return $arr;
    }


    public function GetInvitationCode($strInvitationCode){
    	$arr = array();
        $obj = new InvitationCode();
    	$result = $obj->getInvitationCode($strInvitationCode) ;
        if ($result) {
            $arr = Status::mergeStatus($arr,200);
        }else{
            $arr = Status::mergeStatus($arr,4012);
        } 
        return $arr;
    	
    }

}

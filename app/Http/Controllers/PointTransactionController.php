<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RegisterPromo;
use App\PointTransaction;
use App\UserDetails;
use App\User;
use App\TransactionType;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\DB;
use App\CodeTransaction;
use App\InvitationCode;
use App\Setting;
use App\Session;
use App\PromoCode;
use App\Status;
use DateTime;
use App\RelativeTransaction;
use App\Book;
use App\BookStatus;
use App\Plan;
use App\Balance;
use App\FortTrans;
use App\Http\Controllers\NotifiController;
use Exception;
use Carbon\Carbon;
use App\Mailer;
use App\Cashtransaction;
use App\InvitationCodeTrans;

class PointTransactionController extends Controller
{
 
    public function RegPromo($strRegPromoCode,$intUserID){
        $arr = array();
    	$objRegPromo = new RegisterPromo();
    	$ResultobjRegPromo = $objRegPromo->getRegisterPromo($strRegPromoCode);
    	//dd($ResultobjRegPromo);
    	if($ResultobjRegPromo){
    		$objPointTransaction = new PointTransaction();
    		$ResultUserDetails =  DB::table('userdetails')->where('user_id',$intUserID)->update(['regpromo_id' => $ResultobjRegPromo->id]);
        	
			if($ResultUserDetails == 0){
			    $arr = Status::mergeStatus($arrCancelSession,4012);
                return $arr;

	        }
            $objTransactionType = new TransactionType();
        	$objPointTransaction->user_id = $intUserID;
    		$objPointTransaction->fldcost = 0;
    		$objPointTransaction->fldpoint = $ResultobjRegPromo->fldregpromopoints;
    		$objPointTransaction->transactionType_id = $objTransactionType->getTarnsactionID('RegisterPromo');
    		$objPointTransaction->fldtransactionnote = 'Register promo code';
    		$objPointTransaction->revoked = 0;
    		
    		$date = date("Y-m-d");
    		$ExpiryDate=date_create($date);
			
    		date_add($ExpiryDate,date_interval_create_from_date_string($ResultobjRegPromo->fldexpiryday." days"));
    		
			
			$objPointTransaction->fldexpirydate = date_format($ExpiryDate,"Y-m-d");
			$resultPointTransaction = $objPointTransaction->save(); 
            
			if(!$resultPointTransaction){
     			return false;
	        }
            $objCodeTransaction = new CodeTransaction();
            $resultCodeTransaction =  $objCodeTransaction->addCodeTransaction('RegCode',$objPointTransaction->id,$ResultobjRegPromo->pkRegPromoID);
             if(!$resultCodeTransaction){
                return false;
            }
            return true;
    	}else{
    		return false;
    	}

     }
    
    public function InvitationCode($strInvitationCode,$intUserID){
        $arr = array();
        $objInvitationCode = new InvitationCode();
        $ResultobjInvitationCode = $objInvitationCode->getInvitationCode($strInvitationCode);
        
        if($ResultobjInvitationCode){
            $objPointTransaction = new PointTransaction();
            
            $objTransactionType = new TransactionType();
            $objPointTransaction->user_id = $intUserID;
            //dd($objPointTransaction->user_id.'hiii');
            $objPointTransaction->fldcost = 0;
            $objSetting = new Setting();
            $objPointTransaction->fldpoint = $objSetting->getInvitationPoint();
            $objPointTransaction->transactionType_id = $objTransactionType->getTarnsactionID('InvitationCode');
            $objPointTransaction->fldtransactionnote = 'Invitation code';
            $objPointTransaction->revoked = 0;
            
            
            $objPointTransaction->fldexpirydate = '2050-01-01';
            $resultPointTransaction = $objPointTransaction->save(); 
            if(!$resultPointTransaction){
                return false;
            }
            
            $objCodeTransaction = new CodeTransaction();
            $resultCodeTransaction =  $objCodeTransaction->addCodeTransaction('InvitationCode',$objPointTransaction->id,$ResultobjInvitationCode->id);
            
             if(!$resultCodeTransaction){
                return false;
            }
            return true;
        }else{
            return false;
        }
    	
    }
    public function PromoCode($strPromoCode,$intSessionID){
        
        $arr = array();
        $objUser = new UserController();
        $objPromoCode = new PromoCode();
        $ResultobjPromoCode = $objPromoCode->getPromoCode($strPromoCode,$intSessionID);
        
        if($ResultobjPromoCode){
            $objPointTransaction = new PointTransaction();
            
            $objTransactionType = new TransactionType();
            $objPointTransaction->user_id = $objUser->GetUserID();
            $objPointTransaction->fldcost = 0;
            
            $objPointTransaction->fldpoint = $ResultobjPromoCode->fldpromocodepoints;
            $objPointTransaction->transactionType_id = $objTransactionType->getTarnsactionID('PromoCode');
            $objPointTransaction->fldtransactionnote = 'Promo code';
            $objPointTransaction->revoked = 0;
            
            
            $objPointTransaction->fldexpirydate = '3000-01-01';
            $resultPointTransaction = $objPointTransaction->save();

            if(!$resultPointTransaction){
                return false;
            }
            
            $objCodeTransaction = new CodeTransaction();
            $resultCodeTransaction =  $objCodeTransaction->addCodeTransaction('PromoCode',$objPointTransaction->id,$ResultobjPromoCode->id);
            if(!$resultCodeTransaction){
                return false;
            }
        

            return $objPointTransaction;
        }else{
            return false;
        }
    	
    }
    public function ReservePlan(Request $request){
        $condition = explode('-',$request->merchant_reference);
        if(str_contains($condition[1], 'CAS') == true ){
            (isset($condition[5]) && $condition[5] != ""&& $condition[5] != null) ? $promocode = $condition[5] : $promocode = null ;
            $noOfParticipiants = $condition[4] ; 
            $objFort = new FortTrans();
            $arr = array();
            $sortedRequest = $request->all();
            ksort($sortedRequest);
            $auth = env("PAYFORT_SIGNATURE_OUT", "");
            foreach($sortedRequest as $key => $val) {
                //  echo "$key = $val\n";
                if($key != "signature")
                { $auth = $auth.$key.'='.$val; }
            }
            $auth = $auth.env("PAYFORT_SIGNATURE_OUT", "");            
            $signature = hash('sha256', $auth);
            $intSessionID = $request->sessionID;
            $objUser = new UserController();
            
            $objFort->amount = $request->amount;
            $objFort->response_code = $request->response_code;
            $objFort->card_number = $request->card_number;
            $objFort->card_holder_name = $request->card_holder_name;
            $objFort->signature = $request->signature;
            $objFort->payment_option = $request->payment_option;
            $objFort->expiry_date = $request->expiry_date;
            $objFort->customer_ip = $request->customer_ip;
            $objFort->customer_email = $request->customer_email;
            $objFort->eci = $request->eci;
            $objFort->language = $request->language;
            $objFort->command = $request->command;
            $objFort->response_message = $request->response_message;
            $objFort->sdk_token = $request->sdk_token;
            $objFort->authorization_code = $request->authorization_code;
            $objFort->merchant_reference = $request->merchant_reference;
            $objFort->currency = $request->currency;
            $objFort->status = $request->status;
            $merchant_reference_sub = explode('-', $request->merchant_reference);
            $objFort->user_id = $merchant_reference_sub[0];
            if($objFort->verifyPayment($request->merchant_reference) == false)
            {
                return '7890';
            }
            $sessionID = explode('CAS', $merchant_reference_sub[1]);
            $objFort->session_id = $sessionID[1];
            try{
                $resultFort = $objFort->save();
                if(!$resultFort){ 
                    
                    return '4012';
                }
            }catch (Exception $e){
                
                $errorCode = $e->errorInfo[1];
                if($errorCode == 1062){
                    // return '200';
                }
            }
//             $signature = $request->signature;
//             $request->response_code = '14000';
//             $request->status = '14';
            if($signature == $request->signature && $request->response_code == "14000" && $request->status == "14"){
                $objBook = new Book();
                $result = $objBook->bookSessionByCash($sessionID[1],$merchant_reference_sub[0],$promocode,$noOfParticipiants,$request->merchant_reference,$objFort);
//                 dd($result);
                        if($result == "200"){
                            return '200';
                        }else{
                            //refund
//                             return $result;
                        }
                }else{
                $arr = Status::mergeStatus($arr,4012);
                return '4014';
            }
//             dd('stop');
            return '200';   
        }else{
        $objFort = new FortTrans();
        //dd(count($objFort->isMerchantExist($request->merchant_reference)));
        
        $arr = array();
        
        $sortedRequest = $request->all();
        ksort($sortedRequest);
        
        $auth = env("PAYFORT_SIGNATURE_OUT", "");

	foreach($sortedRequest as $key => $val) {
			  //  echo "$key = $val\n";
			  if($key != "signature")
			   { $auth = $auth.$key.'='.$val; }
			}

	$auth = $auth.env("PAYFORT_SIGNATURE_OUT", "");
			
	
        $signature = hash('sha256', $auth);
        
               $intPlanID = $request->pkPlanID;
        $objUser = new UserController();
        //$intUserID = $objUser->GetUserID();
        $objPointTransaction = new PointTransaction();

       
        $objFort->amount = $request->amount;
        $objFort->response_code = $request->response_code;
        $objFort->card_number = $request->card_number;
        $objFort->card_holder_name = $request->card_holder_name;
        $objFort->signature = $request->signature;
        $objFort->payment_option = $request->payment_option;
        $objFort->expiry_date = $request->expiry_date;
        $objFort->customer_ip = $request->customer_ip;
        $objFort->customer_email = $request->customer_email;
        $objFort->eci = $request->eci;
        $objFort->language = $request->language;
        $objFort->command = $request->command;
        $objFort->response_message = $request->response_message;
        $objFort->sdk_token = $request->sdk_token;
        $objFort->authorization_code = $request->authorization_code;
        $objFort->merchant_reference = $request->merchant_reference;
        $objFort->currency = $request->currency;
        $objFort->status = $request->status;

        $merchant_reference_sub = explode('-', $request->merchant_reference);
  
        $objFort->user_id = $merchant_reference_sub[0];
       // dd($merchant_reference_sub);
        if($objFort->verifyPayment($request->merchant_reference) == false)
        {
            return '7890';
        }
        $PlanID = explode('PLN', $merchant_reference_sub[1]);

        $objFort->plan_id = $PlanID[1];
       // $PlanID[1];
        try{
            $resultFort = $objFort->save();
            if(!$resultFort){

                return '4012';
            }
        }catch (Exception $e){
           
            $errorCode = $e->errorInfo[1];
            if($errorCode == 1062){
               // return '200';
            }
        }
       // dd($auth." ".$signature." ".$request->signature);
        if($signature == $request->signature && $request->response_code == "14000" && $request->status == "14"){
 
        $arr = array();
        $objPlan = new Plan();
        $ResultobjPlan = $objPlan->loadPlanByID($objFort->plan_id);
        
        if($ResultobjPlan){

            
            $objTransactionType = new TransactionType();
            $objPointTransaction->user_id = $objFort->user_id;
            $objPointTransaction->fldcost = $ResultobjPlan->fldplancostegp;
            
            $objPointTransaction->fldpoint = $ResultobjPlan->fldplanpoints;
            
            $objPointTransaction->transactionType_id = $objTransactionType->getTarnsactionID('Plan');
            $objPointTransaction->revoked = 0; 
            $objPointTransaction->fldtransactionnote = 'Buy Plan';
            $objPointTransaction->merchant_refrence = $request->merchant_reference;
            
            try{
                 $resultPointTransaction = $objPointTransaction->save(); 
            }
            catch (Exception $e){
           
                $errorCode = $e->errorInfo[1];
                if($errorCode == 1062){
                    }
          }


           
     //bouns Transaction
            $objPointTransactionBouns = new PointTransaction();

            $objPointTransactionBouns->user_id = $objFort->user_id;
            
            
            $objPointTransactionBouns->fldpoint = $ResultobjPlan->fldplanbouns;
            
            $objPointTransactionBouns->transactionType_id = $objTransactionType->getTarnsactionID('Bouns');
            $objPointTransactionBouns->revoked = 0; 
            $objPointTransactionBouns->fldtransactionnote = 'Buy Plan Bouns';
            $objPointTransactionBouns->merchant_refrence = $request->merchant_reference.'-B';

           
            $date = date("Y-m-d");
            $ExpiryDate=date_create($date);
            

            date_add($ExpiryDate ,date_interval_create_from_date_string("3 months"));

            $dtExpiryDate = $objPointTransaction->getMaxExpiredDateOfUserPlan($objFort->user_id);
            
            $objPointTransactionBouns->fldexpirydate = $ExpiryDate;

            try{
            $resultPointTransaction = $objPointTransactionBouns->save();
            }
            catch (Exception $e){
           
                $errorCode = $e->errorInfo[1];
                if($errorCode == 1062){
                    return '200';
                    }
          }

        if($objFort->card_number != NULL && strlen($objFort->card_number) > 4)
          { 
           
            $carbon_now = Carbon::now();
            $date = $carbon_now->toFormattedDateString();
            $card_ending = substr($objFort->card_number, -4);

            $user = new User();
            $UserData = $user->getUser($objFort->user_id);
    
            $mailer = new Mailer();
            $result = $mailer->send('customer-care@sportoya.com',$UserData['email'],'9151212',[
                "product_name" => "Sportoya",
                "name" => $objFort->card_holder_name,
                "credit_card_statement_name" => "SPORTOYA",
                "credit_card_brand" => $objFort->payment_option,
                "credit_card_last_four" => $card_ending,
                "receipt_id" => $objFort->authorization_code,
                "date" => $date,
                "receipt_details" => [
                   [
                    "description" => $ResultobjPlan->fldplanname,
                    "amount" => $ResultobjPlan->fldplancostegp.".00 EGP"
                   ]
                ],
                "total" => $ResultobjPlan->fldplancostegp.".00 EGP",
                "support_url" => "mailto:customer-care@sportoya.com"

                ]);

            //dd($result);
        }

          
          return '200';
            
        }else{
            $arr = Status::mergeStatus($arr,4012);
                return '4013';
        }
    }else{
        $arr = Status::mergeStatus($arr,4012);
                return '4014';
    }
     return '200';
    }
}
   
    public function Gift(Request $request){
        $arr = array();
        $intPoints = $request->fldpoints;
        $intExpiryDays = $request->fldexpirydays;
        $arr = array();
        $objUser = new UserController();
        
        
            $objPointTransaction = new PointTransaction();
            
            $objTransactionType = new TransactionType();
            $objPointTransaction->user_id = $objUser->GetUserID();
            $objPointTransaction->fldcost = 0;
            $objPointTransaction->revoked = 0;
            
            $objPointTransaction->fldooint = $intPoints;
            $objPointTransaction->transactionType_id = $objTransactionType->getTarnsactionID('Gift');
            $objPointTransaction->fldtransactionnote = 'Gift';
            
            
           $mydate = date('Y-m-d H:i:s');

           $daystosum = $intexpirydays;
           $datesum = date('Y-m-d', strtotime($mydate.' + '.$daystosum.' days'));


            $objPointTransaction->fldexpirydate = $datesum;
            $resultPointTransaction = $objPointTransaction->save(); 
            if(!$resultPointTransaction){
                $arr = Status::mergeStatus($arr,404);
            }
            
            
            return $arr;
        
        
    }


    public function BookSession($objSession,$objPromoCode){

            $arr = array();
            $objUser = new UserController();    
            $objPointTransaction = new PointTransaction();
            $objBalance = new Balance();
           
            $arrTransIds = array();
            $promoPoints = 0;
        

        if($objPromoCode !== null){
               $promoPoints = $objPromoCode->fldpromocodepoints;
            }
            else
            {
                $promoPoints = 0;
            }

//dd($promoPoints);
           

            $intTotalPoints = $objBalance->getBalanceWithoutBouns() + $promoPoints;
//dd($intTotalPoints);
            if($objSession->fldsessionpoint <= $intTotalPoints){
               // dd($objSession->fldSessionPoint);
                $objPointTransaction = new PointTransaction();
                $objTransactionType = new TransactionType();
                $objPointTransaction->user_id = $objUser->GetUserID();
                $objPointTransaction->fldcost = 0;
        
                $objPointTransaction->fldpoint = -($objSession->fldsessionpoint);
                $objPointTransaction->transactionType_id = $objTransactionType->getTarnsactionID('ReserveSession');
                $objPointTransaction->fldtransactionnote = 'Booking Regular Points';
            
                $objPointTransaction->revoked = 0;
            
                $objPointTransaction->fldexpirydate = '2050-01-01';
                $resultPointTransaction = $objPointTransaction->save(); 

                $arrTransIds[] = $objPointTransaction->id; 

                if($objPromoCode !== null && $promoPoints > 0){
                   
                    $resultPointTransactionPromo = $this->PromoCode($objPromoCode-> fldpromocode,$objSession->id,$resultPointTransaction);
			//dd($objPromoCode);
                    $arrTransIds[] = $resultPointTransactionPromo->id;


                }

            }
            else
            {
                $RegularPointsAvailblilty = 0;
                $BonusPointDeduction = 0;

                //This will calculate the regular points availabile if promo is applicable or n/a 
                $RegularPointsAvailblilty = $objBalance->getBalanceWithoutBouns() + $promoPoints;

                $BonusPointDeduction = $objSession->fldsessionpoint - $RegularPointsAvailblilty;
             
                $RegularPointsDeduction = $RegularPointsAvailblilty;
                //dd($BonusPointDeduction.'----------------R'.$BonusPointDeduction);
                if($objPromoCode !== null && $promoPoints > 0){
                   
                    $resultPointTransactionPromo = $this->PromoCode($objPromoCode->fldpromocode,$objSession->id);

                    $arrTransIds[] = $resultPointTransactionPromo->id;
                }

                if($RegularPointsDeduction > 0)
                {
                    $objPointTransaction = new PointTransaction();
                    $objTransactionType = new TransactionType();
                    $objPointTransaction->user_id = $objUser->GetUserID();
                    $objPointTransaction->fldcost = 0;
            
                    $objPointTransaction->fldpoint = -($RegularPointsDeduction);
                    $objPointTransaction->transactionType_id = $objTransactionType->getTarnsactionID('ReserveSession');
                    $objPointTransaction->fldtransactionnote = 'Booking Regular Points';
                
                    $objPointTransaction->revoked = 0;
                
                    $objPointTransaction->fldexpirydate = '3000-01-01';
                    $resultPointTransaction = $objPointTransaction->save(); 

                    $arrTransIds[] = $objPointTransaction->id; 
                }

                if($BonusPointDeduction > 0)
                {
                    $objPointTransaction = new PointTransaction();
                    $objTransactionType = new TransactionType();
                    $objPointTransaction->user_id = $objUser->GetUserID();
                    $objPointTransaction->fldcost = 0;
        
                    $objPointTransaction->fldpoint = -($BonusPointDeduction);
                    $objPointTransaction->transactionType_id = $objTransactionType->getTarnsactionID('BookBouns');
                    $objPointTransaction->fldtransactionnote = 'Booking Bonus Points';
            
                    $objPointTransaction->revoked = 0;
                    $date = date("Y-m-d");
                    $ExpiryDate = date_create($date);
                

                    date_add($ExpiryDate ,date_interval_create_from_date_string("3 months"));
            
                    $objPointTransaction->fldexpirydate = $ExpiryDate;
                    $resultPointTransaction = $objPointTransaction->save(); 
                    $arrTransIds[] = $objPointTransaction->id;
                }



            }

    if(!$resultPointTransaction){
                return false;
            }
            //print($ResultobjInvitationCode[0]->pkInvitationCodeID);
            return $arrTransIds;
        

    }

    public function CancelSession(Request $request){

      $arr = array();
      $objBook = new Book();

      $obj = $objBook->getBook($request->intBookID);
      if($obj->book_type == 'Cash'){
          $objUser = new UserController();
          $objBookStatus = new BookStatus();
          $objSession = new Session();
          $objCash = new Cashtransaction();
          $intUserID = $objUser->GetUserID();
          if($obj->bookStatus_id == 1){
              $arrTransIds = array();
              $objUpdateBookStatus = $objBook->cancelSessionForCash($obj->session_id,$obj->id);
              $arrTransIds = json_decode($obj->cashTransaction_id); 
              $cashtrans = $objCash->where('id',$arrTransIds[0])->first();
              (count($arrTransIds) > 1) ? $cashtrans2 = $objCash->where('id',$arrTransIds[1])->first() : null ; 
              $objCash->user_id = $intUserID;
              $objCash->transactiontype_id = '14';
              $objCash->note = 'Cash refund for session cancellation by user';
              $objCash->merchant_reference = $cashtrans->merchant_reference; 
              (count($arrTransIds) > 1) ? $objCash->amount =  abs($cashtrans->amount + $cashtrans2->amount) : $objCash->amount = -($cashtrans->amount);
              $objCash->save();
              $arr = Status::mergeStatus($arr,200);
              return $arr;
          }
          $objUpdateBookStatus = $objBook->cancelSessionForCash($obj->session_id,$obj->id);
          $objNotifi = new NotifiController();
          $Notifi = $objNotifi->notifiMsg($obj->session_id,$obj->id,$objBookStatus->getBookStatusID('CANCELLED'));
          
          $objSession = new Session();
          $objSessionWithCancellation = $objSession->getSessionWithCancellation($obj->session_id);
          
          $CurrentDate = Carbon::now()->toDateString();
          $equation = date('Y-m-d', strtotime($objSessionWithCancellation->fldsessiondate.' - '.$objSessionWithCancellation['cancellationType']->fldmaxtime.' days'));
          if( $CurrentDate>$equation){
//               dd('stop1');
              $arr = array();
              $arrTransIds = array();
              $objTransactionType = new TransactionType();
              $objCashTransaction = new Cashtransaction();
              $arrTransIds = json_decode($obj->cashTransaction_id);
              $CashTransactionPromo =  $objCashTransaction->WhereIn('id',$arrTransIds)->where('transactionType_id',3)->first();
              $CashTransaction = $objCashTransaction->Where('id',$arrTransIds[0])->first();
              $cashDeduct = $objSessionWithCancellation['fldsessionprice'];
              if($CashTransactionPromo != null){
                  $cashDeduct = $objSessionWithCancellation['fldsessionprice'] - $CashTransactionPromo->amount;
              }
              $objCashTransaction->user_id = $intUserID;
              $objCashTransaction->transactiontype_id = '14';
              $objCashTransaction->amount = ($cashDeduct) - floor(($objSessionWithCancellation['cancellationType']->fldpointsdeducted /100) * ($cashDeduct));
              $objCashTransaction->note = 'Cash refund for session cancellation by user';
              $objCashTransaction->merchant_reference = $CashTransaction->merchant_reference;
              $resultCashTransaction = $objCashTransaction->save();
              if(!$resultCashTransaction){
                  $arr = Status::mergeStatus($arr,4012);
                  return $arr;
              }
              $arr = Status::mergeStatus($arr,200);
              return $arr;
          }else{
//               dd('stop2');
              $arrTransIds = array();
//               $objUpdateBookStatus = $objBook->cancelSessionForCash($obj->session_id,$obj->id);
              $arrTransIds = json_decode($obj->cashTransaction_id);
              $cashtrans = $objCash->where('id',$arrTransIds[0])->first();
              (count($arrTransIds) > 1) ? $cashtrans2 = $objCash->where('id',$arrTransIds[1])->first() : null ;
              $objCash->user_id = $intUserID;
              $objCash->transactiontype_id = '14';
              $objCash->note = 'Cash refund for session cancellation by user';
              $objCash->merchant_reference = $cashtrans->merchant_reference;
              (count($arrTransIds) > 1) ? $objCash->amount =  abs($cashtrans->amount + $cashtrans2->amount) : $objCash->amount = -($cashtrans->amount);
              $objCash->save();
              $arr = Status::mergeStatus($arr,200);
              return $arr;
          }         
      }else{
      if(!isset($request->skipPass) && $request->skipPass != 'Sk!P@SportAdmIn') 
      {  
            $objUser = new UserController();
             $intUserID = $objUser->GetUserID();
             //dd($obj);
            if($obj->user_id !== $intUserID){
              $arr = Status::mergeStatus($arr,5003);
              return $arr;
            }
            if($obj->bookStatus_id == 3){
              $arr = Status::mergeStatus($arr,4029);
              return $arr;
            }
      }
      else
      {
         $intUserID = $request->intUserID;
      }

	$objBookStatus = new BookStatus();
	$objSession = new Session();
	$objSessionWithCancellation = $objSession->getSessionWithCancellation($obj->session_id);
	$session = $objSession->getSessionDetails($obj->session_id);
      $objPointTransaction = new PointTransaction();
      if($obj->bookStatus_id == 1){
          $objUpdateBookStatus = $objBook->updateBookStatus($request->intBookID,$objBookStatus->getBookStatusID('CANCELLED'),$session);
        
	$ResultOfPointsBack = $objPointTransaction->updatePointTransByTransID(json_decode($obj->pointTransaction_id));
        $arr = Status::mergeStatus($arr,200);
        return $arr;
      }
      $objUpdateBookStatus = $objBook->updateBookStatus($request->intBookID,$objBookStatus->getBookStatusID('CANCELLED'),$session);
      $objNotifi = new NotifiController();
      $Notifi = $objNotifi->notifiMsg($session->id,$request->intBookID,$objBookStatus->getBookStatusID('CANCELLED'));
      //dd($objUpdateBookStatus);
      //dd($obj);
         
      $objPointTransaction = new PointTransaction();
     
      
      
      
       $mydate = date('Y-m-d H:i:s');

       /*$daystosum = $intExpiryDays;
       $datesum = date('Y-m-d', strtotime($mydate.' + '.$daystosum.' days'));       */

       $orig_timezone = new \DateTimeZone('UTC');

        $schedule_date = new \DateTime($mydate,$orig_timezone);
        $schedule_date->setTimeZone(new \DateTimeZone('Africa/Cairo'));
        $CurrentDate =  $schedule_date->format('Y-m-d H:i:s');
       
        $equation = date('Y-m-d', strtotime($objSessionWithCancellation->fldsessiondate.' - '.$objSessionWithCancellation['cancellationType']->fldmaxtime.' days'));
        
//dd($obj);
        if( $CurrentDate>$equation){
            
            $arr = array();
            
            $objTransactionType = new TransactionType();
            $objPointTransaction->user_id = $intUserID;
            $objPointTransaction->fldcost = 0;
         
             $Book = $objBook->where('id',$request->intBookID)->first();
             $pointTransaction =  DB::table('pointtransactions')->WhereIn('id',json_decode($Book->pointTransaction_id))->where('transactionType_id',3)->first();
             //dd($pointTransaction);
             //$objPointTransaction->select()->WhereIn('id',json_decode($Book->pointTransaction_id))->where('transactionType_id',3)->get();
             $pointDeduct = $objSessionWithCancellation['fldsessionpoint'];
             if($pointTransaction != null){
                 $pointDeduct = $objSessionWithCancellation['fldsessionpoint'] -$pointTransaction->fldpoint;
             }
            //dd( $r);
             $objPointTransaction->fldpoint = -floor(($objSessionWithCancellation['cancellationType']->fldpointsdeducted /100) * ($pointDeduct));
               //print(($objSessionWithCancellation['cancellationRule']->fldPointsDeducted /100) );       
            $objPointTransaction->transactionType_id = $objTransactionType->getTarnsactionID('CancellationFare');
            $objPointTransaction->fldtransactionnote = 'Cancel Session';
            
            
          
            $objPointTransaction->revoked = 0;
            $objPointTransaction->fldexpirydate = '2050-01-01';
            $resultPointTransaction = $objPointTransaction->save(); 
            if(!$resultPointTransaction){
                $arr = Status::mergeStatus($arr,4012);
                return $arr; 
            }
            
            $resultOfUpdate = $objPointTransaction->updatePointTrans($intUserID,json_decode($obj['pointTransaction_id']));

            if(!$resultOfUpdate){
                $arr = Status::mergeStatus($arr,4012);
                return $arr;    
            }
            $arr = Status::mergeStatus($arr,200);
            return $arr;
        }else{
            
            $ResultOfPointsBack = $objPointTransaction->updatePointTransByTransID(json_decode($obj->pointTransaction_id));

            $arr = Status::mergeStatus($arr,200);
            return $arr;
            
        }

    }
    }

}
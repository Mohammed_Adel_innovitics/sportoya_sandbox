<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\District;
use App\Status;
class DistrictController extends Controller
{
    public function ListDistrict(){
    	$arr = array();
    	$objDistrict = new District();
    	$arrDistrict = $objDistrict->listDistrict();
    	
    	
		$arr['results'] = $arrDistrict;
		$arr = Status::mergeStatus($arr,200);

		return $arr;


    }

    public function ListDistrictAttachedToCity(Request $request){
    	$arr = array();
    	$objDistrict = new District();
    	$arrDistrict = $objDistrict->listDistrictAttachedToCity($request->pkCityID);
    
		$arr['results'] = $arrDistrict;
		$arr = Status::mergeStatus($arr,200);
		return $arr;
    }

    public function AddDistrict(Request $request){
    	
		$arr = array();
		$input = $request->all();
        $sport = District::create($input);

		$arr['results'] = $sport;
		$arr = Status::mergeStatus($arr,200);
		return $arr;

    }

    public function DeleteDistrict(Request $request){
    	$arr = array();
    	$intDistrictID = $request->pkDistrictID;

    	$objDistrict = new District();

    	$result = $objDistrict->deleteDistrict($intDistrictID);	
    	if($result){
	    	$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,4012);
		}
		return $arr;
    }

    public function UpdateDistrict(Request $request){
		$arr = array();
		$intCityID = $request->fkCityID;
		$intDistrictID = $request->pkDistrictID;
		$strDistrictName = $request->fldDistrictName;
		$objDistrict = new District();
		$result = $objDistrict->updateDistrict($intDistrictID,$strDistrictName,$intCityID);    	
		if($result){
	    	$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,4012);
		}
		return $arr;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReviewProvider;
use App\Status;
use App\Provider;
use App\Http\Controllers\UserController;

class ReviewProviderController extends Controller
{
    //

    public function ListReviewAttachedToProvider(Request $request){
    		$arr = array();
    		$objReviewProvider = new ReviewProvider();
    		$arrReviewAttachedToProvider = $objReviewProvider->listReviewAttachedToProvider($request->pkProviderID);
    		$arr['results'] = $arrReviewAttachedToProvider;
    		$arr = Status::mergeStatus($arr,200);

    		return $arr;
    }
    public function ListReviewAttachedToProviderByToken(Request $request){
    		$arr = array();
    		$objUser = new UserController();
    		$intUserID = $objUser->GetUserID();
    		$objProvider = new Provider();
    		$intProviderID = $objProvider->getProviderIDByUserID($intUserID);
    		$objReviewProvider = new ReviewProvider();
//dd($intProviderID);
    		$arrReviewAttachedToProvider = $objReviewProvider->listReviewAttachedToProvider($intProviderID);
    		$arr['results'] = $arrReviewAttachedToProvider;
    		$arr = Status::mergeStatus($arr,200);

    		return $arr;
    }


}

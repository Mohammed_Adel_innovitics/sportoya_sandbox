<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;
use App\Status;
use App\Http\Controllers\UserController;
use App\Provider;
class LocationController extends Controller
{
    //
    public function ListLocation(){
    	$arr = array();
    	$objLocation = new Location();
    	$arrLocation = $objLocation->listLocation();
    	
		$arr['results'] = $arrLocation;
		$arr = Status::mergeStatus($arr,200);
    	return $arr;
    }
    public function ListLocationAttachedToProvider(Request $request){
    	$arr = array();
    	$objLocation = new Location();
    	$arrLocation = $objLocation->loadLocationByProviderID($request->provider_id);
    	
		$arr['results'] = $arrLocation;
		$arr = Status::mergeStatus($arr,200);
    	return $arr;
    }
    public function ListLocationAtttachedToProviderByToken(Request $request){
    	$arr = array();
    	$arr = array();
        $objUser = new UserController();
        $objProvider = new Provider();
        $intUserID = $objUser->GetUserID();
    	$objLocation = new Location();
    	$intProviderID = $objProvider->getProviderIDByUserID($intUserID);
//dd($intProviderID);
    	$arrLocation = $objLocation->loadLocationByProviderID($intProviderID);
    	
		$arr['results'] = $arrLocation;
		$arr = Status::mergeStatus($arr,200);
    	return $arr;
    }



    public function AddLocation(Request $request){
        $arr = array();
        $input = $request->all();
        $provider = Location::create($input);
		
		$arr['results'] = $provider;
		$arr = Status::mergeStatus($arr,200);

		return $arr;
    }

    public function DeleteLocation(Request $request){
    	$arr = array();
    	$intLocationID = $request->location_id;

    	$objLocation = new Loaction();

    	$result = $objLocation->deleteLoaction($intCityID);	
    	if($result){
	    	$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,4012);
		}
		return $arr;
    }

    public function UpdateLocation(Request $request){
		$arr = array();
		$intLocationID = $request->loaction_id;
		$strLatitude = $request->fldLatitude;
		$strLongitude = $request->fldLongitude;
		$strLocationName = $request->fldLocationName;
		$strLocationNameAddress = $request->fldLocationNameAddress;
		$strShortAddress = $request->fldShortAddress;
		$intProviderID = $request->provider_id;
        $intDistrictID = $request->fkDistrictID;
		$objLocation = new Location();
		$result = $objCity->updateLocation($intLocationID,$strLatitude,$strLongitude,$strLocationName,$strLocationNameAddress,$strShortAddress,$intProviderID);    	
		if($result){
	    	$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,4012);
		}
		return $arr;
    }

    public function GetLocationByID(Request $request){
    	$arr = array();
    	$intLocationID = $request->location_id;

    	$objLocation = new Location();
		$result = $objLocation->loadLocationByID($intCityID);    	
		if($result){
	    	
			$arr['status']['object'] = $result;
			$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,4012);
		}
		return $arr;	
    }




}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\Setting;

class SettingController extends Controller
{
    public function IsRegPromoCodeActive(){
        $arr = array();
        $objSetting = new Setting();
        $IsRegPromoCodeActive = $objSetting->checkRegPromoCodeEnable();
     $arr['result'] = $IsRegPromoCodeActive;
        if($IsRegPromoCodeActive == 1){
           
            $arr = Status::mergeStatus($arr,200);   
        }else{
            $arr = $arr = Status::mergeStatus($arr,4015);   
        }
        
        return $arr;

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ComplainType;
use App\Complain;
use App\Http\Controllers\UserController; 
use App\Status;
use App\Helpers\mailerHelper;
use App\User;

class ComplainController extends Controller
{
    public function AddComplain(Request $request){
    	$arr = array();
        $objComplainType = new ComplainType();
    	$objComplain = new Complain();
    	$objUser = new UserController(); 
        $user = new User();
        $UserData = $user->getUser($objUser->GetUserID());
        //dd($UserData);
    	$objComplain->complaintype_id = $objComplainType->getComplainTypeID($request['fldComplainType']);
    	$objComplain->fldcomplaindescription = $request['fldComplainDescription'];
    	$objComplain->user_id = $objUser->GetUserID();
        
        $result = $objComplain->save();
        $mail = mailerHelper::MailerSender("Sportoya.com - Complaint Handling","thank you using our services. Your feeadback has been submitted and smeone from our customer services will get back to you in the upcomming 24 hours.",$UserData['email'],$UserData['name'],"Complaints@sportoya.com");
        $arr = Status::mergeStatus($arr,200);
        return $arr;

    }

   
}

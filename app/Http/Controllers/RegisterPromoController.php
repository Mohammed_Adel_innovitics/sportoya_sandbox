<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RegisterPromo;
use App\Plan;
use App\Status;

class RegisterPromoController extends Controller
{
    public  function ListRegPromo(){
        $arr = array();
        $obj = new RegisterPromo();
        $result = $obj->listRegPromo();
        if ($result) {
           
            $arr['results'] = $result;
            $arr = Status::mergeStatus($arr,200);
        }else{
           $arr = Status::mergeStatus($arr,4012);
        } 
        return $arr;    
    }


     public function GetRegisterPromo($strRegisterPromo){
    	$arr = array();
        $obj = new RegisterPromo();
    	$result = $obj->getRegisterPromo($strRegisterPromo);
    	if ($result) {
            $arr = Status::mergeStatus($arr,200);
        }else{
           $arr = Status::mergeStatus($arr,4012);
        } 
        return $arr;
    }


    public function GetPlanAttachedToRegPromo(Request $request){
        $arr = array();
    	$objRegPromo = new RegisterPromo();
    	$ResultobjRegPromo = $objRegPromo->getRegisterPromo($request->fldRegPromoCode);
    	if($ResultobjRegPromo){
    		$objPlan = new Plan();
    		$arrPlan = $objPlan->getPlanAttachedToRegPromo($ResultobjRegPromo->pkRegPromoID);
    	
            $arr['results'] = $arrPlan;
            $arr = Status::mergeStatus($arr,200);

    	}else{
    		$arr = Status::mergeStatus($arr,4012);
    	}
    	return $arr;

    }



    public function DeleteRegPromo(Request $request){
        $arr = array();
        $intRegPromoID = $request->pkRegPromoID;

        $objRegisterPromo = new RegisterPromo();

        $result = $objRegisterPromo->deleteRegPromo($intRegPromoID);  
        if($result){
            $arr = Status::mergeStatus($arr,200);
            
        }else{
            $arr = Status::mergeStatus($arr,4012);
        }
        return $arr;
    }

    public function UpdateRegPromo(Request $request){
        $arr = array();
        /*$validator = Validator::make($request->all(), [
            'fldCancellationName' => 'required|unique:tbl_sport',
            
        ]);

        if ($validator->fails()) {
            $arr = Status::mergeStatus($arr,4007);   

            return $arr;

        }*/

        $intRegPromoID = $request->pkRegPromoID;
        $strRegPromoName = $request->fldRegPromoName;
        $intSessionID = $request->fkSessionID;
        $strPromoCode = $rrequest->fldPromoCode;
        
        $objPromoCode = new PromoCode();
        $result = $objPromoCode->updateRegPromo($intRegPromoID,$strRegPromoName,$strRegPromoText,$intRegPromoPoints,$intPlanID,$strExpiryDay);      
        if($result){
            $arr = Status::mergeStatus($arr,200);
            
        }else{
            $arr = Status::mergeStatus($arr,404);
        }
        return $arr;
    }



}

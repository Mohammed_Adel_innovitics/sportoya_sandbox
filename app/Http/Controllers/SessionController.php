<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Session;
use App\Provider;
use Validator;
use App\Note;
use App\Type;
use App\Http\Controllers\UserController;
use App\Book;
use App\Status;
use Faker\Provider\DateTime;
use App\Gallery;
use App\UserSportInterest;
use App\PointTransaction;
use App\Location;
use App\Http\Controllers\NotifiController;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use App\Cashtransaction;

class SessionController extends VoyagerBaseController
{
    //
        use BreadRelationshipParser ;
    public function listSession(){
        $arr = array();
        $objSession = new Session();

        $arrSession = $objSession->listSession();
        
        $arr['results'] = $arrSession;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function GetSessionDetails(Request $request){
        $arr = array();
        $intSessionID = $request->pkSessionID;
        $objUser = new UserController();
        $intUserID = $objUser->GetUserID();

        $objSession = new Session();
        $result = $objSession->getSession($intSessionID);
        $objBook = new Book();
        $IsBookedByUser =  $objBook->userIsBookedThisSession($intUserID,$intSessionID);
        $result['IsBookedByUser'] = $IsBookedByUser;     
        $result['availableSlots'] = $objSession->availableSlots($intSessionID);
        $arr['results'] = $result;
      
        $arr = Status::mergeStatus($arr,200);

        return $arr;
    }
    public function DeleteSession(Request $request){
        $arr = array();
        $intSessionID = $request->pkSessionID;

        $objSession = new Session();

        $result = $objSession->deleteSession($intSessionID);  
        if($result){
            $arr = Status::mergeStatus($arr,200);
            
        }else{
            $arr = Status::mergeStatus($arr,4012);
        }
        return $arr;
    }
    public function UploadSessionImg (Request $request){
       
            $image = $request->file('img');
            
            $imgName = time().'.jpg';
            
            $destinationPath = storage_path('/app/public/sessions/Mobile/');
            
            
            $image->move($destinationPath, $imgName);
            $arr = Status::mergeStatus($arr,200);
            return $arr;
            
            
       
    }
    public function AddSessionByProvider(Request $request){
        $arr = array();
        $objSession = new Session();
        $objSession->fldsessionname = $request->fldSessionName;
        $objSession->fldsessiondate = $request->fldSessionDate;
        $objSession->fldsessionduration = $request->fldSessionDuration;
        $objSession->fldfeaturedimage = 'sessions/Mobile/'.$request->arrSessionImg[0];
        $objSession->fldstarttime = $request->fldStartTime;
        $objSession->fldendtime = $request->fldEndTime;
        $objSession->sport_id = $request->fkSportID;
        $objSession->fldmaxslots = $request->fldMaxSlots;
        $objSession->isautobooked = $request->IsAutoBooked;
        
        $objSession->location_id = $request->fkLocationID;
        $objSession->cancellationType_id = $request->fkCancellationRuleID;
        $objSession->fldsessioncost = $request->fldSessionCost;
        $objSession->fldstatus = 'Pending';
//dd($request->note1.'-------second ------'.$request->note2);
        $objSession->note1 = $request->note1;
        $objSession->note2 = $request->note2;
        $arrImg = array();
        
        if($request->arrSessionImg !=[]){
            foreach($request->arrSessionImg as $obj){
               
                $obj = 'sessions/Mobile/'.$obj;
                $arrImg[] = $obj;
            }
            $objSession->images = json_encode($arrImg);
        }
       
        $result = $objSession->save();
        
      
            $arr = Status::mergeStatus($arr,200);
            return $arr;
    }

    public function filterSession(Request $request){
//dd($request->arrDate);
        $arr = array();
        $objSession = new Session();
        $arrSession = $objSession->FilterSession($request->sport_id,$request->session_id,$request->arrDate,$request->time_from,$request->time_to,$request->fkDistrictID);

        if(!empty($arrSession)){
            
            $arr['results'] = $arrSession;
            $arr = Status::mergeStatus($arr,200);
            
        }else{
            $arr = Status::mergeStatus($arr,404);
        }
        return $arr;   

    }

   

    

    public function FilterSessionByUserInterest(Request $request){
        $objSession = new Session();
        
        $objUser = new UserController();
        $intUserID = $objUser->GetUserID();

        $arrSession = $objSession->filterSessionByUserInterest($intUserID);

        $arr['results'] = $arrSession;
        $arr = Status::mergeStatus($arr,200);
        return $arr; 
        
    }

    public function UpcomingBookedSession(){
        $objUser = new UserController();
        $intUserID = $objUser->GetUserID();
        $arr = array();
        $objSession = new Session();
         $arrSession = $objSession->listUpcomingBookedSession($intUserID);
         
        $arr['results'] = $arrSession;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function GetSessionAttachedToProvider(Request $request){
        $arr = array();
        $intProviderID = $request->pkProviderID;
//dd($intProviderID);
        $objLocation = new Location();
        $arrLocationIDs = $objLocation->getLocationIDs($intProviderID);
//print_r($arrLocationIDs);die();
        $objSession = new Session();
      
        $arrSession = $objSession->getSessionAttachedToProvider($arrLocationIDs);
        if(!empty($arrSession)){
           
            $arr['results'] = $arrSession;
            $arr = Status::mergeStatus($arr,200);

            
        }else{
           $arr = Status::mergeStatus($arr,404);
        }
        return $arr;   
    }

    public function GetSessionUpComingAttachedToProviderByTokan(Request $request){
        $arr = array();
        $objUser = new UserController();
        $intUserID = $objUser->GetUserID();
        
        $objProvider = new Provider();

        $intProviderID = $objProvider->getProviderIDByUserID($intUserID);
        $objLocation = new Location();
        $arrLocationIDs = $objLocation->getLocationIDs($intProviderID);

        $objSession = new Session();

        $arrSession = $objSession->getSessionAttachedToProviderUpcoming($arrLocationIDs);
       
        if(!empty($arrSession)){
           
            $arr['results'] = $arrSession;
            $arr = Status::mergeStatus($arr,200);

        }else{
           $arr = Status::mergeStatus($arr,404);
        }
        return $arr;   
    }
    public function GetSessionPastAttachedToProviderByTokan(Request $request){
        $arr = array();
        $objUser = new UserController();
        $intUserID = $objUser->GetUserID();
        
       $objProvider = new Provider();
        
        $intProviderID = $objProvider->getProviderIDByUserID($intUserID);
        $objLocation = new Location();
        $arrLocationIDs = $objLocation->getLocationIDs($intProviderID);


        $objSession = new Session();

        $arrSession = $objSession->getSessionAttachedToProviderPast($arrLocationIDs);
       
        if(!empty($arrSession)){
           
            $arr['results'] = $arrSession;
            $arr = Status::mergeStatus($arr,200);

        }else{
           $arr = Status::mergeStatus($arr,404);
        }
        return $arr;   
    }
    public function GetSessionAttachedToProviderByTokan(Request $request){
        $arr = array();
        $objUser = new UserController();
        $intUserID = $objUser->GetUserID();
        //print($intUserID);
        $objProvider = new Provider();
        $intProviderID = $objProvider->getProviderIDByUserID($intUserID);
        $objLocation = new Location();
        $arrLocationIDs = $objLocation->getLocationIDs($intProviderID);
        $objSession = new Session();

        $arrSession = $objSession->getSessionAttachedToProvider($arrLocationIDs,"1");
        //dd($arrSession);
        if(!empty($arrSession)){
           
            $arr['results'] = $arrSession;
            $arr = Status::mergeStatus($arr,200);

        }else{
           $arr = Status::mergeStatus($arr,404);
        }
        return $arr;   
    }
    public function getAvailbleSlots(Request $request){
        $arr = array();
        $intSessionID = $request->pkSessionID;

        $objSession = new Session();
        $result = $objSession->availableSlots($intSessionID);
        return $result;

    }


    
    public function UpdateSessionByProvider(Request $request){

        $arr = array();
        $objSession = new Session();
        (isset($request->fldAutoBooked)) ? $auto = $request->fldAutoBooked : $auto = $request->IsAutoBooked ;
        $result = $objSession->updateSession($request->pkSessionID,$request->fldSessionName,$request->fldSessionDate,$request->fldSessionDuration,$request->fldStartTime,$request->fldEndTime,$request->fkSportID,$request->fkLocationID,$request->fkCancellationRuleID,$request->fldMaxSlots,$request->fldHoldSlots,$request->fldSessionCost,$auto,$request->arrSessionImg,$request->note1,$request->note2);
        if(!empty($request->arrNote)){ 
            $objNote = new Note();
            $resultNote = $objNote->deleteNote($request->pkSessionID,1);
            foreach($request->arrNote as $obj){
                $objNote = new Note();
                $objType = new Type();

                $objNote->fldNoteContent = $obj;
                
                $objNote->fkItemID = $request->pkSessionID;
                //dd($objType->getTypeID($obj['fldTypeName']));
                $objNote->type_id = $objType->getTypeID('Session');
                $resultAddNote = $objNote->save(); 
            }
        }
        
        $arr = Status::mergeStatus($arr,200);
          
        return $arr; 

    }
    public function SessionFilter(Request $request){

        $arr = array();
        $objSession = new Session();
        $arrSession = $objSession->FilterSessionforWeb($request->sport_id,$request->session_id,$request->arrDate,$request->time_from,$request->time_to);
        if(!empty($arrSession)){
            
            $arr['results'] = $arrSession;
            $arr = Status::mergeStatus($arr,200);
            
        }else{
            $arr = Status::mergeStatus($arr,404);
        }
        return $arr;   

    }


    public function ProviderUpdateSessionStatus(Request $request){
        $arr = array();
        $objBook = new Book();
        if(count($objBook->getBookAttendedTo($request->pkSessionID)) >0 ){
           
            
            $arr = Status::mergeStatus($arr,5008);
            return $arr;
           
        }
        $objBook = new Book();
        $arrBook = $objBook->where('session_id',$request->pkSessionID)->whereIn('bookStatus_id',['1','2'])->get();
        $arrBookIDs = array();
        foreach ($arrBook as $objbook){
            $arrBookIDs [] = $objbook->id ; 
            if($objbook->book_type == 'Cash'){
                $totalamount = 0;
                $objbook->cashTransaction_id = json_decode($objbook->cashTransaction_id);
                $transids = $objbook->cashTransaction_id;
                foreach ($transids as $index=>$trans){
                    $objCash = new Cashtransaction();
                    $transids[$index] = $objCash->where('id',$trans)->first();
                    $totalamount += $transids[$index]['amount'];
                }
                $objbook->cashTransaction_id = $transids;
                $objbook->amounttoberefund = abs($totalamount);
                $objCashTrans = new Cashtransaction();
                $objCashTrans->user_id = $objbook->user_id;
                $objCashTrans->amount = abs($totalamount);
                $objCashTrans->transactiontype_id = '14' ;
                $objCashTrans->note = "Cash Refund for session cancellation by provider";
                $objCashTrans->merchant_reference = $transids[0]['merchant_reference'];
                $refundResult = $objCashTrans->save() ;
            }else{
                $objbook->pointTransaction_id = json_decode($objbook->pointTransaction_id);
                $transids = $objbook->pointTransaction_id;
                $objPointTransaction = new PointTransaction();
                $resultRevoke = $objPointTransaction->updateGroupPointTrans($transids);
            }
        }
//         dd($arrBookIDs);
        $objNotifi = new NotifiController();
        $objNotifi->notifiMsg($request->pkSessionID,'',6);
        $resultUpdate = $objBook->updateBookedSessionToCancel($arrBookIDs);
        $objSession = new Session();
        $ResultUpdateStatus = $objSession->updateSessionStatusCancelled($request->pkSessionID);
            $arr = Status::mergeStatus($arr,200);
           return $arr; 
    }
    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);
        
        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        $this->authorize('browse', app($dataType->model_name));
        
        $getter = $dataType->server_side ? 'paginate' : 'get';
        
        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
        $searchable = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + 1;
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }
        
        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $query = $model::select('*');
            
            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');
            
            if ($search->value && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }
            
            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }
            
            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }
        
        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }
        
        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;
        
        // Check if a default search key is set
        $defaultSearchKey = isset($dataType->default_search_key) ? $dataType->default_search_key : null;
        
        $view = 'voyager::bread.browse';
        
        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }
        
        return Voyager::view($view, compact(
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchable',
            'isServerSide',
            'defaultSearchKey'
            ));
    }
    
    //***************************************
    //                _____
    //               |  __ \
    //               | |__) |
    //               |  _  /
    //               | | \ \
    //               |_|  \_\
    //
    //  Read an item of our Data Type B(R)EAD
    //
    //****************************************
    
    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }
        
        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);
        
        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');
        
        // Check permission
        $this->authorize('read', $dataTypeContent);
        
        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        
        $view = 'voyager::bread.read';
        
        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }
        
        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }
    
    //***************************************
    //                ______
    //               |  ____|
    //               | |__
    //               |  __|
    //               | |____
    //               |______|
    //
    //  Edit an item of our Data Type BR(E)AD
    //
    //****************************************
    
    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        $dataTypeContent = (strlen($dataType->model_name) != 0)
        ? app($dataType->model_name)->findOrFail($id)
        : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name
        
        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }
        
        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');
        
        // Check permission
        $this->authorize('edit', $dataTypeContent);
        
        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        
        $view = 'voyager::bread.edit-add';
        
        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        
        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }
    
    // POST BR(E)AD
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;
        
        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        
        // Check permission
        $this->authorize('edit', $data);
        
        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);
        
        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }
        
        if (!$request->ajax()) {
            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
            
            event(new BreadDataUpdated($dataType, $data));
            
            return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                'alert-type' => 'success',
                ]);
        }
    }
    
    //***************************************
    //
    //                   /\
    //                  /  \
    //                 / /\ \
    //                / ____ \
    //               /_/    \_\
    //
    //
    // Add a new item of our Data Type BRE(A)D
    //
    //****************************************
    
    public function create(Request $request)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        $this->authorize('add', app($dataType->model_name));
        
        $dataTypeContent = (strlen($dataType->model_name) != 0)
        ? new $dataType->model_name()
        : false;
        
        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }
        
        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');
        
        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        
        $view = 'voyager::bread.edit-add';
        
        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        
        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }
    
    /**
     * POST BRE(A)D - Store data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if( (isset($request['date']) && count($request['date']) > 0 ) && (isset($request['time']) && count($request['time']) > 0 ) && $request->hasfile('img')){
            $additional  = array();
            foreach ($request['date'] as $key => $objDate){
                $additional[$key]['date'] = $objDate ;
                $additional[$key]['time'] = $request['time'][$key] ;
                $additional[$key]['image'] = $request->file('img')[$key] ;
            }
        }
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        $this->authorize('add', app($dataType->model_name));
        
        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);
        
        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }
        
        if (!$request->has('_validate')) {
            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
            event(new BreadDataAdded($dataType, $data));
            if(isset($additional) && count($additional) > 0){
                foreach ($additional as $obj){
                    $saved = $this->UploadSessionImgRecurring($obj['image']);
                    if($obj['date'] != null && $obj['date'] != "" && $obj['time'] != "" && $obj['time'] != null){
                        $obj['date'] = explode(',',$obj['date']);
                        foreach ($obj['date'] as $datevalue) {
                            DB::table('sessions')->insert(['fldsessionname'=>$data['fldsessionname'],
                                'fldsessiondate'=>$datevalue,
                                'fldsessionduration'=>$data['fldsessionduration'],
                                'fldfeaturedimage'=>$saved,
                                'fldstarttime'=>$obj['time'],
                                'fldisfeaturehot'=>$data['fldisfeaturehot'],
                                'sport_id'=>$data['sport_id'],
                                'location_id'=>$data['location_id'],
                                'cancellationType_id'=>$data['cancellationType_id'],
                                'fldmaxslots'=>$data['fldmaxslots'],
                                'fldholdslots'=>$data['fldholdslots'],
                                'fldsessionpoint'=>$data['fldsessionpoint'],
                                'fldstatus'=>$data['fldstatus'],
                                'fldsessioncost'=>$data['fldsessioncost'],
                                'isautobooked'=>$data['isautobooked'],
                                'images'=>$data['images'],
                                'revoked'=>$data['revoked'],
                                'note1'=>$data['note1'],
                                'note2'=>$data['note2'],
                                'sportoyaSessionMargin'=>$data['sportoyaSessionMargin'],
                                'created_at'=>$data['created_at'],
                                'fldsessionprice'=>$data['fldsessionprice'],
                                'sportoyaSessionMarginPayNow'=>$data['sportoyaSessionMarginPayNow'],
                            ]);
                        }
                    }
                }
            }
            DB::table('sessions')->where('fldsessiondate',null)->delete();
            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => $data]);
            }
            
            return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                'alert-type' => 'success',
                ]);
        }
    }
    
    //***************************************
    //                _____
    //               |  __ \
    //               | |  | |
    //               | |  | |
    //               | |__| |
    //               |_____/
    //
    //         Delete an item BREA(D)
    //
    //****************************************
    
    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        $this->authorize('delete', app($dataType->model_name));
        
        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
            $this->cleanup($dataType, $data);
        }
        
        $displayName = count($ids) > 1 ? $dataType->display_name_plural : $dataType->display_name_singular;
        
        $res = $data->destroy($ids);
        $data = $res
        ? [
            'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
            'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                'alert-type' => 'error',
                ];
            
            if ($res) {
                event(new BreadDataDeleted($dataType, $data));
            }
            
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }
    
    /**
     * Remove translations, images and files related to a BREAD item.
     *
     * @param \Illuminate\Database\Eloquent\Model $dataType
     * @param \Illuminate\Database\Eloquent\Model $data
     *
     * @return void
     */
    protected function cleanup($dataType, $data)
    {
        // Delete Translations, if present
        if (is_bread_translatable($data)) {
            $data->deleteAttributeTranslations($data->getTranslatableAttributes());
        }
        
        // Delete Images
        $this->deleteBreadImages($data, $dataType->deleteRows->where('type', 'image'));
        
        // Delete Files
        foreach ($dataType->deleteRows->where('type', 'file') as $row) {
            if (isset($data->{$row->field})) {
                foreach (json_decode($data->{$row->field}) as $file) {
                    $this->deleteFileIfExists($file->download_link);
                }
            }
        }
    }
    
    /**
     * Delete all images related to a BREAD item.
     *
     * @param \Illuminate\Database\Eloquent\Model $data
     * @param \Illuminate\Database\Eloquent\Model $rows
     *
     * @return void
     */
    public function deleteBreadImages($data, $rows)
    {
        foreach ($rows as $row) {
            if ($data->{$row->field} != config('voyager.user.default_avatar')) {
                $this->deleteFileIfExists($data->{$row->field});
            }
            
            if (isset($row->details->thumbnails)) {
                foreach ($row->details->thumbnails as $thumbnail) {
                    $ext = explode('.', $data->{$row->field});
                    $extension = '.'.$ext[count($ext) - 1];
                    
                    $path = str_replace($extension, '', $data->{$row->field});
                    
                    $thumb_name = $thumbnail->name;
                    
                    $this->deleteFileIfExists($path.'-'.$thumb_name.$extension);
                }
            }
        }
        
        if ($rows->count() > 0) {
            event(new BreadImagesDeleted($data, $rows));
        }
    }
    
    /**
     * Order BREAD items.
     *
     * @param string $table
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function order(Request $request)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        $this->authorize('edit', app($dataType->model_name));
        
        if (!isset($dataType->order_column) || !isset($dataType->order_display_column)) {
            return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('voyager::bread.ordering_not_set'),
                'alert-type' => 'error',
            ]);
        }
        
        $model = app($dataType->model_name);
        $results = $model->orderBy($dataType->order_column, $dataType->order_direction)->get();
        
        $display_column = $dataType->order_display_column;
        
        $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->whereField($display_column)->first();
        
        $view = 'voyager::bread.order';
        
        if (view()->exists("voyager::$slug.order")) {
            $view = "voyager::$slug.order";
        }
        
        return Voyager::view($view, compact(
            'dataType',
            'display_column',
            'dataRow',
            'results'
            ));
    }
    
    public function update_order(Request $request)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        $this->authorize('edit', app($dataType->model_name));
        
        $model = app($dataType->model_name);
        
        $order = json_decode($request->input('order'));
        $column = $dataType->order_column;
        foreach ($order as $key => $item) {
            $i = $model->findOrFail($item->id);
            $i->$column = ($key + 1);
            $i->save();
        }
    }
    public function UploadSessionImgRecurring ($img){
        
        $fullFilename = null;
        $resizeWidth = 1800;
        $resizeHeight = null;
        $slug = 'sessions';
        $file = $img;
        
        $path = $slug.'/'.date('F').date('Y').'/';
        
        $filename = basename($file->getClientOriginalName(), '.'.$file->getClientOriginalExtension());
        $filename_counter = 1;
        
        // Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc...
        while (Storage::disk(config('voyager.storage.disk'))->exists($path.$filename.'.'.$file->getClientOriginalExtension())) {
            $filename = basename($file->getClientOriginalName(), '.'.$file->getClientOriginalExtension()).(string) ($filename_counter++);
        }
        
        $fullPath = $path.$filename.'.'.$file->getClientOriginalExtension();
        
        $ext = $file->guessClientExtension();
        
        if (in_array($ext, ['jpeg', 'jpg', 'png', 'gif'])) {
            $image = Image::make($file)
            ->resize($resizeWidth, $resizeHeight, function (Constraint $constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->encode($file->getClientOriginalExtension(), 75);
            
            // move uploaded file from temp to uploads directory
            if (Storage::disk(config('voyager.storage.disk'))->put($fullPath, (string) $image, 'public')) {
                $status = __('voyager::media.success_uploading');
                $fullFilename = $fullPath;
                
            } else {
                $status = __('voyager::media.error_uploading');
            }
        } else {
            $status = __('voyager::media.uploading_wrong_type');
        }
        
        // echo out script that TinyMCE can handle and update the image in the editor
        return $fullFilename;
        
        
        
    }

}

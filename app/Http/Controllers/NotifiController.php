<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifi;
use App\Status;
use App\Book;
use App\Session;
use App\Location;
use App\Helpers\mailerHelper;
use App\User;
use App\Http\Controllers\UserController;
use App\Mailer;

class NotifiController extends Controller
{
    //
    public function AddNotifi(Request $request){
        $arr = array();
        $objNotifi = new Notifi();
        $result = $objNotifi->addNotifi($request->msg);
        if($result){
            $arr = Status::mergeStatus($arr,200);
            return $arr;
        }else{
            $arr = Status::mergeStatus($arr,4012);
            return $arr;
        }
        
    }
    
    public function notifiMsg($intSessionID,$intBookID,$request_type){
        $objBook = new Book();
        
        $objLocation = new Location();
        $objSession = new Session();
        $session = $objSession->getSessionDetails($intSessionID);
        //dd($session);
        $Provider = $objLocation->getProvider($session->location_id);
        $user = new User();
        
        $objUser = new UserController();
        
        if($intBookID != null)
        {
            $Book = $objBook->getBook($intBookID);
            $UserData = $user->getUser($Book->user_id);
        }
        
        if($request_type == 5){
            
            $msg = 'Oops! '.$Provider->fldprovidername.' recommends you choose another session';
            
            $mailer = new Mailer();
            $result = $mailer->send('customer-care@sportoya.com',$UserData['email'],'9458272',[
                "product_name" => "Sportoya",
                "name" => $UserData['name'],
                "provider_name" => $Provider->fldprovidername,
                "session_name" => $session->fldsessionname,
                "support_url" => "mailto:customer-care@sportoya.com"
            ]);
            
        }else if($request_type == 6){
            
            
            $mailer = new Mailer();
            $objBook = new Book();
            //dd($arrBookUserIDs);
            $arrBookUserIDs = $objBook ->getTotalBookUserIDs($intSessionID);
            
            if($arrBookUserIDs == null || count($arrBookUserIDs) == 0)
            {
                return true;
            }
            //dd($arrBookUserIDs);
            foreach ($arrBookUserIDs as $bookID) {
                # code...
//                 $Book = $objBook->getBook($bookID);
                $UserData = $user->getUser($bookID);
                
                
                $result = $mailer->send('customer-care@sportoya.com',$UserData['email'],'11240248',[
                    "product_name" => "Sportoya",
                    "name" => $UserData['name'],
                    "session_name" => $session->fldsessionname,
                    "provider_name" => $Provider->fldprovidername,
                    "support_url" => "mailto:customer-care@sportoya.com"
                ]);
            }
            
            $arr = array();
            $objNotifi = new Notifi();
            
            $msg = $Provider->fldprovidername.' has just cancelled '.$session->fldsessionname;
            $result = $objNotifi->addMultiNotifi($msg, $arrBookUserIDs);
            
            
            if($result){
                return true;
            }else{
                return false;
            }
        }else if($request_type == 2){
            
            
            $msg = 'Get ready! Your spot in '.$session->fldsessionname.' is reserved for you';
            $session['book'] = $Book;
            
            $mailer = new Mailer();
            $result = $mailer->send('customer-care@sportoya.com',$UserData['email'],'9151970',[
                "product_name" => "Sportoya",
                "name" => $UserData['name'],
                "session_name" => $session->fldsessionname,
                "provider_name" => $Provider->fldprovidername,
                "confirmation_code" => $Book->fldverificationcode,
                "support_url" => "mailto:customer-care@sportoya.com"
            ]);
            
        }
        else if($request_type == 4){
            
            
            $msg = 'You have just cancelled Your spot in '.$session->fldsessionname.'';
            
            $mailer = new Mailer();
            $result = $mailer->send('customer-care@sportoya.com',$UserData['email'],'9153653',[
                "product_name" => "Sportoya",
                "name" => $UserData['name'],
                "session_name" => $session->fldsessionname,
                "support_url" => "mailto:customer-care@sportoya.com"
            ]);
            
        }
        else if($request_type == 7){
            $msg = '';
        }
        else{
            return true;
        }
        
        $arr = array();
        $objNotifi = new Notifi();
        $result = $objNotifi->addNotifi($msg, $Book->user_id);
        if($result){
            return true;
        }else{
            return false;
        }
    }
    
}

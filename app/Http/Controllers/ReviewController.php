<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use App\Http\Controllers\UserController;
use App\Status;

class ReviewController extends Controller
{
    
	public function AddReview(Request $request){
		$arr = array();
		$objUser = new UserController();
		$intUserID = $objUser->GetUserID();
		
		$objReview = new Review();
		$objReview->fldreviewtext = $request->fldReviewText;
		$objReview->fldreviewrate =$request->fldReviewRate;
		$objReview->user_id = $intUserID;
		$objReview->session_id = $request->fkSessionID;				
		$objReview->book_id = $request->fkBookID;

        $Review = $objReview->save();
        if(!$Review){
        	$arr = Status::mergeStatus($arr,4012);	
        	return $arr;
        }
        $arr = Status::mergeStatus($arr,200);
        return $arr;

	} 

}

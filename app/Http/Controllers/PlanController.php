<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;
use Validator;
use App\Status;
use App\UserDetails;
use App\Http\Controllers\UserController;
use App\RegisterPromo;

class PlanController extends Controller
{
    public function ListPlan(){
		$arr = array();
		$objPlan = new Plan();
		$arrPlan = $objPlan->ListPlan();

		//return $arrSport;

		$arr['results'] = $arrPlan;
		$arr = Status::mergeStatus($arr,200);
		return $arr;
	}

	
	public function AddPlan(Request $request){
		$arr = array();
		$validator = Validator::make($request->all(), [
            'fldPlanName' => 'required|unique:tbl_plan',
            
        ]);

        if ($validator->fails()) {
            $arr = Status::mergeStatus($arr,4007);

		return $arr;

        }
		
		$input = $request->all();
        $plan = Plan::create($input);

        
		$arr['results'] = $plan;
		$arr = Status::mergeStatus($arr,200);
		return $arr;
        	
	}


	public function DeletePlan(Request $request){
    	$arr = array();
    	$intPlanID = $request->pkPlanID;

    	$objPlan = new Plan();

    	$result = $objPlan->deletePlan($intPlanID);	
    	if($result){
	    	$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,4012);
		}
		return $arr;
    }

    public function UpdatePlan(Request $request){
		$arr = array();
		/*$validator = Validator::make($request->all(), [
            'fldPlanName' => 'required|unique:tbl_sport',
            
        ]);

        if ($validator->fails()) {
            $arr = Status::mergeStatus($arr,4007);

		return $arr;

        }*/

		$intPlanID = $request->pkPlanID;
		$strPlanName = $request->fldPlanName;
		$intPlanPoints = $request->fldPlanPoints;
		$strPlanCostEGP = $request->fldPlanCostEGP;
		$intFreezeDay = $request->fldFreezeDay;
		$intExpiryNumberMonth = $request->intExpiryNumberMonth;
		$strExpiryText = $request->fldExpiryText;

		$objPlan = new Plan();
		$result = $objPlan->updatePlan($intPlanID,$strPlanName,$intPlanPoints,$strPlanCostEGP,$intFreezeDay,$intExpiryNumberMonth,$strExpiryText);    	
		if($result){
	    	$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,4012);
		}
		return $arr;
    }

    public function GetPlanByID(Request $request){
    	$arr = array();
    	$intPlanID = $request->pkPlanID;

    	$objPlan = new Plan();
		$result = $objPlan->loadPlanByID($intPlanID);    	
		if($result){
	    	
			$arr['status']['object'] = $result;
			$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,4012);
		}
		return $arr;	
    }

    /*public function PlanAttachedToRegPromoID(){

    	$objUser = new UserController();
    	$intUserID = $objUser->GetUserID();
    	$objUserDetails = new UserDetails();
    	$intRegisterProID  = $objUserDetails->getRegPromoID($intUserID);
    	//dd($intRegisterProID);
    	
    	$objPlan = new Plan();
		$arrPlan = $objPlan->getPlanAttachedToRegPromo($intRegisterProID);
		$arr['results'] = $arrPlan;
		$arr = $arr = Status::mergeStatus($arr,200);

		return $arr;

    }*/




    public function GetPlanAttachedToRegPromoID(Request $request){
	$arr = array();
    	$strRegPromo = $request->RegPromo;
    	//dd($request->RegPromo);
    	$objRegPromo = new RegisterPromo();
    	$ResultRegPromo = $objRegPromo->getRegisterPromo($strRegPromo);
//dd($ResultRegPromo);
if($ResultRegPromo == null){

		 $arr = Status::mergeStatus($arr,5000);

		return $arr;
}
    	$objPlan = new Plan();

		$Plan = $objPlan->getPlanAttachedToRegPromoID($ResultRegPromo->id);
		if(count($Plan)>0){
			$arr['results'] = $Plan;
			 $arr = Status::mergeStatus($arr,200);	
		}else{
			
			 $arr = Status::mergeStatus($arr,4012);
		}
		

		return $arr;

    }


    public function BuyPlan(Request $request){

    	$objUser = new UserController();
    	$intUserID = $objUser->GetUserID();
    	$intPlanID = $request->pkPlanID;
    	$objPlan = new Plan();

    	$IsUserFitToBuyPlan = $objPlan->IsFitUserToBuyPlan($intRegisterProID,$intPlanID);

    	if($IsUserFitToBuyPlan){


    	}else{
    		//this Plan this not fit your regPromoCode
    	}

    }
	
}

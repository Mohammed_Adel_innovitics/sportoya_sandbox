<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Uploader;
use App\Status;

class UploadController extends Controller
{
    //

       public function uploadProfile(Request $request)
    {
    	$request->validate([	
    						'image' => 'required',
    						'file_name' => 'required',
						]);

    	$objUploader = new Uploader();

    	$r_response = $objUploader->uploadFileOfType($request->file('image'),$request->file_name,'P');
    
    	return ($r_response == false) ?  Status::printStatus(4032): Status::printStatus(200);
    }

      public function uploadSession(Request $request)
    {
    	$request->validate([	
    						'image' => 'required',
    						'file_name' => 'required',
						]);

    	$objUploader = new Uploader();

    	$r_response = $objUploader->uploadFileOfType($request->file('image'),$request->file_name,'S');
    
    	return ($r_response == false) ?  Status::printStatus(4032): Status::printStatus(200);
    }

}

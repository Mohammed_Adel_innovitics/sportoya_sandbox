<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\User;
use App\InvitationCode;
use App\RegisterPromo;
use Validator;
use App\Status;

class ValidationController extends Controller
{
    public function CheckEmailAvailability(Request $request)
    {
        $arr = array();
        $validator = Validator::make($request->all(), [
            
            'email' => 'required|email|unique:users',
            
            
        ]);

        if ($validator->fails()) {
            //return response()->json(['error'=>$validator->errors()], 401);            
            
            $arr['status']['code'] = '200';
            $arr['status']['message'] = $validator->errors();
            
            return $arr;


        }
       $arr = Status::mergeStatus($arr,200);
        

        return $arr;
    }


    public function CheckEmailAndInviationCode(Request $request)
    {
        $arr = array();
        $validator = Validator::make($request->all(), [
            
            'email' => 'required|email|unique:users',
            
            'fldInvitationCode'=> 'exists:invitationcodes'
            
        ]);
        if ($validator->fails()) {
            //return response()->json(['error'=>$validator->errors()], 401);            
            //$arr = $validator->errors(); 
            //dd($arr['messages']['name']);


           
            //print_r($validator->errors());die;
            $error = $validator->errors();
            
            if($error->first('email') !== null   && $error->first('fldInvitationCode') == null){
                $arr = Status::mergeStatus($arr,4016);
            }
            if($error->first('fldInvitationCode') !== null && $error->first('email') == null ){
                $arr = Status::mergeStatus($arr,4017);
            }
            if($error->first('fldInvitationCode') !== null && $error->first('email') !== null ){
                $arr = Status::mergeStatus($arr,4018);
            }
            return $arr;
        }

        $InvitationCode = new InvitationCode();

        if(isset($request->fldInvitationCode)||$request->fldInvitationCode !==null){
            $obj =$InvitationCode->getInvitationCode($request->fldInvitationCode);    
            if($obj!= NULL){
                $arr = Status::mergeStatus($arr,200);
          
        
            }else{
                $arr = Status::mergeStatus($arr,4013);
                
                
            }
        }


        
        $arr = Status::mergeStatus($arr,200);
        //dd($obj);
        

        return $arr;
    }
    public function CheckPhoneAvailability(Request $request){
      $arr = array();
      $validator = Validator::make($request->all(), [
            
            'fldPhoneNumber' => 'required|unique:phones',
            
        ]);

        if ($validator->fails()) {
            $arr = Status::mergeStatus($arr,4007);
        }else{
            $arr = Status::mergeStatus($arr,200);
        }  
        return $arr;

    }
    public function CheckRegisterCodeAvailability(Request $request){
        $arr = array();
        $RegisterPromo = new RegisterPromo();
        $obj =$RegisterPromo->getRegisterPromo($request->fldRegisterCode);
        //dd($obj->pkRegPromoID);
        if(count($obj)>0){
            $arr = Status::mergeStatus($arr,200);
        }else{
            $arr = Status::mergeStatus($arr,4014);  
            
        }

        return $arr; 

    }

}

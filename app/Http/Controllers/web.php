<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/listCity', 'CityController@ListCity');
Route::post('/addCity', 'CityController@AddCity');
Route::post('/deleteCity', 'CityController@DeleteCity');
Route::post('/updateCity', 'CityController@UpdateCity');

Route::post('/listDistrict', 'DistrictController@ListDistrict');
Route::post('/addDistrict', 'DistrictController@AddDistrict');
Route::post('/deleteDistrict', 'DistrictController@DeleteDistrict');
Route::post('/updateDistrict', 'DistrictController@UpdateDistrict');


Route::post('/listSport', 'SportController@ListSport');
Route::post('/listSportinterest', 'SportController@ListSportInterest');
Route::post('/listSportnavbar', 'SportController@ListSportNavBar');
Route::post('/addSport', 'SportController@AddSport');
Route::post('/deleteSport', 'SportController@DeleteSport');
Route::post('/updateSport', 'SportController@UpdateSport');





Route::post('/listSession', 'SessionController@ListSession');
Route::post('/addSession', 'SessionController@AddSession');

Route::post('/listLocation', 'LocationController@ListLocation');
Route::post('/addLocation', 'LocationController@AddLocation');
Route::post('/deleteLocation', 'LocationController@DeleteLocation');
Route::post('/updateLocation', 'LocationController@UpdateLocation');
Route::post('/listLocationAttachedToProvider', 'LocationController@ListLocationAtttachedToProvider');

Route::post('/listPlan', 'PlanController@ListPlan');
Route::post('/addPlan', 'PlanController@AddPlan');
Route::post('/deletePlan', 'PlanController@DeletePlan');
Route::post('/updatePlan', 'PlanController@UpdatePlan');

Route::post('/listRegPromo', 'RegisterPromoController@ListRegPromo');


Route::post('/listProvider', 'ProviderController@ListProvider');
Route::post('/listCancellationRule', 'CancellationRuleController@ListCancellationRule');
Route::post('/addCancellationRule', 'CancellationRuleController@AddCancellationRule');
Route::post('/updateCancellationRule', 'CancellationRuleController@UpdateCancellationRule');
Route::post('/deleteCancellationRule', 'CancellationRuleController@DeleteCancellationRule');



Route::post('/listPromoCode', 'PromoCodeController@ListPromoCode');
Route::post('/deletePromoCode', 'PromoCodeController@DeletePromoCode');
Route::post('/updatePromoCode', 'PromoCodeController@UpdatePromoCode');
Route::post('/addPromoCode', 'PromoCodeController@AddPromoCode');


Route::get('/home', 'HomeController@index')->name('home');

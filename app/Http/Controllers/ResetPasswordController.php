<?php

namespace App\Http\Controllers;
require '../PHPMailer-master/PHPMailerAutoload.php';
use Illuminate\Http\Request;
use App\ResetPassword;
use App\Status;
use Illuminate\Support\str;
use App\Http\Controllers\UserController;
use App\UserDetails;
use Illuminate\Mail\Mailable;
use Mail;
use App\Mailer;


class ResetPasswordController extends Controller
{
    
    public function AddResetPassword(Request $request){
        $arr = array();
        $orig_timezone = new \DateTimeZone('UTC');
        $schedule_date = new \DateTime(date('Y-m-d H:i:s', strtotime('now +1 hour')),$orig_timezone);
        $schedule_date->setTimeZone(new \DateTimeZone('Africa/Cairo'));

        $ExpiryDate =  $schedule_date->format('Y-m-d H:i:s');

        $objUser = new UserController();

        $checkEmail = $objUser->getUserByEmail($request->fldEmail);
        if(!$checkEmail){
            return Status::mergeStatus($arr,4030);      
        }
        
        $objResetPassword = new ResetPassword();
            $objResetPassword->user_id = $checkEmail->id;
            $objResetPassword->fldemail = $request->fldEmail;
            $objResetPassword->fldtoken = Str::random(60);
            $objResetPassword->fldexpirydate = $ExpiryDate;
            $objResetPassword->revoked = 0;
            $objResetPassword->save();
            $objUserDetails = new UserDetails();
            $UserDetails = $objUserDetails->getUserDetailsByID($objResetPassword->user_id);
            //dd($UserDetails);
            $mailer = new Mailer();
            $result = $mailer->send('customer-care@sportoya.com',$objResetPassword->fldemail,'9112382',[
                "product_name" => "Sportoya",
                "product_url" => "Sportoya",
                "name" => $UserDetails->fldfullname,
                "action_url" => "https://api.sportoya.com/reset?uid=".$objResetPassword->fldtoken,
                "support_url" => "mailto:customer-care@sportoya.com",
                "company_name" => "Sportoya"
                ]);

        

            if($result){
                return Status::mergeStatus($arr,200);   
            }
            return Status::mergeStatus($arr,4012);

    }


    public function CheckValidation(Request $request){
        $arr = array();
        $objResetPassword = new ResetPassword();
        //dd($request->fldToken);
        $resultObject = $objResetPassword->checkValidationToken($request->fldToken);
        if(!$resultObject){
            return Status::mergeStatus($arr,4031);  
        }
        //$arr['result'] = $resultObject;
        return Status::mergeStatus($arr,200);
         

    }

    public function ChangePassword(Request $request){
        $arr = array();
        $objResetPassword = new ResetPassword();
        $resultObject = $objResetPassword->checkValidationToken($request->fldToken);
        //dd($resultObject);
        if($resultObject){
            $objUser = new UserController();
            $result = $objUser->UpdateForgetpassword($request->fldPassword,$resultObject->user_id);
            $resultRevoke = $objResetPassword->revokedToken($request->fldToken);
            return Status::mergeStatus($arr,200);   
        }
        return Status::mergeStatus($arr,4012);
    }   

    
    
        
        
    
    
}

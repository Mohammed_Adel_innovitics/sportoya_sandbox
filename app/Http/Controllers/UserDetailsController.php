<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserDetails;
use App\Status;
use App\Http\Controllers\UserController;
use App\Phone;
use App\Uploader;
use DateTime;



class UserDetailsController extends Controller
{

	public function editUserProfile(Request $request){
		$objUser = new UserController();
		$intUserID = $objUser->GetUserID();
		$arr = array();
		$objUserDetails = new UserDetails();
		if(!isset($request->fldUserPhoto)){
			$request->fldUserPhoto = '';
		}
		$Result = $objUserDetails->updateUserProfile($intUserID,$request->fldFullName,$request->fldGender,$request->fldBirthDate,$request->fkCityID,$request->fldEmergencyName,$request->fldUserPhoto,$request->fldPhoneNumber); 
		if(!$request->fldPassword==null){
			$result = $objUser->UpdatePassword($request->fldPassword);
		}
		
		$objPhone = new Phone();
		$resultDeletePhone = $objPhone->deletePhone($intUserID);
		$objPhone->fldnumbertype = 'emergancyNumber';
		$objPhone->fldphonenumber = $request->fldPhoneNumber;
		$objPhone->user_id = $intUserID;
		$resultAdd = $objPhone->save();
			
		if($resultAdd){
			$arr = Status::mergeStatus($arr,200);
			return $arr;
		}else{
			$arr = Status::mergeStatus($arr,4012);
			return $arr;
		}	
		
	}

	public function editUserDetails(Request $request){
		$objUser = new UserController();
		$intUserID = $objUser->GetUserID();
		$arr = array();
		$objUserDetails = new UserDetails();
		$file_name = '';

		
		if($request->image != null)
		{
			$date = new DateTime();
            $currentDateTime = $date->getTimestamp();
			
			$file_name = $objUser->GetUserID().'-'.$currentDateTime.'.png';
			$objUploader = new Uploader();

    		$r_response = $objUploader->uploadFileOfType($request->file('image'),$file_name,'P');

		}
	

		$Result = $objUserDetails->updateUserProfile($intUserID,$request->fldFullName,$request->fldGender,$request->fldBirthDate,$request->fkCityID,$request->fldEmergencyName,$file_name,$request->fldPhoneNumber); 
		if(!$request->fldPassword==null){
			$result = $objUser->UpdatePassword($request->fldPassword);
		}


		
		$objPhone = new Phone();
		$objPhone->deleteEmergPhone($intUserID);
		$objPhone->fldnumbertype = 'emergancyNumber';
		$objPhone->fldphonenumber = $request->fldPhoneNumber;
		$objPhone->user_id = $intUserID;
		$resultAdd = $objPhone->save();

		if(isset($request->fldPhoneNumberM))
		{
			$objPhone = new Phone();
			if($objPhone->checkIfPhoneBelongsToUser($request->fldPhoneNumberM,$intUserID))
				{
					$arr = Status::mergeStatus($arr,200);
					return $arr;	
				}
		  
		   if($objPhone->checkIsNewPhone($request->fldPhoneNumberM))
			{
				$objPhone->deletePhone($intUserID);
				$objPhone->fldnumbertype = 'phoneNumber';
				$objPhone->fldphonenumber = $request->fldPhoneNumberM;
				$objPhone->user_id = $intUserID;
				$resultAdd = $objPhone->save();
			}
			else
			{
				$arr = Status::mergeStatus($arr,4020);
				return $arr;
			}
    	}


			
		if($resultAdd){
			$arr = Status::mergeStatus($arr,200);
			return $arr;
		}else{
			$arr = Status::mergeStatus($arr,4012);
			return $arr;
		}	
		
	}

	public function editEntityPassword(Request $request){
		$objUser = new UserController();
		$intUserID = $objUser->GetUserID();
		$arr = array();
		$objUserDetails = new UserDetails();
		

		if($request->fldPassword != null){
			$result = $objUser->UpdatePassword($request->fldPassword);

			if($result)
			{
				$arr = Status::mergeStatus($arr,200);
				return $arr;
			}
		}

			$arr = Status::mergeStatus($arr,4012);
			return $arr;
	
		
	}

	public function GetUserDetails(Request $request){
		$arr = array();
		$objUser = new UserController();
		$intUserID = $objUser->GetUserID();
		$objUserDetails = new UserDetails();
		$UserDetails = $objUserDetails->getUserDetailsByID($intUserID);
		$arr['results'] = $UserDetails;
		$arr = Status::mergeStatus($arr,200);
			return $arr;

	}
     
}

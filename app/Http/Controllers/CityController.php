<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\City;
use Validator;
use App\Status;

class CityController extends Controller
{
    //
    public function ListCity(){
    	$arr = array();
    	$objCity = new City();
    	$arrCity = $objCity->listCity();
    	
    	
		$arr['results'] = $arrCity;
		$arr = Status::mergeStatus($arr,200);
		return $arr;


    }

    public function AddCity(Request $request){
    	$arr = array();
		$validator = Validator::make($request->all(), [
            'fldCityName' => 'required|unique:tbl_city',
            
        ]);

        if ($validator->fails()) {
        	
        	$arr = Status::mergeStatus($arr,4007);
            return $arr;
        }
        $fldCityName = $request->fldCityName;
    	$objCity = new City();

    	$result = $objCity->addCity($fldCityName);

		if($result){
	    	$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,4012);
		}
		return $arr;
    }

    public function DeleteCity(Request $request){
    	$arr = array();
    	$intCityID = $request->pkCityID;

    	$objCity = new City();

    	$result = $objCity->deleteCity($intCityID);	
    	if($result){
	    	$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,4012);
		}
		return $arr;
    }

    public function UpdateCity(Request $request){
    	$arr = array();
    	$validator = Validator::make($request->all(), [
            'fldCityName' => 'required|unique:tbl_city',
            
        ]);
        if ($validator->fails()) {
            $arr = Status::mergeStatus($arr,4007);
			return $arr   ;        
        }
		$intCityID = $request->pkCityID;
		$strCityName = $request->fldCityName;
		$objCity = new City();
		$result = $objCity->updateCity($intCityID,$strCityName);    	
		if($result){
	    	$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,4012);
		}
		return $arr;
    }

    public function GetCityByID(Request $request){
    	$arr = array();
    	$intCityID = $request->city_id;

    	$objCity = new City();
		$result = $objCity->loadCityByID($intCityID);    	
		if($result){
	    	
			$arr['status']['object'] = $result;
			$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,4012);
		}
		return $arr;	
    }

}

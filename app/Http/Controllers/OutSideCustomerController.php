<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OutSideCustomer;
use App\Http\Controllers\BookController;
use App\Status;
class OutSideCustomerController extends Controller
{
    public function AddOutSideCustomer(Request $request){
	$arr = array();
    	$objBook = new BookController();

    	if(!$objBook->CheckParticipentAvailability(1,$request->fkSessionID)['result']){	
				//$arr['status']['code'] = '200';
					$arr = Status::mergeStatus($arr,4011);
					$arr['results'] = $objBook->CheckParticipentAvailability(1,$request->fkSessionID)['number'];
					return $arr;	 
		}
    	//$objOutSideCustomer = new OutSideCustomer();
    	$arr = array();
    	$input = $request->all();

    	$OutSideCustomer = new OutSideCustomer();
        $OutSideCustomer->fldname = $request->fldName;
        $OutSideCustomer->fldphone = $request->fldPhone;
        $OutSideCustomer->fldemail = $request->fldEmail;
        $OutSideCustomer->session_id = $request->fkSessionID;
        
        $result = $OutSideCustomer->save();
        if($result){
            $arr = Status::mergeStatus($arr,200);
        }else{
            $arr = Status::mergeStatus($arr,4012);    
        }
    
        return $arr;

    }
}

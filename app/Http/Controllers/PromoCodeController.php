<?php

namespace App\Http\Controllers;

require '../PHPMailer-master/PHPMailerAutoload.php';
use Illuminate\Http\Request;
use App\PromoCode;
use App\Status;

use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use TCG\Voyager\Facades\Voyager;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PromoCodeController extends VoyagerBaseController
{
	    use BreadRelationshipParser ;

    public function DonotHavePromoCode(Request $request){
		$arr = array();
		$result = $this->MailSenderToSportoya($request->fldEmail);
			if($result){
				$result1 = $this->MailSenderToClient($request->fldEmail);
			}
            if($result1){
                return Status::mergeStatus($arr,200);   
            }
            return Status::mergeStatus($arr,4012);	
	}
	public function MailSenderToSportoya($strEmail){
        $arr = array();
        $mail = new \PHPMailer();

        $htmlMail = '<html lang="en">
		<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>Sportoya - Password Reset</title>

	    <!-- Global stylesheets -->
	    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	    <link href="http://beta.sportoya.com/resetpassword/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	    <link href="http://beta.sportoya.com/resetpassword/assets/css/core.css" rel="stylesheet" type="text/css">
	    <link href="http://beta.sportoya.com/resetpassword/assets/css/components.css" rel="stylesheet" type="text/css">
	    <link href="http://beta.sportoya.com/resetpassword/assets/css/colors.css" rel="stylesheet" type="text/css">
	    <!-- /global stylesheets -->



	</head>

	<body style="background-image: url(../../);
	             background-color: #fff;">
	 
	    <div class="page-container">
	 
	        <div class="page-content">

	         
	            <div class="content-wrapper">
	 
	                <div class=" ">

	                    <div class="panel panel-white">
	                        <div class="panel-heading">
	                        <img src="http://beta.sportoya.com/resetpassword/assets/images/logo_demo.png" class="content-group mt-10" alt="" style="width: 190px;">  
	                    </div>
	                        <div class=" panel-heading" style="background-color: #00aaff;
	                                                        color: #fff;    ">
	                            <h6 class="panel-title">Subscription Request</h6>
	                             
	                        </div>
	                        <br/>
	                        <div class="panel-body no-padding-bottom">
	                            <div class="row">
	                                <div class="col-sm-6 content-group">
	                                    
	                                    <h2> Hi Sportoya Subscription Team,</h2>

	                                    
	                                    <h4> Someone have requested registration promo code.</h4>
	                                </br>
	                                    <span class="text-semibold">Email:   '.$strEmail.'</span>
	                                    <br/>
	                                    <br/>
	                                        <h4> This E-mail has been sent through Sportoya App as per visitor request. </h4>
	                                </div>

	                                
	                            </div>

	                            <div class="row">
	                                 

	                                 
	                            </div>
	                        </div>

	                         

	                        <div class="panel-body">
	                            <div class="row invoice-payment">
	                                <div class="col-sm-7">
	                                    <div class="content-group">
	                                        <h6>Cheers,</h6>
	                                        <div class="mb-15 mt-15">
	                                            <img src="http://beta.sportoya.com/resetpassword/assets/images/signature.png" class="display-block" style="width: 272px;" alt="">
	                                        </div>

	                                        
	                                    </div>
	                                </div>

	                                 
	                            </div>

	                             
	                        </div>
	                    </div>
	                    

	                </div>
	             
	            </div>
	             

	        </div>  
	    </div>
	     

	</body>
	</html>
	';

	        //$mail->Host = 'n3plcpnl0129.prod.ams3.secureserver.net';                       // Specify main and backup server

	        $mail->Host = 'mail.sportoya.com';
	        $mail->isSMTP();
	        $mail->SMTPOptions = array(
	    'ssl' => array(
	        'verify_peer' => false,
	        'verify_peer_name' => false,
	        'allow_self_signed' => true ));
	        //$mail->SMTPSecure = 'tls';
	        $mail->SMTPAuth = true;                             // Enable SMTP authentication
	        $mail->Username = 'no-reply@sportoya.com';                   // SMTP username
	        $mail->Password = 'no-reply#Sportoya';               // SMTP password
	        //$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
	        $mail->Port = 25;                                    //Set the SMTP port number - 587 for authenticated TLS
	        $mail->setFrom('no-reply@sportoya.com','SUBSCRIPTION REQUEST', 0);     //Set who the message is to be sent from
	        //$mail->addReplyTo('l@innovitics.com');  //Set an alternative reply-to address
	        // Add a recipient

			$mail->addAddress('subscription@sportoya.com');	 

	        //$mail->addAddress('subscription@sportoya.com');               // Name is optional
	        //$mail->addCC('cc@example.com');
	        //$mail->addBCC('bcc@example.com');
	        $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
	        //$mail->addAttachment('/usr/labnol/file.doc');         // Add attachments
	        //$mail->addAttachment('/images/image.jpg', 'new.jpg'); // Optional name
	        $mail->isHTML(true);                                  // Set email format to HTML

	        $mail->Subject = 'SUBSCRIPTION REQUEST';
	        $mail->Body    = $htmlMail;
	        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

	        //Read an HTML message body from an external file, convert referenced images to embedded,
	        //convert HTML into a basic plain-text alternative body
	        //$mail->msgHTML(file_get_contents('email_modern_brown.html'), dirname(__FILE__));
	        if(!$mail->send()) { 
	            //return false;
	            echo 'Mailer Error: ' . $mail->ErrorInfo;
	            
	        }else{
	            return true;
	        }

    }
    public function MailSenderToClient($strEmail){
        $arr = array();
        $mail = new \PHPMailer();

        $htmlMail = '<html lang="en">
		<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>Sportoya - Password Reset</title>

	    <!-- Global stylesheets -->
	    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	    <link href="http://beta.sportoya.com/resetpassword/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	    <link href="http://beta.sportoya.com/resetpassword/assets/css/core.css" rel="stylesheet" type="text/css">
	    <link href="http://beta.sportoya.com/resetpassword/assets/css/components.css" rel="stylesheet" type="text/css">
	    <link href="http://beta.sportoya.com/resetpassword/assets/css/colors.css" rel="stylesheet" type="text/css">
	    <!-- /global stylesheets -->



	</head>

	<body style="background-image: url(../../);
	             background-color: #fff;">
	 
	    <div class="page-container">
	 
	        <div class="page-content">

	         
	            <div class="content-wrapper">
	 
	                <div class=" ">

	                    <div class="panel panel-white">
	                        <div class="panel-heading">
	                        <img src="http://beta.sportoya.com/resetpassword/assets/images/logo_demo.png" class="content-group mt-10" alt="" style="width: 190px;">  
	                    </div>
	                        <div class=" panel-heading" style="background-color: #00aaff;
	                                                        color: #fff;    ">
	                            <h6 class="panel-title">Subscription Request</h6>
	                             
	                        </div>
	                        <br/>
	                        <div class="panel-body no-padding-bottom">
	                            <div class="row">
	                                <div class="col-sm-6 content-group">
	                                    
	                                    <h2> Hi,</h2>

	                                    
	                                    <h4> Thanks for your interest. We will send you a promo code very soon.</h4>
	                                </br>
	                                    <span class="text-semibold">Email:   '.$strEmail.'</span>
	                                    <br/>
	                                    <br/>
	                                        <h4> This E-mail has been sent through Sportoya App as per your request. </h4>
											<h5> If you did not request a promo code, please ignore this e-mail. </h5>
	                                </div>

	                                
	                            </div>

	                            <div class="row">
	                                 

	                                 
	                            </div>
	                        </div>

	                         

	                        <div class="panel-body">
	                            <div class="row invoice-payment">
	                                <div class="col-sm-7">
	                                    <div class="content-group">
	                                        <h6>Cheers,</h6>
	                                        <div class="mb-15 mt-15">
	                                            <img src="http://beta.sportoya.com/resetpassword/assets/images/signature.png" class="display-block" style="width: 272px;" alt="">
	                                        </div>

	                                        
	                                    </div>
	                                </div>

	                                 
	                            </div>

	                             
	                        </div>
	                    </div>
	                    

	                </div>
	             
	            </div>
	             

	        </div>  
	    </div>
	     

	</body>
	</html>
	';

	        //$mail->Host = 'n3plcpnl0129.prod.ams3.secureserver.net';                       // Specify main and backup server

	        $mail->Host = 'mail.sportoya.com';
	        $mail->isSMTP();
	        $mail->SMTPOptions = array(
	    'ssl' => array(
	        'verify_peer' => false,
	        'verify_peer_name' => false,
	        'allow_self_signed' => true ));
	        //$mail->SMTPSecure = 'tls';
	        $mail->SMTPAuth = true;                             // Enable SMTP authentication
	        $mail->Username = 'no-reply@sportoya.com';                   // SMTP username
	        $mail->Password = 'no-reply#Sportoya';               // SMTP password
	        //$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
	        $mail->Port = 25;                                    //Set the SMTP port number - 587 for authenticated TLS
	        $mail->setFrom('no-reply@sportoya.com','PROMO CODE REQUEST', 0);     //Set who the message is to be sent from
	        //$mail->addReplyTo('l@innovitics.com');  //Set an alternative reply-to address
	        // Add a recipient

			$mail->addAddress($strEmail);	 

	        //$mail->addAddress('subscription@sportoya.com');               // Name is optional
	        //$mail->addCC('cc@example.com');
	        //$mail->addBCC('bcc@example.com');
	        $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
	        //$mail->addAttachment('/usr/labnol/file.doc');         // Add attachments
	        //$mail->addAttachment('/images/image.jpg', 'new.jpg'); // Optional name
	        $mail->isHTML(true);                                  // Set email format to HTML

	        $mail->Subject = 'PROMO CODE REQUEST';
	        $mail->Body    = $htmlMail;
	        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

	        //Read an HTML message body from an external file, convert referenced images to embedded,
	        //convert HTML into a basic plain-text alternative body
	        //$mail->msgHTML(file_get_contents('email_modern_brown.html'), dirname(__FILE__));
	        if(!$mail->send()) { 
	            //return false;
	            echo 'Mailer Error: ' . $mail->ErrorInfo;
	            
	        }else{
	            return true;
	        }

    }
	public function ListPromoCode(){

		$objPromoCode = new PromoCode();
		$arrPromoCode = $objPromoCode->listPromoCode();

		$arr['result'] = $arrPromoCode;

		$arr = $arr = Status::mergeStatus($arr,200);

		return $arr;
	}

	public function AddPromoCode(Request $request){
		$arr = array();
		$input = $request->all();

        $objPromoCode = PromoCode::create($input);
		$arr['results'] = $objPromoCode;
		$arr = Status::mergeStatus($arr,200);

		return $arr;
	}
	public function DeletePromoCode(Request $request){
    	$arr = array();
    	$intPromoCodeID = $request->pkPromoCodeID;

    	$objPromoCode = new PromoCode();

    	$result = $objPromoCode->deletePromoCode($intPromoCodeID);	
    	if($result){
	    	$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,404);
		}
		return $arr;
    }

    public function UpdatePromoCode(Request $request){
		$arr = array();
		/*$validator = Validator::make($request->all(), [
            'fldCancellationName' => 'required|unique:tbl_sport',
            
        ]);

        if ($validator->fails()) {
        	$arr = Status::mergeStatus($arr,4007);   

			return $arr;

        }*/

		$intPromoCodeID = $request->pkPromoCodeID;
		$strPromoCodePoints = $request->fldPromoCodePoints;
		$intSessionID = $request->fkSessionID;
		$strPromoCode = $rrequest->fldPromoCode;
		
		$objPromoCode = new PromoCode();
		$result = $objPromoCode->updatePromoCode($intPromoCodeID,$strPromoCodePoints,$intSessionID,$strPromoCode);    	
		if($result){
	    	$arr = Status::mergeStatus($arr,200);
			
		}else{
			$arr = Status::mergeStatus($arr,404);
		}
		return $arr;
    }


    public function CheckPromoCode(Request$request){
    	$arr = array();
    	$strPromoCode = $request->fldPromoCode;
    	$intSessionID = $request->pkSessionID;
    	$objPromoCode = new PromoCode();

    	$resultOfPromoCode = $objPromoCode->getPromoCode($strPromoCode,$intSessionID);
		if($resultOfPromoCode == NULL){
			$arr = Status::mergeStatus($arr,4010);
			return $arr;

		}
		$arr['results'] = $resultOfPromoCode;
		$arr = Status::mergeStatus($arr,200);
		return $arr;		
    }
	public function create(Request $request)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        $this->authorize('add', app($dataType->model_name));
        
        $dataTypeContent = (strlen($dataType->model_name) != 0)
        ? new $dataType->model_name()
        : false;
        
        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }
        
        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');
        
        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        
        $view = 'voyager::bread.edit-add';
        $session = DB::table('sessions')->where('fldsessiondate','>',Carbon::now())->where('fldstatus','Live')->get();
//         dd($session);
        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        
        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','session'));
    }


}

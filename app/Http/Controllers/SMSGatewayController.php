<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SMSGateway;
use App\Phone;

class SMSGatewayController extends Controller
{
    //

    public function Send(Request $request){
        

        $SMS = new SMSGateway();

        $Phone = new Phone();
        $phoneNumber = $Phone->getUserPhoneNumber($request->userID);

        if($phoneNumber != false)
        {
        	$SMS->send($phoneNumber,$request->content);
        }


    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Provider;
use App\Status;

class ProviderController extends Controller
{
    public function ListProvider(){
    	$arr = array();
        $objProvider = new Provider();
    	$arrProvider = $objProvider->listProvider();
    	
		$arr['results'] = $arrProvider;
        $arr = Status::mergeStatus($arr,200);

		return $arr;
    }

    public function GetProviderByID(Request $request){
    	$arr = array();
        $intProviderID = $request->provider_id;
    	$objProvider = new Provider();
    	$obj = $objProvider->getProviderByID($intProviderID);
    	
		$arr['results'] = $obj;
        $arr = Status::mergeStatus($arr,200);

		return $arr;

    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

use Illuminate\Support\Facades\Auth;
use Validator;
use App\InvitationCode;
use App\Phone;
use App\Setting;
use App\Http\Controllers\PointTransactionController;
use App\RegisterPromo;
use App\Status;
use App\Provider;
use App\UserDetails;

use Illuminate\Support\Facades\DB;
use App\InvitationCodeTrans;
use App\Mailer;

class UserController extends Controller
{

public $successStatus = 200;

      public function login(Request $request){
        $objInvitationCode = new InvitationCode();
        $arr = array();
        if($request->fbid != ''){
          $objUserDetails = new UserDetails();
          $result = $objUserDetails->checkFBID($request->fbid);
          if(!$result){
            $arr = Status::mergeStatus($arr,5006);
            return $arr;
          }
          $ch = curl_init("https://graph.facebook.com/me?fields=id&access_token=".$request->fb_token);
          curl_setopt($ch, CURLOPT_HEADER, 0);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          $output = curl_exec($ch);       
          curl_close($ch);
          $output = json_decode($output); 
         // dd($output);

          if(!isset($output->error) && $request->fbid == $output->id){
            $user = new User();
            
            $objUser = new UserDetails();

            $obj = $objUser->getUserDetailsByFBID($request->fbid);
            //dd($obj);
            $user1 = $user->getUser($obj->user_id);
            $success['token'] =  $user1->createToken('MyApp')->accessToken;
            $success['fkUserLevelID'] = $user1->UserLevel_id;
            //dd($user);
            $success['userDetails'] =  $obj;
            $success['invitationCode'] = $objInvitationCode->getInvitationCodeOfUser($obj->user_id);
            $arr['results'] = $success;
            $arr = Status::mergeStatus($arr,200);
            return $arr;
          }
          else{
            $arr = Status::mergeStatus($arr,203);
            return $arr;
          }
        }

        
        //dd( request('email').request('password'));
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
           
            $objUser = new UserDetails();
           
            $objInvitationCode = new InvitationCode();
            //dd($user->id);
            $obj = $objUser->getUserDetailsByID($user->id);
            //dd($obj);
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            //dd($user);
            $success['fkUserLevelID'] = $user->UserLevel_id;
            //dd($success['fkUserLevelID']);
            $objProvider = new Provider();
            if($success['fkUserLevelID'] === '1'){
              $success['userDetails'] = $obj;
              $success['invitationCode'] = $objInvitationCode->getInvitationCodeOfUser($user->id);  
            
            }else if($success['fkUserLevelID'] === '2'){
              $success['userDetails'] = $objProvider->getProviderDetails($user->id);
                  
            }

            $success['userDetails']['email'] = request('email'); 
            $arr['results'] = $success;
            $arr = Status::mergeStatus($arr,200);
        }
        else{
            
            $arr = Status::mergeStatus($arr,401);
        }
        return $arr;
      }


    public function superAdminLogin(Request $request){
        $objInvitationCode = new InvitationCode();
        $arr = array();
        if($request->fbid != ''){
          $objUserDetails = new UserDetails();
          $result = $objUserDetails->checkFBID($request->fbid);
          if(!$result){
            $arr = Status::mergeStatus($arr,5006);
            return $arr;
          }
          $ch = curl_init("https://graph.facebook.com/me?fields=id&access_token=".$request->fb_token);
          curl_setopt($ch, CURLOPT_HEADER, 0);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          $output = curl_exec($ch);       
          curl_close($ch);
          $output = json_decode($output); 
         // dd($output);

          if(!isset($output->error) && $request->fbid == $output->id){
            $user = new User();
            
            $objUser = new UserDetails();

            $obj = $objUser->getUserDetailsByFBID($request->fbid);
            //dd($obj);
            $user1 = $user->getUser($obj->user_id);
            $success['token'] =  $user1->createToken('MyApp')->accessToken;
            $success['fkUserLevelID'] = $user1->UserLevel_id;
            //dd($user);
            $success['userDetails'] =  $obj;
            $success['invitationCode'] = $objInvitationCode->getInvitationCodeOfUser($obj->user_id);
            $arr['results'] = $success;
            $arr = Status::mergeStatus($arr,200);
            return $arr;
          }
          else{
            $arr = Status::mergeStatus($arr,203);
            return $arr;
          }
        }

        
        //dd( request('email').request('password'));
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
           
            $objUser = new UserDetails();
           
            $objInvitationCode = new InvitationCode();
            //dd($user->id);
            $obj = $objUser->getUserDetailsByID($user->id);
            //dd($obj);
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            //dd($user);
            $success['fkUserLevelID'] = $user->UserLevel_id;
            //dd($success['fkUserLevelID']);
            $objProvider = new Provider();
            if($success['fkUserLevelID'] === '1'){
              $success['userDetails'] = $obj;
              $success['invitationCode'] = $objInvitationCode->getInvitationCodeOfUser($user->id);  
            
            }else if($success['fkUserLevelID'] === '2'){
              $success['userDetails'] = $objProvider->getProviderDetails($user->id);
                  
            }

            $success['userDetails']['email'] = request('email'); 
            $arr['results'] = $success;
            $arr = Status::mergeStatus($arr,200);
        }
        else{
            
            $arr = Status::mergeStatus($arr,401);
        }
        return $arr;
      }

      public function superUserLogin(Request $request)
      {
          $arr = array();

          $usr = new User();
          $userId = $usr->getUserId($request->email);
          if(Auth::loginUsingId($userId) && request('password') == 'Sp0Rt9-3Mx-3kw72-MGWOq'){
            $user = Auth::user();
           
            $objUser = new UserDetails();
           
            $objInvitationCode = new InvitationCode();
            //dd($user->id);
            $obj = $objUser->getUserDetailsByID($user->id);
            //dd($obj);
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            //dd($user);
            $success['fkUserLevelID'] = $user->UserLevel_id;
            //dd($success['fkUserLevelID']);
            $objProvider = new Provider();
            if($success['fkUserLevelID'] === '1'){
              $success['userDetails'] = $obj;
              $success['invitationCode'] = $objInvitationCode->getInvitationCodeOfUser($user->id);  
            
            }else if($success['fkUserLevelID'] === '2'){
              $success['userDetails'] = $objProvider->getProviderDetails($user->id);
                  
            }

            $success['userDetails']['email'] = request('email'); 
            $arr['results'] = $success;
            $arr = Status::mergeStatus($arr,200);
        }
        else{
            
            $arr = Status::mergeStatus($arr,401);
        }
        return $arr;
      }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */

    public function register(Request $request)
    {
       $arr = array();
       $fkFBID ='';
      //check Register Code

      $objSetting = new Setting();
      $InvitationCode = new InvitationCode();
      
     // dd($objSetting->checkRegPromoCodeEnable());
        if($objSetting->checkRegPromoCodeEnable() == 1){
            $validator = Validator::make($request->all(), [
            'fldRegPromoText' => 'required',
            ]);

          if ($validator->fails()) {
             
              $arr = Status::mergeStatus($arr,4022);
              
              return $arr;

          } 

          $RegisterPromo = new RegisterPromo();
          $objResultRegisterPromo = $RegisterPromo->getRegisterPromo($request->fldRegPromoText);
          if($objResultRegisterPromo == NULL){
              $arr = Status::mergeStatus($arr,4010);
              
              return $arr;        
            
          }


      }

      //valide for(name,password,email)
      
        $validator = Validator::make($request->all(), [
            'name'=>'required',
            'email' => 'required|email|unique:users',
            'password'=> 'required|min:6',
            'fldInvitationCode'=>'exists:invitationcodes',
            'fldPhoneNumber'=>'required|unique:phones',

            
        ]);


        if ($validator->fails()) {
            //return response()->json(['error'=>$validator->errors()], 401);            
            $error = $validator->errors();
            
            if($error->first('name')){
                $arr = Status::mergeStatus($arr,4021);
            }
            if($error->first('email')){
                $arr = Status::mergeStatus($arr,4016);
            }
          //dd($error->first('fldPhoneNumber'));
            
            if($error->first('password')){
                $arr = Status::mergeStatus($arr,4019);
            }
            if($error->first('fldInvitationCode')){
                $arr = Status::mergeStatus($arr,4017);
            }
             if($error->first('fldPhoneNumber')){
                $arr = Status::mergeStatus($arr,4020);
            }

            return $arr;

        }

        if($request->fbid != ''){
          $objUserDetails = new UserDetails();

          $result = $objUserDetails->checkFBID($request->fbid);
          //dd($result);
          if($result == true){
            $arr = Status::mergeStatus($arr,5005);
            return $arr;
          }
          $ch = curl_init("https://graph.facebook.com/me?fields=id&access_token=".$request->fb_token);
          curl_setopt($ch, CURLOPT_HEADER, 0);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          $output = curl_exec($ch);       
          curl_close($ch);
          $output = json_decode($output); 
         // dd($output);

          if(!isset($output->error) && $request->fbid == $output->id){
            $fkFBID = $request->fbid;
            //dd($fkFBID);
          }else{
            $arr = Status::mergeStatus($arr,203);
            return $arr;
          }
        }
        //check Invitation Code  
        //dd($request->fldInvitationCode);
        
        if(isset($request->fldInvitationCode)||$request->fldInvitationCode!==null){
            
            $objInvitationCode = $InvitationCode->getInvitationCode($request->fldInvitationCode);
            if($objInvitationCode == NULL){
              $arr['status']['code'] = '200';
              $arr['status']['message'] = $validator->errors();
              return $arr;  
            }  
        }

        //Create User

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['UserLevel_id'] = 1;
        //dd($input['name']);
       /* $objUser = new User();
        $objUser->name = $input['name'];
        $objUser->password = $input['password'];
        $objUser->email = $input['email'];
        $objUser->save();*/
        $user = User::create($input);

        if(isset($request->fldInvitationCode)||$request->fldInvitationCode!==null){
            
            $objInvitationCodeTrans = new InvitationCodeTrans();
            $objInvitationCodeTrans->owner_user_id = $objInvitationCode->user_id;
            $objInvitationCodeTrans->consumer_user_id = $user->id; 
            $objInvitationCodeTrans->invitationCode_id = $objInvitationCode->id;
            $objInvitationCodeTrans->revoked = 0; 

            $objInvitationCodeTrans->save();
        }
        //Create User Details  
        $objUserDetails = new UserDetails();
        $objUserDetails->user_id = $user->id;
        $objUserDetails->fldFullName = $input['name'];
        $objUserDetails->fkFBID = $fkFBID;
      

        if($fkFBID != '')
        {
           $objUserDetails->flduserphoto = 'https://graph.facebook.com/'.$fkFBID.'/picture?type=large';
        }
        $resultUserDetails = $objUserDetails->save(); 
        //Create Phone  

        $sql = DB::table('users')->where('id',$user->id)->update(['UserLevel_id'=>1]);
        $objPhone = new Phone();
        $objPhone->fldNumberType = 'phoneNumber';
        $objPhone->fldPhoneNumber = $request->fldPhoneNumber;
        $objPhone->user_id = $user->id;
        $resultSavePhone = $objPhone->save();
        

        

        //create Point transaction for Reg Promo
        /*if(isset($input['fldRegPromoText'])||$input['fldRegPromoText']!=null){
          $objPointTransaction = new PointTransactionController();
          $resultPointTransaction = $objPointTransaction->RegPromo($input['fldRegPromoText'],$user->id);          
        }  */
          //dd($resultPointTransaction);
          
        //Create Invitation Code  
         $InvitationCodeController = new InvitationCodeController();
          $obj = $InvitationCodeController->AddInvitationCode($user->id,$objUserDetails->fldFullName);

        if(isset($request->fldInvitationCode)||$request->fldInvitationCode!=null){
            $objPointTransaction = new PointTransactionController();
            $resultPointTransaction = $objPointTransaction->InvitationCode($request->fldInvitationCode,$user->id);
        }


          $success['token'] =  $user->createToken('MyApp')->accessToken;
          
          $success['userDetails'] = $objUserDetails->getUserDetailsByID($user->id);
          
          $success['invitationCode'] = $InvitationCode->getInvitationCodeOfUser($user->id);
        
          $success['fkUserLevelID'] = 1;
   
            $mailer = new Mailer();
            $result = $mailer->send('customer-care@sportoya.com',$success['userDetails']->email,'9150643',[
                "product_name" => "Sportoya",
                "name" => $success['userDetails']->fldfullname,
                "invitation_code" => $success['invitationCode'],
                "support_email" => "mailto:customer-care@sportoya.com",
                "sender_name" => "Ahmed Hatem"
                ]);
         
          $arr['results'] = $success;
          $arr = Status::mergeStatus($arr,200);
        
            return $arr;
    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }


    public function GetUserID(){
      

      //$x = $request->x;
      $user = Auth::user();
      foreach($user->tokens as $token) {
            $id = $token->user_id;
            return $id;
        }
    }

public function Updatepassword($strPassword){
      $objUser =  DB::table('users')->where('id', $this->GetUserID())->update(['password' => bcrypt($strPassword)]);
        
        return $objUser;
    }
public function UpdateForgetpassword($strPassword,$intUserID){
    $objUser =  DB::table('users')->where('id', $intUserID)->update(['password' => bcrypt($strPassword)]);
      
      return $objUser;
}
public function getUserByEmail($strEmail){
//dd($strEmail);
  $objUser =  DB::table('users')->where('email', $strEmail)->first();
  //dd($objUser);
  return $objUser;
  
}

    
    
}


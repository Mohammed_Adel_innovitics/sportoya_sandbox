<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FortTrans;
use App\Status;

class FrontTransController extends Controller
{
    //
    
    public function fortToken(Request $request)
    {
        
        $request->validate([
            'device_id' => 'required',
        ]);
        
        
        $objFortTrans= new FortTrans();
        $r_response = $objFortTrans->generateFortToken($request['device_id']);
        
        return ($r_response == false) ?  Status::printStatus(4012): Status::mergeStatus($r_response,200);
    }
}

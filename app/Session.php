<?php

namespace App;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Sport;
use App\Note;
use App\cancellationType;
use App\Location;
use App\Book;
use DateTime;
use App\UserSportInterest;
use App\Http\Controllers\UserController;
use App\BookStatus;
use App\Provider;
use App\OutSideCustomer;
use App\Setting;
use App\Http\Controllers\NotifiController;
use App\Http\Controllers\SessionController;
use Carbon\Carbon;
use App\Http\Controllers\BalanceController;

class Session extends Model
{
    //
    protected $table = 'sessions';

    protected $fillable = ['id','fldsessionname','fldsessiondate','fldsessionduration','fldfeaturedimage','fldstarttime','fldendtime','fldisfeaturehot','sport_id','location_id','cancellationType_id','fldsessionpoint','fldmaxslots','fldholdslots','fldstatus','revoked','fldsessioncost','isautobooked','images','note1','note2','sportoyaSessionMargin','fldsessionprice','sportoyaSessionMarginPayNow'];

    public $intUserID;
    
    
    
    public function getSessionDetails($intSessionID){
        $objSession = Session::select('id','fldsessionname','fldsessiondate','fldsessionduration','fldfeaturedimage','fldstarttime','fldendtime','fldisfeaturehot','sport_id','location_id','cancellationType_id','fldsessionpoint','fldmaxslots','fldholdslots','fldstatus','revoked','fldsessioncost','isautobooked','images','note1','note2','sportoyaSessionMargin','fldsessionprice','sportoyaSessionMarginPayNow')->where('id',$intSessionID)->with('sport')->with('location')->with('cancellationType')->first();
        return $objSession;
    }
    public function getSession($intSessionID){
        $objSession = Session::select('id','fldsessionname','fldsessiondate','fldsessionduration','fldfeaturedimage','fldstarttime','fldendtime','fldisfeaturehot','sport_id','location_id','cancellationType_id','fldsessionpoint','fldmaxslots','fldholdslots','fldstatus','revoked','fldsessioncost','isautobooked','images','note1','note2','sportoyaSessionMargin','fldsessionprice','sportoyaSessionMarginPayNow')->where('id',$intSessionID)->with('sport')->with('location')->with('cancellationType')->first();
       
        //dd($arrSession->toSql());
    //dd($objSession->pkSessionID);
       $objSession->fldavailableslots = "".$this->availableSlots($objSession->id);
//dd($objSession->fldAvailableSlots);
			$objSetting = new Setting();
	            $objSession->fldfeaturedimage = $objSetting->getMediaUrl().$objSession->fldfeaturedimage;

       if($objSession->images !== null && $objSession->images !== ''){
                //dd($objSession->images);
                
                
               $objSession->images = json_decode($objSession->images);
                
                $objSetting = new Setting();
                $mediaUrl = $objSetting->getMediaUrl();
                 $images = array();
                foreach($objSession->images as $objGallery){
                    
                    $objGallery = $mediaUrl.$objGallery;
                    $images[] = $objGallery;
                } 
                $objSession->images = array();
                $objSession->images = $images;
        }

             $date = new DateTime($objSession->fldsessiondate);
             $dateStart = new DateTime($objSession->fldstarttime);
            
            $objSession->fldsessiondate = $date->format("D, j M Y");
            $objSession->fldstarttime = $dateStart->format("H:i");
    
        return $objSession;
    }

    

    public function checkSession($intSessionID){
       $objSession = $this->where('id',$intSessionID)->get();
       
       if($objSession == null){
        return false;
       }
    
        return $objSession;
    }

    public function filterSessionByUserInterest($intUserID){
        $objUserSportInterest = new UserSportInterest();
        $arrSportIDs = $objUserSportInterest->getUserSportIDs($intUserID);
        //dd($arrSportIDs);
       /* $mydate = date('Y-m-d H:i:s');
        $orig_timezone = new \DateTimeZone('UTC');
        $schedule_date = new \DateTime($mydate,$orig_timezone);
        $schedule_date->setTimeZone(new \DateTimeZone('Africa/Cairo'));
        $currentDateTime = $schedule_date->format('Y-m-d H:i:s');*/

         $carbonNow = Carbon::now();
        $currentDateTime = $carbonNow->toDateTimeString();

        $arrSession = Session::select('id','fldsessionname','fldsessiondate','fldsessionduration','fldfeaturedimage','fldstarttime','fldendtime','fldisfeaturehot','sport_id','location_id','cancellationType_id','fldsessionpoint','fldmaxslots','fldholdslots','fldstatus','revoked','fldsessioncost','isautobooked','images','note1','note2','sportoyaSessionMargin','fldsessionprice','sportoyaSessionMarginPayNow')->where('fldstatus','Live')->where(DB::raw('cast(concat(`fldsessiondate`, " ",`fldstarttime`) as datetime)'),'>=',$currentDateTime)->with('location')->with('sport')->with('cancellationType')->whereIn('sport_id', $arrSportIDs)->orderBy('fldsessiondate', 'ASC')->orderBy('fldstarttime', 'ASC')->paginate(10);
        
       foreach($arrSession as $objSession){
            
            $objBook = new Book();
            $objLocation = new Location();
            $objProvider = new Provider();
            $objSetting = new setting();
            $objSession->fldfeaturedimage = $objSetting->getMediaUrl().$objSession->fldfeaturedimage;
            $date = new DateTime($objSession->fldsessiondate);
             $dateStart = new DateTime($objSession->fldstarttime);
            
            $objSession->fldsessiondate = $date->format("D,j M Y");
            $objSession->fldstarttime = $dateStart->format("H:i");


//print($objSession->provider->pkProviderID . '-' );
//dd($objSession->provider->pkProviderID);
$objSession->provider12 = '';

            $objSession->provider12 = $objProvider->getProvider($objSession->location_id);
 unset($objSession->provider);

$objSession->provider = $objSession->provider12;
unset($objSession->provider12);
//dd($objSession->provider);
            $objSession->fldavailableslots = "".$this->availableSlots($objSession->id);
            $Result = $objBook->userBookedSession($intUserID,$objSession->id);
            if($objSession->images !== null && $objSession->images !== ''){
                    //dd($objSession->images);

                    
                $objSession->images = json_decode($objSession->images);
                
                $objSetting = new Setting();
                $mediaUrl = $objSetting->getMediaUrl();
                 $images = array();
                foreach($objSession->images as $objGallery){
                    
                    $objGallery = $mediaUrl.$objGallery;
                    $images[] = $objGallery;
                } 
                $objSession->images = array();
                $objSession->images = $images;
                
                //dd($objSession->images);
            }
            if(!$Result){
                $objSession->IsBookedByUser = false;
            }else{
                $objBookStatus = new BookStatus();
                $objSession->IsBookedByUser = true;
                $objSession->booking = $Result;
                $objSession->booking->BookStatus = $objBookStatus->getBookStatusByID($Result->bookStatus_id);
            }
            $objBalance = new Balance();
            $objSession->totalpoints = $objBalance->getBalance();
        }
        //dd($arrSession);
        
        return $arrSession;

    }

public function FilterSession($intSportID,$intSessionID,$arrDate,$strStartTime,$strEndTime,$intDistrictID){
        $objLocation = new Location();
        $objUser = new UserController();
        $intUserID = $objUser->GetUserID();
     
        $carbonNow = Carbon::now();
        $currentDateTime = $carbonNow->toDateTimeString();

        $session = Session::select('id','fldsessionname','fldsessiondate','fldsessionduration','fldfeaturedimage','fldstarttime','fldendtime','fldisfeaturehot','sport_id','location_id','cancellationType_id','fldsessionpoint','fldmaxslots','fldholdslots','fldstatus','revoked','fldsessioncost','isautobooked','images','note1','note2','sportoyaSessionMargin','fldsessionprice','sportoyaSessionMarginPayNow')->where('fldstatus','Live')->where(DB::raw('cast(concat(`fldsessiondate`, " ",`fldstarttime`) as datetime)'),'>=',$currentDateTime)->with('location')->with('sport')->with('provider')->with('cancellationType')->orderBy('fldsessiondate', 'ASC')->orderBy('fldstarttime', 'ASC');
      

        if($intSportID != 1 && $intSportID !=2 && $intSportID != null){

            $session->where('sport_id', $intSportID);
        }
        if($intSportID == 2){
            
           $arrSession = $this->filterSessionByUserInterest($intUserID); 

           return $arrSession;
        }
//dd($intDistrictID);
        if($intDistrictID !== null){

           $arrLocationIDs = $objLocation->getLocationAttachedToDistrict($intDistrictID); 
        
           //dd($arrLocationIDs);
           $session->whereIn('location_id',$arrLocationIDs);
       
       
        }
        if($intSessionID !== null){
           $session->where('id','=',$intSessionID);
        }
//print_r($arrDate);
        if($arrDate != null){
            $session->whereIn('fldsessiondate', $arrDate);
        }
        if($strStartTime !== null){
            $session->Where('fldstarttime','>=',$strStartTime);
            $session->Where('fldstarttime','<=',$strEndTime);
        }
        /*if($strEndTime !== null){
            $session->where('fldEndTime','<=',$strEndTime);   
        }*/

       //dd($session->toSql());
        //$arrSession = $session->toSql();
        $arrSession = $session->paginate(10);

        //dd($arrSession);

        
         foreach($arrSession as $objSession){
            if($objSession->images !== null && $objSession->images !== ''){
                    //dd($objSession->images);

                    
                $objSession->images = json_decode($objSession->images);
                
                $objSetting = new Setting();
                $mediaUrl = $objSetting->getMediaUrl();
                 $images = array();
                foreach($objSession->images as $objGallery){
                    
                    $objGallery = $mediaUrl.$objGallery;
                    $images[] = $objGallery;
                } 
                $objSession->images = array();
                $objSession->images = $images;
                
                //dd($objSession->images);
            }
            $objBook = new Book();
            $objLocation = new Location();
            $objProvider = new Provider();
            $objSetting = new Setting();     
            $objSession->fldfeaturedimage = $objSetting->getMediaUrl().$objSession->fldfeaturedimage;
            $date = new DateTime($objSession->fldsessiondate);
             $dateStart = new DateTime($objSession->fldstarttime);
            
            $objSession->fldsessiondate = $date->format("D, j M Y");
            $objSession->fldstarttime = $dateStart->format("H:i");


//print($objSession->provider->pkProviderID . '-' );
//dd($objSession->provider->pkProviderID);
$objSession->provider12 = '';

            $objSession->provider12 = $objProvider->getProvider($objSession->location_id);
 unset($objSession->provider);

$objSession->provider = $objSession->provider12;
//dd($objSession->provider);
unset($objSession->provider12);
//dd($objSession->provider);
		
		
		
            $objSession->fldavailableslots = "".$this->availableSlots($objSession->id);
            $Result = $objBook->userBookedSession($intUserID,$objSession->id);
             
          
            
            if(!$Result){
                $objSession->IsBookedByUser = false;
            }else{
                $objBookStatus = new BookStatus();
                $objSession->IsBookedByUser = true;
                $objSession->booking = $Result;
                $objSession->booking->BookStatus = $objBookStatus->getBookStatusByID($Result->bookStatus_id);
            }
            $objBalance = new Balance();
            $objSession->totalpoints = $objBalance->getBalance();
        }
        //dd($arrSession);
        
        return $arrSession;
    }

    

    public function updateSession($pkSessionID,$fldSessionName,$fldSessionDate,$fldSessionDuration,$fldStartTime,$fldEndTime,$fkSportID,$fkLocationID,$fkcancellationTypeID,$fldMaxSlots,$fldHoldSlots,$intSessionCost,$IsAutoBooked,$images,$note1,$note2){
        if($images !== null || !empty($images) ){
            foreach($images as $obj){
                $obj = 'sessions/Mobile/'.$obj;
                $arrImg[] = $obj;
            }
            $objSession =  DB::table('sessions')->where('id', $pkSessionID)
            ->update(['fldsessionname' => $fldSessionName,'fldsessiondate'=>$fldSessionDate,'fldsessionduration'=>$fldSessionDuration,'fldstarttime'=>$fldStartTime,'fldendtime'=>$fldEndTime,'sport_id'=>$fkSportID,'location_id'=>$fkLocationID,'cancellationType_id'=>$fkcancellationTypeID,'fldmaxslots'=>$fldMaxSlots,'fldholdslots'=>$fldHoldSlots,'fldsessioncost'=>$intSessionCost,'isautobooked'=>$IsAutoBooked,'images'=>json_encode($arrImg),'note1'=>$note1,'note2'=>$note2]);
            
        }else{
            $objSession =  DB::table('sessions')->where('id', $pkSessionID)
            ->update(['fldsessionname' => $fldSessionName,'fldsessiondate'=>$fldSessionDate,'fldsessionduration'=>$fldSessionDuration,'fldstarttime'=>$fldStartTime,'fldendtime'=>$fldEndTime,'sport_id'=>$fkSportID,'location_id'=>$fkLocationID,'cancellationType_id'=>$fkcancellationTypeID,'fldmaxslots'=>$fldMaxSlots,'fldholdslots'=>$fldHoldSlots,'fldsessioncost'=>$intSessionCost,'isautobooked'=>$IsAutoBooked,'note1'=>$note1,'note2'=>$note2]);
        }
        
    return $objSession;


    }

    public function updateSessionWithoutImg($pkSessionID,$fldSessionName,$fldSessionDate,$fldSessionDuration,$fldStartTime,$fldEndTime,$fldIsFeatureHot,$fkSportID,$fkLocationID,$fkcancellationTypeID,$fldSessionPoint,$fldMaxSlots,$fldHoldSlots,$note1,$note2){

         $objSession =  DB::table('sessions')->where('id', $pkSessionID)
          ->update(['fldsessionname' => $fldSessionName,'fldsessiondate'=>$fldSessionDate,'fldsessionduration'=>$fldSessionDuration,'fldstarttime'=>$fldStartTime,'fldendtime'=>$fldEndTime,'fldisfeaturehot'=>$fldIsFeatureHot,'sport_id'=>$fkSportID,'location_id'=>$fkLocationID,'cancellationType_id'=>$fkcancellationTypeID,'fldsessionpoint'=>$fldSessionPoint,'fldmaxslots'=>$fldMaxSlots,'fldholdslots'=>$fldHoldSlots,'note1'=>$note1,'note2'=>$note2]);
        return $objSession;

    }
    public function revokedSession($intSessionID){
        $objSession = DB::table('sessions')->where('id',$intSessionID)->update(['revoked'=>1]);
        return $objSession;
    }

    public function getSessionIDs($intProviderID){
        
    }

    public function availableSlots($intSessionID){
        $objSession = Session::select('fldmaxslots','fldholdslots')->where('id',$intSessionID)->first();

        $objBook = new Book();

        $BookCount = $objBook->getCountBookedOnSession($intSessionID);
        //dd($arrBook);
        $TotalBooked =  $BookCount;

        $objOutSide = new OutSideCustomer();
        $TotalOutSide = $objOutSide->getCountoutSideOnSession($intSessionID);
        $TotalParticipent = $TotalBooked + $TotalOutSide;     
        $availableSlots = ($objSession->fldmaxslots - $objSession->fldholdslots)-$TotalParticipent;

        return $availableSlots;


    }


    public function getSessionAttachedToProvider($arrLocationIDs,$extendedDays = NULL){
        $objUser = new UserController();
        $intUserID = $objUser->GetUserID();
        
        $carbonNow = Carbon::now();
    
        
        if($extendedDays == NULL)
        {       $currentDateTime = $carbonNow->toDateTimeString(); }
        else
        {
         $carbonNow =  Carbon::now()->subDays($extendedDays);     
         $currentDateTime = $carbonNow->toDateTimeString(); }

         $arrSession = Session::select('id','fldsessionname','fldsessiondate','fldsessionduration','fldfeaturedimage','fldstarttime','fldendtime','fldisfeaturehot','sport_id','location_id','cancellationType_id','fldsessionpoint','fldmaxslots','fldholdslots','fldstatus','revoked','fldsessioncost','isautobooked','images','note1','note2','sportoyaSessionMargin','fldsessionprice','sportoyaSessionMarginPayNow')->with('location')->with('sport')->with('cancellationType')->where(DB::raw('cast(concat(`fldsessiondate`, " ",`fldstarttime`) as datetime)'),'>=',$currentDateTime)->whereIn('fldstatus',['Live','Pending'])->whereIn('location_id', $arrLocationIDs)->orderBy('fldsessiondate', 'ASC')->orderBy('fldstarttime', 'ASC')->paginate(10);
             //dd($arrSession);

        foreach($arrSession as $objSession){
            
            $objBook = new Book();
            $objSetting = new Setting();
                    
            $objSession->fldfeaturedimage = $objSetting->getMediaUrl().$objSession->fldfeaturedimage;
            $date = new DateTime($objSession->fldsessiondate);
            $objSession->fldSessionDateformate = $objSession->fldsessiondate;
            $dateStart = new DateTime($objSession->fldstarttime);
            $objSession->fldsessiondate = $date->format("D, j M Y");
            $objSession->fldstarttime = $dateStart->format("H:i");
            
        unset($objSession->provider);

       
        //dd($objSession->provider);
            $objSession->fldavailableslots = "".$this->availableSlots($objSession->id);

            $Result = $objBook->userBookedSession($intUserID,$objSession->id);
            
            if($objSession->images !== null && $objSession->images !== ''){

               $objSetting = new Setting();
                $mediaUrl = $objSetting->getMediaUrl();
                 $images = array();
                 $objSession->images = json_decode($objSession->images);
                 //dd($objSession->images);
                foreach($objSession->images as $objGallery){
                    
                    $objGallery = $mediaUrl.$objGallery;
                    $images[] = $objGallery;
                } 
                $objSession->images = array();
                $objSession->images = $images;
            }
            if(!$Result){
                $objSession->IsBookedByUser = false;
            }else{
                
                $objSession->IsBookedByUser = true;
                
            }
            $objBalance = new Balance();
            $objSession->totalpoints = $objBalance->getBalance();
        }
        return $arrSession;
    }


    public function listUpcomingBookedSession($intUserID){
        
       
       $objBookStatus = new BookStatus();
        
        $carbonNow = Carbon::now();
        $currentDateTime = $carbonNow->toDateTimeString();
       
        $arrUpComingBookedSession = DB::table('books')
            ->leftJoin('sessions', 'books.session_id', '=', 'sessions.id')
            
            ->where('user_id',$intUserID)
            ->where(DB::raw('cast(concat(`fldsessiondate`, " ",`fldstarttime`) as datetime)'),'>=',$currentDateTime)
            ->where('bookStatus_id',$objBookStatus->getBookStatusID('booked')||$objBookStatus->getBookStatusID('pending'))
            ->orderBy('books.created_at', 'Desc')
            ->paginate(10);
            
           
            foreach ($arrUpComingBookedSession as $obj) {
                $obj->session = '';
                
                $obj->session = $this->getSession($obj->id);

                $objBook = new Book();
                $objSetting = new Setting();
                    
            
            $obj->fldfeaturedimage = $objSetting->getMediaUrl().$objSession->fldfeaturedimage;
            $date = new DateTime($obj->fldsessiondate);
            

            $dateStart = new DateTime($obj->fldstarttime);
            
            $obj->fldsessiondate = $date->format("D,j M Y");
            $obj->fldstarttime = $date->format("H:i");
            $obj->fldavailableslots = "".$this->availableSlots($obj->id);
            
             if($obj->images !== null && $obj->images !== ''){
                //dd($objSession->images);
                
                $obj->images = json_decode($obj->images);

               foreach($obj->images as $objGallery){
                    $objSetting = new Setting();    
                    $objGallery= $objSetting->getMediaUrl().$objGallery;
                } 
        }
            
    //dd( $obj->fldAvailableSlots);
            $Result = $objBook->userBookedSession($intUserID,$obj->id);


            if(!$Result){
                $obj->IsBookedByUser = false;
                $obj->VerificationCode = '';
             }else{
                $obj->IsBookedByUser = true;
                $obj->booking = $Result;
             }
            } 
           return $arrUpComingBookedSession; 
       
    }
    
   
    
     public function getSessionAttachedToProviderwithoutPagnation($arrLocationIDs,$extendedDays = NULL){
        //$objUser = new UserController();
        //$intUserID = $objUser->GetUserID();
        //dd($arrLocationIDs);
        if($extendedDays == NULL)
        {       $currentDateTime = $carbonNow->toDateTimeString(); }
        else
        {
         $carbonNow =  Carbon::now()->subDays($extendedDays);     
         $currentDateTime = $carbonNow->toDateTimeString(); }

        /*$arrSession = Session::whereIn('location_id', $arrLocationIDs)->select('id','fldsessionname','fldsessiondate','fldsessionduration','fldfeaturedimage','fldstarttime','fldendtime','fldisfeaturehot','sport_id','location_id','cancellationType_id','fldsessionpoint','fldmaxslots','fldholdslots','fldstatus','revoked','fldsessioncost','isautobooked','images','note1','note2','sportoyaSessionMargin')->where(DB::raw('cast(concat(`fldsessiondate`, " ",`fldstarttime`) as datetime)'),'>=',$currentDateTime)->with('location')->with('sport')->with('cancellationType')->get();*/

         $arrSession = Session::whereIn('location_id', $arrLocationIDs)->select('id','fldsessionname','fldsessiondate','fldsessionduration','fldfeaturedimage','fldstarttime','fldendtime','fldisfeaturehot','sport_id','location_id','cancellationType_id','fldsessionpoint','fldmaxslots','fldholdslots','fldstatus','revoked','fldsessioncost','isautobooked','images','note1','note2','sportoyaSessionMargin','fldsessionprice','sportoyaSessionMarginPayNow')->with('location')->with('sport')->with('cancellationType')->get();
    
        foreach($arrSession as $objSession){
            
            $objBook = new Book();
            $objSetting = new Setting();    
            
            $objSession->fldfeaturedimage = $objSetting->getMediaUrl().$objSession->fldfeaturedimage;
            $date = new DateTime($objSession->fldsessiondate);
            
            $objSession->fldsessiondate = $date->format("D,j M Y");
            $objSession->fldstarttime = $date->format("H:i");
            
        unset($objSession->provider);

       
        //dd($objSession->provider);
            $objSession->fldavailableslots = "".$this->availableSlots($objSession->id) ;
//print($objSession->fldAvailableSlots);
            $objSession->fldTotalBookedNumber = count($objBook->getTotalBookedSessions($objSession->id));
//dd( $objSession->fldTotalBookedNumber);
            //$Result = $objBook->userBookedSession($intUserID,$objSession->pkSessionID);
            
            if($objSession->images !== null && $objSession->images !== ''){
                $objSession->images = json_decode($objSession->images);
                $objSetting = new Setting();
                $mediaUrl = $objSetting->getMediaUrl();
                 $images = array();
                foreach($objSession->images as $objGallery){
                    
                    $objGallery = $mediaUrl.$objGallery;
                    $images[] = $objGallery;
                } 
                $objSession->images = array();
                $objSession->images = $images;
            }
          
            
        }
        //dd($arrSession);
        return $arrSession;    
    }
    public function getSessionAttachedToProviderUpcoming($arrLocationIDs){
        //$objUser = new UserController();
        //$intUserID = $objUser->GetUserID();
        //dd($arrLocationIDs);
        $carbonNow = Carbon::now();
        $currentDateTime = $carbonNow->toDateTimeString();

        $arrSession = Session::whereIn('location_id', $arrLocationIDs)->select('id','fldsessionname','fldsessiondate','fldsessionduration','fldfeaturedimage','fldstarttime','fldendtime','fldisfeaturehot','sport_id','location_id','cancellationType_id','fldsessionpoint','fldmaxslots','fldholdslots','fldstatus','revoked','fldsessioncost','isautobooked','images','note1','note2','sportoyaSessionMargin')->where(DB::raw('cast(concat(`fldsessiondate`, " ",`fldstarttime`) as datetime)'),'>=',$currentDateTime)->with('location')->with('sport')->with('cancellationType')->orderBy('fldsessiondate', 'ASC')->orderBy('fldstarttime', 'ASC')->paginate(10);
        foreach($arrSession as $objSession){
            
            $objBook = new Book();
            $objSetting = new Setting();    
            
            $objSession->fldfeaturedimage = $objSetting->getMediaUrl().$objSession->fldfeaturedimage;
            $date = new DateTime($objSession->fldsessiondate);
            $timeStart = new DateTime($objSession->fldstarttime);
            
            
            $objSession->fldsessiondate = $date->format("D,j M Y");
            $objSession->fldstarttime = $timeStart->format("H:i");
            
        	unset($objSession->provider);

        
            $objSession->fldavailableslots = "".$this->availableSlots($objSession->id) ;
            
            $objSession->fldTotalBookedNumber = count($objBook->getTotalBookedSessions($objSession->id));
            
            
            if($objSession->images !== null && $objSession->images !== ''){
                $objSession->images = json_decode($objSession->images);
                $objSetting = new Setting();
                $mediaUrl = $objSetting->getMediaUrl();
                 $images = array();
                foreach($objSession->images as $objGallery){
                
                    
                    $objGallery = $mediaUrl.$objGallery;
                    $images[] = $objGallery;
                } 
                $objSession->images = array();
                $objSession->images = $images;
            }
          
            
        }
        //dd($arrSession);
        return $arrSession;    
    }


    public function getSessionAttachedToProviderPast($arrLocationIDs){
        
         $carbonNow = Carbon::now();
        $currentDateTime = $carbonNow->toDateTimeString();   
        $arrSession = Session::whereIn('location_id', $arrLocationIDs)->select('id','fldsessionname','fldsessiondate','fldsessionduration','fldfeaturedimage','fldstarttime','fldendtime','fldisfeaturehot','sport_id','location_id','cancellationType_id','fldsessionpoint','fldmaxslots','fldholdslots','fldstatus','revoked','fldsessioncost','isautobooked','images','note1','note2','sportoyaSessionMargin')->where(DB::raw('cast(concat(`fldsessiondate`, " ",`fldstarttime`) as datetime)'),'<',$currentDateTime)->with('location')->with('sport')->with('cancellationType')->orderBy('fldsessiondate', 'DESC')->orderBy('fldstarttime', 'DESC')->paginate(10);
        
         
       
        foreach($arrSession as $objSession){
            
            $objBook = new Book();
            $objSetting = new Setting();
            $objSession->fldfeaturedimage = $objSetting->getMediaUrl().$objSession->fldfeaturedimage;
            $date = new DateTime($objSession->fldsessiondate);
            
            $dateStart = new DateTime($objSession->fldstarttime);
            $objSession->fldsessiondate = $date->format("D,j M Y");
            $objSession->fldstarttime = $dateStart->format("H:i");

            
            
        unset($objSession->provider);

       
        //dd($objSession->provider);
            $objSession->fldavailableslots = "".$this->availableSlots($objSession->id) ;
            $objSession->fldTotalBookedNumber = count($objBook->getTotalBookedSessions($objSession->id));
            //$Result = $objBook->userBookedSession($intUserID,$objSession->pkSessionID);
            
           if($objSession->images !== null && $objSession->images !== ''){
                $objSession->images = json_decode($objSession->images);
                $objSetting = new Setting();
                $mediaUrl = $objSetting->getMediaUrl();
                 $images = array();
                foreach($objSession->images as $objGallery){
                    
                    $objGallery = $mediaUrl.$objGallery;
                    $images[] = $objGallery;
                } 
                $objSession->images = array();
                $objSession->images = $images;
            }
        }
        //dd($arrSession);
        return $arrSession;    
    }

     public function getSessionAttachedToProviderIDsTimeIgnored($arrLocationIDs){
  
 
        $arrSessionIDs = Session::whereIn('location_id', $arrLocationIDs)->select('id')->get();
        
 
       
        return $arrSessionIDs;
    }

     public function getSessionAttachedToProviderIDs($arrLocationIDs){
  
         $carbonNow = Carbon::now();
        $currentDateTime = $carbonNow->toDateTimeString();
        $arrSessionIDs = Session::whereIn('location_id', $arrLocationIDs)->select('id')->where(DB::raw('cast(concat(`fldsessiondate`, " ",`fldstarttime`) as datetime)'),'>=',$currentDateTime)->get();
        
           //dd($arrSessionIDs);
       
        return $arrSessionIDs;
    }
    public function getSessionWithCancellation($intSessionID){
        $objSession = $this->where('id',$intSessionID)->with('cancellationType')->first();
        return $objSession;

    }
    public function updateSessionStatusCancelled($intSessionID){
        $objSession = Session::where('id',$intSessionID)->update(['fldStatus'=>'Cancelled']);

        return $objSession;
    }
    public function checkSessionStatus($intSessionID){
//dd($intSessionID);
       $Session = $this->where('id',$intSessionID)->select('fldstatus')->first();
//dd($Session);
       if($Session->fldstatus =='Cancelled'){
            return false;     
       }else if($Session->fldstatus =='Live'){
            return true;
       }else{
            return false;
       }
    }

    public function location(){
        return $this->belongsTo('App\Location','location_id','id');
    }

    public function book(){
        return $this->hasMany('App\book','session_id','id')->where('user_id',$this->intUserID);      
    }

    public function sport(){
        return $this->belongsTo('App\Sport','sport_id','id');   
    }

    public function provider(){
        return $this->belongsTo('App\Provider','location_id','location_id');      
    }

    public function cancellationType(){
        return $this->belongsTo('App\CancellationType','cancellationType_id','id');      
    }
    public function Note(){
        return $this->hasMany('App\Note','fkItemID','id')->where('type_id','1');      
    }

    /******************************* CPanel *****************************************/
   /***********************************************************************************/
   /***********************************************************************************/   
   /***********************************************************************************/
   /***********************************************************************************/


      public function save(array $options = [])
    {
     
        if(isset($this->original) && count($this->original) > 0)
            {
                    if($this->original['fldstatus'] == 'Live')
                    {
                        if($this->fldstatus == 'Cancelled')
                        {
                            $request = new Request();
                            $request->pkSessionID = $this->id;

                            $sessionController = new SessionController();
                            $sessionController->ProviderUpdateSessionStatus($request);
                        }

                        else if($this->fldstatus == 'Pending')
                        {

                            return false;
                        }
                    }
                    else if($this->original['fldstatus'] == 'Cancelled')
                        { return false; }
            }
        parent::save();


    }

   

   /***********************************************************************************/
   /***********************************************************************************/
   /***********************************************************************************/   
   /***********************************************************************************/
   /******************************END CPanel**************************************/



}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Session;
use App\Setting;
use TCG\Voyager\Traits\Spatial;


class Location extends Model
{

    use Spatial;

    protected $table = 'locations';

    protected $fillable = ['id','fldlongitude','fldlatitude','fldlocationname','fldlocationaddressName','fldshortaddress','provider_id','district_id'];

     protected $spatial = ['coordinates'];

     protected $hidden = ['coordinates'];

	
    public function listLocation(){

    	$arrLocation = $this->get();
    	return $arrLocation;
    }

    public function getProvider($intLocationID){
        //dd($intLocationID);
        $objLocation = $this->where('id',$intLocationID)->first();
           
        $objProvider = DB::table('providers')->where('id',$objLocation->provider_id)->first();
        return $objProvider;
    }

    public function loadLocationByProviderID($intProviderID){
        $arrLocation = Location::select('id','fldlongitude','fldlatitude','fldlocationname','fldlocationaddressName','fldshortaddress','provider_id','district_id')->where('provider_id',$intProviderID)->with('provider')->get();
      
        return $arrLocation;
    }
    public function getLocationIDs($intProviderID){
        $arrLocation1 = array();
        $arrLocation = Location::select('id')->where('provider_id',$intProviderID)->with('provider')->pluck('id')->toArray();
        
        return $arrLocation;
    }

    public function getLocationAttachedToDistrict($intDistrictID){

        //print($intDistrictID);
        $arrLocationIDs = Location::select('id')->where('district_id',$intDistrictID)->pluck('id')->toArray();

	
 	
	return $arrLocationIDs;
       
    }
    public function deleteLocation($intLocationID){

        $objLocation = DB::table('locations')->where('id',$intLocationID)->delete();
        return $objLocation;
    }
    
    public function  session(){
    	 return $this->hasMany('App\Session','location_id','id');
    }

    public function provider(){
    	return $this->belongsTo('App\Provider','provider_id','id');
    }

     public function save(array $options = [])
    {
         $coordinates = $this->getCoordinates(); 
       //  dd($this);
         if($coordinates != null && count($coordinates) > 0)
         {
            $this->fldLongitude =  $coordinates[0]['lng'];
            $this->fldLatitude =  $coordinates[0]['lat'];
            
         }
    

        parent::save();
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelativeTransaction extends Model
{
    protected $table = 'relative_transaction';

    protected $fillable = ['id','fldMainTransactionID','fldSubTransactionID','created_at','updated_at'];


    public function getRelativeByID($intMainTransactionID){
//dd($intMainTransactionID);
    	$intSubTransactionID = RelativeTransaction::select('fldSubTransactionID')->where('fldMainTransactionID',$intMainTransactionID)->first();
    	//dd($intSubTransactionID->fldSubTransactionID);
	
    	return $intSubTransactionID;
    }
    public function getRelativeTransIDs($arrMainTransIDs){
//dd($arrMainTransIDs);
    	$arrSubTransactionIDs = RelativeTransaction::select('fldSubTransactionID')->whereIn('fldMainTransactionID',$arrMainTransIDs)->get();
    	//print_r($arrSubTransactionIDs);die;
	foreach($arrSubTransactionIDs as $obj){
		$arrSubTransactionID[] = $obj->fldSubTransactionID;		
	}

	return $arrSubTransactionID;
    }
}

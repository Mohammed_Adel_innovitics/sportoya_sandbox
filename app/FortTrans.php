<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;



class FortTrans extends Model
{
    protected $table = 'forttransactions';

	protected $fillable = ['id','amount','response_code','card_number','card_holder_name','signature','payment_option','expiry_date','customer_ip','eci','language','fort_id','command','response_message','sdk_token','authorization_code','merchant_reference','customer_email','currency','status','user_id','plan_id','created_at','updated_at','session_id'];

	public function isMerchantExist($strMarchent){
	    $arrMerchant = $this->where('merchant_reference',$strMarchent)->exists();
	    return $arrMerchant;
	}
	
	
	public function generateFortToken($device_id)
	{
	    /*["access_code" : "xUXcSa2Du6VTGxYklDAm" , "merchant_identifier" : "aLFoTDmZ" , "language" : "en", "device_id" : UIDevice.current.identifierForVendor!.uuidString , "service_command" : "SDK_TOKEN"]*/
	    
	    $arrData = array(
	        'service_command' => 'SDK_TOKEN',
	       
	        'access_code' => env("PAYFORT_ACCESS_CODE", ""),
	        'merchant_identifier' => env("PAYFORT_MERCHANT_IDENTIFIER", ""), 
	        'language' => 'en',
	        'device_id' => $device_id,
	    );
	    
	    $arrData['signature'] = $this->signatureGenerator($arrData);
	    $contents = $this->connectToFort($arrData);
	    $result['result'] = $contents;
	    //dd($result);
	    return $contents->status == "22" ? $result : false;
	}
/* public function checkPaymentStatus($merchant_reference)
    {
    	

    		$arrData = array(
			'query_command' => 'CHECK_STATUS',
			'access_code' => 'RHoJecFOPpeBb1b0aHwR',
			'merchant_identifier' => 'TAqhIJyJ',
			'language' => 'en',
			'merchant_reference' => $merchant_reference,
			);

			$arrData['signature'] = $this->signatureGenerator($arrData);
			$contents = $this->connectToFort($arrData);
			
			$sigValidation = $this->signatureValidate($contents,$contents->signature);
			//dd($sigValidation);
						//dd($contents);

			if($sigValidation == false)
				{	return false;	}

			if(!isset($contents->transaction_status))
				{ Log::stack(['single', 'slack'])->debug('Transaction Status: Failed');
				 return false;	}
			else { Log::stack(['single', 'slack'])->debug('Transaction Status: Success'); }

			//dd($contents);
			return $contents->transaction_status == "14" ? true : false;
    }
*/
	public function verifyPayment($merchant_reference)
	{
	   
	    $arrData = array(
	        'query_command' => 'CHECK_STATUS',
	        'access_code' => env("PAYFORT_ACCESS_CODE", ""),
	        'merchant_identifier' => env("PAYFORT_MERCHANT_IDENTIFIER", ""), 
	        'language' => 'en',
	        'merchant_reference' => $merchant_reference,
	    );
	    
	    $arrData['signature'] = $this->signatureGenerator($arrData);
	    $contents = $this->connectToFort($arrData);
	   // dd($contents);
	    $sigValidation = $this->signatureValidate($contents,$contents->signature,'out');
		

			if($sigValidation == false)
				{	return false;	}

			if(!isset($contents->transaction_status))
				{  return false;	}

			return $contents->transaction_status == "14" ? true : false;
	}
	
	
	private function signatureGenerator($arrData)
	{
	    ksort($arrData);
	    
	    $auth = env("PAYFORT_SIGNATURE_IN", "");
	    
	    foreach($arrData as $key => $val) {
	        //  echo "$key = $val\n";
	        $auth = $auth.$key.'='.$val;
	    }
	    
	    $auth = $auth.env("PAYFORT_SIGNATURE_IN", "");
	    
	    //dd($auth);
	    
	    $signature = hash('sha256', $auth);
	    
	    return $signature;
	    
	}
	
	public function signatureValidate($arrData,$original_sign,$data_type)
	{
		if(!is_array($arrData))
    	{
    		$array = array();
    		foreach ($arrData as $key => $value) 
    			if($key != 'signature')
   					 $array[$key] = $value;

   			$arrData = $array;
    	}

    	$sigKey = ($data_type == 'out') ? env("PAYFORT_SIGNATURE_OUT", "") : env("PAYFORT_SIGNATURE_IN", "");

	    ksort($arrData);
	    
	    $auth = $sigKey;
	    
	    foreach($arrData as $key => $val) {
	        //  echo "$key = $val\n";
	        $auth = $auth.$key.'='.$val;
	    }
	    
	    $auth = $auth.$sigKey;
	    
	    //dd($auth);
	    
	    $signature = hash('sha256', $auth);
	    
	    //dd($auth.' --- Sig = '.$signature.' Orid = '.$original_sign);
	    
	    if($original_sign == $signature)
	    {
	        return true;
	    }
	    
	    
	    
	    return false;
	    
	}
	
	
	
	private function connectToFort($arrData)
	{
	    error_reporting(E_ALL);
	    ini_set('display_errors', '1');
	    
	    $url = env("PAYFORT_URL", "");
	    
	    $client = new Client(array('timeout' => 60,'verify' => false)); //GuzzleHttp\Client
	    
	    $request = $client->post($url, ['headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],'body' => json_encode($arrData)]);
	    
	    $contents = json_decode((string) $request->getBody()); // returns all the contents
	    //dd($contents);
	    return $contents;
	}

}

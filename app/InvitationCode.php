<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class InvitationCode extends Model
{
    //
    

    protected $table = 'invitationcodes';

    protected $fillable = ['id','fldinvitationcode','user_id'];

    public function addInvitationCode($intUserID,$strName){
    	$arrName = explode(' ',trim($strName));
		
    	$strInvitationCode = $arrName[0].str_random(6);

	    $objInvitationCode = DB::table('invitationcodes')->insert([
	    ['fldinvitationcode' => $strInvitationCode,'user_id'=>$intUserID]
		]);
    	return $objInvitationCode;
    }

    public function getInvitationCode($strInvitationCode){
    	$objInvitationCode = InvitationCode::select('id','fldinvitationcode','user_id')->where('fldinvitationcode',$strInvitationCode)->first();
        //dd($arrSession->toSql());
    	return $objInvitationCode;
    }

    public function getInvitationCodeOfUser($intUserID){
        $objInvitationCode = InvitationCode::select('fldinvitationcode')->where('user_id',$intUserID)->first();
        //dd($objInvitationCode);
        //dd($arrSession->toSql());
        return $objInvitationCode['fldinvitationcode'];

    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookStatus extends Model
{
    protected $table = 'bookstatuses';

    protected $fillable = ['id','fldbookstatusname','fldbookstatuscolor'];

    public function getBookStatusID($strBookName){
        $objBookStatus = BookStatus::select('id')->where('fldbookstatusname',$strBookName)->first();

        return  $objBookStatus['id'];
    }

    public function getBookStatusByID($intBookStatusID){
        $objBookStatus = $this->where('id',$intBookStatusID)->first();

        return  $objBookStatus;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplainType extends Model
{
    protected $table = 'complaintypes';

    protected $fillable = ['id','fldcomplaintypename','update_at','created_at'];

    public function getComplainTypeID($strComplainTypeName){
    	$intComplainTypeID = ComplainType::select('id')->where('fldcomplaintypename',$strComplainTypeName)->first();
        return $intComplainTypeID['id'];
    }
    
}

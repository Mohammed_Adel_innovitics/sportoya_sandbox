<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SMSGateway extends Model
{
    //
    public function send($recipientNumber,$messageContent){
    	
    	 $fields = array
              (
                'from'    => 'Sportoya',
                'to'  => $recipientNumber,
                'text' => $messageContent
              );

           $headers = array
                   (
                       'Authorization: Basic U3BvcnRveWE6Q3Jpc3RpYW5vXzIw',
                       'Content-Type: application/json',
                       'Accept: application/json'
                   );

               $ch = curl_init();
               curl_setopt( $ch,CURLOPT_URL, 'https://api.infobip.com/sms/1/text/single' );
               curl_setopt( $ch,CURLOPT_POST, true );
               curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
               curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
               curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
               curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
               $result = curl_exec($ch );
               curl_close( $ch );
		
    } 
}

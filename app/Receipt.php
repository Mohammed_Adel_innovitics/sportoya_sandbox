<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Book;

class Receipt extends Model
{
    //
    protected $table = 'receipts';
    protected $fillable = ['id','bookIDs','revoked','created_at','updated_at'];
    
    public function getReceipt($receipt_id){
        $obj = $this->where('id', $receipt_id)->first();
        return $obj;
    }
    public function revokedReceipt($receipt_id){
       
        $result =  $this->where('id', $receipt_id)->update(['revoked'=>'1']);
        return $result;
    }
}

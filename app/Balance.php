<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\UserController;
use Carbon\Carbon;


class Balance extends Model
{

	public function getBalance(){
		$objUser = new UserController();
		$dateNow = date("Y-m-d H:m:i");
		$balance = DB::table('pointtransactions')->where('user_id', '=', $objUser->GetUserID())->where('revoked','=',0)->sum('fldpoint');
			
		if($balance > 0){
			return $balance;		
		}else{
			return 0;		
		}	
	}

    public function getBalanceWithoutBouns(){
    	$objUser = new UserController();
		$balance = DB::table('pointtransactions')->where('user_id', '=', $objUser->GetUserID())->where('revoked','=',0)->whereNotIn('transactionType_id',[10,11,12])->sum('fldpoint');
		if($balance >0){
			return $balance;		
		}else{
			return 0;		
		}
		
    }

    public function getBalaceOfBouns(){
    	$objUser = new UserController();
    	$dateNow = date("Y-m-d H:m:i");
    	$balance = DB::table('pointtransactions')->where('user_id', '=', $objUser->GetUserID())->where('revoked','=',0)->whereIn('transactionType_id' ,[10,11,12])->sum('fldpoint');
		
		if($balance != null && $balance > 0){
			return $balance;		
		}else{
			return 0;		
		}				
    }
    
    public function checkexpiryBounsPoint(){
        $objUser = new UserController();
        $dateNow = date("Y-m-d H:m:i");
        
        $balance = DB::table('pointtransactions')->where('fldexpirydate','<=',$dateNow)->where('user_id', '=', $objUser->GetUserID())->where('revoked','=',0)->where('expired','=',0)->whereIn('transactionType_id' ,[10,11])->orderby('created_at','desc')->update(['expired' => 1]);
       // dd($balance);
        if($balance>0){
            return true;
        }
        return false;
    }
    
}
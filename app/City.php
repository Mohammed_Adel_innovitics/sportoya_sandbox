<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class City extends Model
{
    protected $table = 'cities';

    protected $fillable = ['id','fldcityname'];

   public function listCity(){
    	$arrCity = $this->paginate(10);
    	return $arrCity;}

    public function loadCityByID($intCityID){
		$objCity = DB::table('cities')->where('id',$intCityID)->get();
    	return $objCity;    	}

	public function addCity($fldCityName){
	    $objCity = DB::table('cities')->insert([
	    ['fldcityname' => $fldCityName]
		]);
    	return $objCity;}

    public function updateCity($intCityID,$strCityName){
    	$objCity = 	DB::table('cities')->where('id', $intCityID)
          ->update(['fldcityname' => $strCityName]);
        return $objCity;
    }

    public function deleteCity($intCityID){
    	$objCity = DB::table('cities')->where('id',$intCityID)->delete();
    	return $objCity;}

}

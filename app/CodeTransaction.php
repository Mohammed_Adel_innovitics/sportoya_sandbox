<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodeTransaction extends Model
{
    protected $table = 'codetransactions';

    protected $fillable = ['id','fldcodetransactiontype','pointTransaction_id','code_id','updated_at','created_at','cashTransaction_id'];

    public function addCodeTransaction($strCodeTransactionType,$intPointTransactionID,$intCodeID){
    	$this->fldcodetransactiontype = $strCodeTransactionType;
    	$this->pointTransaction_id = $intPointTransactionID;
    	$this->code_id = $intCodeID;
    	$result = $this->save();
    	return $result;

    }
    
    public function addTransactionForCash($strCodeTransactionType,$intCashTransactionID,$intCodeID){
        $this->fldcodetransactiontype = $strCodeTransactionType;
        $this->cashTransaction_id = $intCashTransactionID;
        $this->code_id = $intCodeID;
        $result = $this->save();
        return $result;
    }
    
    public function addTransactionForVoucher($strCodeTransactionType,$intPointTransactionID,$intCodeID,$user_id){
        $this->fldcodetransactiontype = $strCodeTransactionType;
        $this->pointTransaction_id = $intPointTransactionID;
        $this->code_id = $intCodeID;
        $this->user_id = $user_id;
        $result = $this->save();
        return $result;
    }
    
    public function checkBookPromo($trans){
        $point = $this->whereIn('pointTransaction_id',json_decode($trans))->count();
        $cash = $this->whereIn('cashTransaction_id',json_decode($trans))->count();
        if($cash > 0 || $point > 0){
            return 'Yes';
        }
        return 'No';
    }

}

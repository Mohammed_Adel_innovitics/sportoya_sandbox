<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\UserSportInterest;
use App\Http\Controllers\UserController;
use App\Setting;
class Sport extends Model
{
   //

   protected $table = 'sports';

   protected $fillable = ['id','fldsportname','fldimg','fldicon','flddescription','fldinterestvissible','fldisbarvisible','updated_at','created_at'];


   public function listSport(){
       $arrSport = $this->whereNotIn('id',[1,2])->get();
       return $arrSport;
   }

   public function listSportInterest(){
       $objUser = new UserController();
       $intUserID = $objUser->GetUserID();
       $objUserSportInterest = new UserSportInterest();
       $ResultSportInterestIds = $objUserSportInterest->getUserSportIDs($intUserID);
       $arrSport = Sport::select('id','fldsportname','fldimg','fldinterestvissible')->whereNotIn('id',[1,2])->where('fldinterestvissible','1')->get();
       foreach($arrSport as $objSport){
           $objSetting = new Setting();
           $objSport->fldimg = $objSetting->getMediaUrl().$objSport->fldimg;
           if((in_array($objSport->id, $ResultSportInterestIds))){
               $objSport->IsUserInterest = 1;    
           }else{
               $objSport->IsUserInterest = 0;    
           }
           
       }
       return $arrSport;
   }
   public function listSportNavBar(){
       $arrSport = Sport::select('id','fldsportname','fldicon')->where('fldIsBarVisible','1')->orderBy('order','ASC')->get();
       
       foreach($arrSport as $objSport){
           $objSetting = new Setting();
           $objSport->fldicon = $objSetting->getMediaUrl().$objSport->fldicon;
       }

       return $arrSport;
   }


   public function loadSportByID($intSportID){
       $objSport = DB::table('sports')->where('id',$intSportID)->get();
       return $objSport;
   }

   public function deleteSport($intSportID){
       $objSport = DB::table('sports')->where('id',$intSportID)->delete();
       return $objSport;
   }
   public function updateSport($intSportID,$strSportName,$strImg,$strIcon,$strDescription,$strInterestVisible,$IsBarVisible){
       $dateTime = date_create('now')->format('Y-m-d H:i:s');
       $objSport =  DB::table('sports')->where('id', $intSportID)->update(['fldsportname' => $strSportName,'fldimg'=>$strImg,'fldicon'=>$strIcon,'flddescription'=>$strDescription,'fldinterestvissible'=>$strInterestVisible,'fldisbarvisible'=>$IsBarVisible,'updated_at'=>$dateTime]);
       return $objSport;
   }


   public function session(){
       return $this->hasMany('App\Session');
   }

   public function sportUserInterest($UserID){
       return DB::table('sports')->$this->hasMany('App\UserSportInterest','sport_id','id')->where('user_id',$UserID)->get;

   }


}
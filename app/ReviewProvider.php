<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use App\UserDetails;

class ReviewProvider extends Model
{
    //
    protected $table = 'vw_review_provider';
    protected $fillable = ['pkProviderID','fldReviewRate','fldReviewText','fkSessionID','fkUserID','fkSessionID','created_at'];

    public function listReviewAttachedToProvider($intProviderID){
    	$arrReview = ReviewProvider::select('pkProviderID','fldReviewRate','fldReviewText','fkSessionID','fkUserID','fkSessionID','created_at')->where('pkProviderID',$intProviderID)->with('session')->paginate(10);

         foreach($arrReview as $obj){
            $objUserDetails = new UserDetails();
//print($obj->fkUserID);
            $obj->user = $objUserDetails->getUserDetailsByID($obj->fkUserID);
         }


        return $arrReview;
    }
    public function getReviewAttachedToProvider($intProviderID){
        
        $arrReview = ReviewProvider::select('pkProviderID','fldReviewRate','fldReviewText','fkSessionID','fkUserID','created_at')->where('pkProviderID',$intProviderID)->with('user')->with('session')->get();
        return $arrReview;
    }
    public function getReviewRateAttachedToProvider($intProviderID){
        $RateValuePerProvider = 0;
        $totalReviewRate = DB::table('vw_review_provider')->where('pkProviderID',$intProviderID)->sum('fldReviewRate');
        if($totalReviewRate > 0){
            $RateValuePerProvider = $totalReviewRate / count($this->getReviewAttachedToProvider($intProviderID));
        }
        return $RateValuePerProvider;
    }
    public function user(){
    	return $this->belongsTo('App\User','fkUserID','id');
    }
    public function session(){
    	return $this->belongsTo('App\Session','fkSessionID','id');
    }
}

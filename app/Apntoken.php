<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Apntoken extends Model
{
   protected $table = 'apntokens';
    protected $fillable = ['id','user_id','token','updated_at','created_at'];

    public function checkApn($apn,$intUserID){
    	$apn = $this->where('token',$apn)->where('user_id',$intUserID)->get();
    	if(count($apn)>0){
    		return true;
    	}else{
    		return false;
    	}
    }  

    public function getApnTokenOfUser($intUserID){
         
        
        $apn = Apntoken::select('token')->where('user_id',$intUserID)->groupBy('token')->pluck('token')->toArray();
        return $apn;
    }

    public function getApnTokenOfUsers($arrUserID){

        $apn = Apntoken::select('token')->whereIn('user_id',$arrUserID)->groupBy('token')->pluck('token')->toArray();
        return $apn;
    }

    public function deleteApn($strToken){
        $apn = Apntoken::select('token')->where('token',$strToken)->delete();
        return $apn; 
    }

}

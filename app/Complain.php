<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complain extends Model
{
    protected $table = 'complains';

    protected $fillable = ['id','fldcomplaindescription','complaintype_id','user_id','created_at','updated_at'];

     
}

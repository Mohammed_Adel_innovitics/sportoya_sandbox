<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutSideCustomer extends Model
{
    protected $table = 'outsidecustomers';

    protected $fillable = ['id','fldname','fldphone','fldemail','session_id','created_at','updated_at'];
	
	public function getAlloutSide($intSessionID){
    	$arrOutSide = OutSideCustomer::select('id','fldname','fldphone','fldemail','session_id','created_at','updated_at')->where('session_id',$intSessionID)->get();
    	
    		return $arrOutSide;
    	}
        public function getCountoutSideOnSession($intSessionID){
    		$OutSideCount = $this->where('session_id',$intSessionID)->count();
    		return $OutSideCount;
    	}	 
}

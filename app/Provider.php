<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Session;
use App\Review;
use App\Location;
use App\ReviewProvider;
use Illuminate\Support\Facades\DB;
use App\Setting;

class Provider extends Model
{
    protected $table = 'providers';
    protected $fillable = ['id','fldprovidername','fldicon','fldwebsite','fldfacebook','fldinstagram','fldsignupdate','user_id','phone_id','about','images'];
    //


    public function listProvider(){
        $arrProvider = Provider::select('id','fldprovidername','fldicon','fldwebsite','fldfacebook','fldinstagram','fldsignupdate','user_id','phone_id','note','images')->with('location')->with('phone')->paginate(10);
        $objSession = new Session();
        $objLocation = new Location();
        $objProviderReview = new ReviewProvider();
        foreach($arrProvider as $obj){
            
            $obj->Review = count($objProviderReview->getReviewAttachedToProvider($obj->pkProviderID));
            
            /*** Provider Images **/
		//dd('');
		  if($obj->images !== null && $obj->images !== ''){
        		
        		
	                
	                 
	                $obj->images = json_decode($obj->images);
	                
	                 $images = array();
	              
	                foreach($obj->images as $objGallery){
	                
	                    $objSetting = new Setting();
	               	    $mediaUrl = $objSetting->getMediaUrl();
	               	    
	                    $objGallery = $mediaUrl.$objGallery;
	                    $images[] = $objGallery;
	           } 
                $obj->images = array();
                $obj->images = $images;
                	          

                //dd($objSession->images);
            }
            
		/* End Of Provider Images ****/
            
            
            $obj->Rate = $objProviderReview->getReviewRateAttachedToProvider($obj->pkProviderID);
            $objSetting = new Setting();
            $obj->fldIcon = $objSetting->getMediaUrl().$obj->fldIcon;
        }
        //dd($arrSession->toSql());


        return $arrProvider;
    }
    public function getLocationIDs($intProviderID){
        $arrProviderIDs = Provider::select('location_id')->where('id',$intProviderID)->get();
        //dd($arrSession->toSql());
        //dd($arrProviderIDs);
        return $arrProviderIDs;
    }

    public function getLocationIDsByUserID($intUserID){
        //dd($intUserID);
        $arrProviderIDs = Provider::select('location_id')->where('user_id',$intUserID)->get();
        //dd($arrProviderIDs);
        return $arrProviderIDs;
    }
    public function getProviderIDByUserID($intUserID){
        $Provider = Provider::select('id')->where('user_id',$intUserID)->first();
        //dd($arrProviderIDs);
        
        return $Provider['id'];
    }
    public function getProviderByID($id){
        //dd($id);
        $objProvider =  Provider::select('id','fldprovidername','fldicon','fldwebsite','fldfacebook','fldinstagram','fldsignupdate','user_id','phone_id','about','images')->with('location')->with('phone')->where('id',$id)->first();
        $objProviderReview = new ReviewProvider();
//dd($objProvider->pkProviderID);
        $objProvider->Review = count($objProviderReview->getReviewAttachedToProvider($objProvider['id']));
        $objProvider->Rate = $objProviderReview->getReviewRateAttachedToProvider($objProvider['id']);
        $objSetting = new Setting();
        $objProvider->fldIcon = $objSetting->getMediaUrl().$objProvider['fldicon'];
        
       // dd('');
        
       
        return $objProvider;
    }

    public function getProvider($intLocationID){
        //dd($intLocationID);
        $objLocation = DB::table('locations')->where('id',$intLocationID)->first();
           //dd($objLocation);
        $objProvider =  Provider::where('id',$objLocation->provider_id)->with('location')->first();
        $objProviderReview = new ReviewProvider();
//dd($objProvider->pkProviderID);
        $objProvider->Review = count($objProviderReview->getReviewAttachedToProvider($objProvider['id']));
        $objProvider->Rate = $objProviderReview->getReviewRateAttachedToProvider($objProvider['id']);
       /* if($objProvider->Gallery !== null && $objProvider->Gallery !== ''){

                $objSetting = new Setting();
                $mediaUrl = $objSetting->getMediaUrl();
                $images = array();
                foreach($objProvider->images as $objGallery){
                    
                    $objGallery = $mediaUrl.$objGallery;
                    $images[] = $objGallery;
                } 
                $objProvider->Gallery = array();
                $objProvider->Gallery = $images;
            }*/
            
         /*** Provider Images **/
		//dd('');
		  if($objProvider->images !== null && $objProvider->images !== ''){
        		
        		
	                
	                 
	                $objProvider->images = json_decode($objProvider->images);
	                
	                 $images = array();
	              
	                foreach($objProvider->images as $objGallery){
	                
	                    $objSetting = new Setting();
	               	    $mediaUrl = $objSetting->getMediaUrl();
	               	    
	                    $objGallery = $mediaUrl.$objGallery;
	                    $images[] = $objGallery;
	           } 
                $objProvider->images = array();
                $objProvider->images = $images;
                	          

                //dd($objSession->images);
            }
            
		/* End Of Provider Images ****/   
        $objSetting = new Setting();
        $objProvider->fldicon = $objSetting->getMediaUrl().$objProvider['fldicon'];
        
        
        return $objProvider;
        
    }       

    public function getProviderDetails($intUserID){
        $objProvider =  Provider::select('id','fldprovidername','fldicon','fldwebsite','fldfacebook','fldinstagram','fldsignupdate','user_id','phone_id','about')->with('location')->with('Gallery')->with('phone')->where('user_id',$intUserID)->first();
        $objProviderReview = new ReviewProvider();
//dd($objProvider->pkProviderID);
        $objProvider->Review = count($objProviderReview->getReviewAttachedToProvider($objProvider['id']));
         if($objProvider->Gallery !== null && $objProvider->Gallery !== ''){

                $objSetting = new Setting();
                $mediaUrl = $objSetting->getMediaUrl();
                $images = array();
                foreach($objProvider->Gallery as $objGallery){
                    
                    $objGallery = $mediaUrl.$objGallery;
                    $images[] = $objGallery;
                } 
                $objProvider->Gallery = array();
                $objProvider->Gallery = $images;
            }

        $objProvider->Rate = $objProviderReview->getReviewRateAttachedToProvider($objProvider['id']);
        $objSetting = new Setting();
        $objProvider->fldicon = $objSetting->getMediaUrl().$objProvider['fldicon'];
        return $objProvider;
    }
   /* public function getProviderIDByUserID($intUserID){
        //dd($intUserID);
        $objProvider= Provider::select('id')->where('user_id',$intUserID)->first();
       // dd($objProvider);
        return $objProvider->id;
    }*/
    public function location(){
        return $this->hasMany('App\Location','provider_id','id');
    }

     public function Note(){
        return $this->hasMany('App\Note','fkItemID','id')->where('type_id','2');      
    }

    public function Gallery(){

        return $this->hasMany('App\Gallery','fkItemID','id')->where('type_id','2');    
    }
    public function phone(){
       return $this->hasMany('App\Phone','user_id','id'); 
    }	
        

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\RelativeTransaction;

class PointTransaction extends Model
{
    //

    protected $table = 'pointtransactions';

    protected $fillable = ['id','fldpoint','fldcost','user_id','fkPlateformID','transactionType_id','fldtransactionnote','fldexpirydate','revoked','created_at','updated_at','merchant_refrence'];


    public function addTrans($point,$user_id){
        $this->fldpoint = -($point);
        $this->user_id = $user_id;
        $this->revoked = 0;
        $this->transactionType_id=12;
        $this->fldtransactionnote = 'bouns expiry';
        $this->save();
    }

    public function updatePointTrans($intUserID,$arrTransactionID ){
//dd($intMainTransactionID);
       /* $objRelativeTransaction = new RelativeTransaction();
        $intSubTransactionID = $objRelativeTransaction->getRelativeByID($intMainTransactionID);
        //dd($intSubTransactionID);
        if($intSubTransactionID!=null){
            $arrTransactionID = [$intMainTransactionID,$intSubTransactionID];     
        }
        //dd($intUserID);
         $arrTransactionID = [$intMainTransactionID];
         foreach($arrTransactionID as $obj){*/
//dd($intUserID.'-'.$obj);
            $objPointTransaction =  DB::table('pointtransactions')->where('user_id', $intUserID)->WhereIn('id',$arrTransactionID)->update(['revoked' => 1]); 

                
         /*}
*/         //dd($objPointTransaction);
        return $objPointTransaction;
    }
    public function updatePointTransByTransID($arrTransactionID){
//dd($intMainTransactionID);
        /*$objRelativeTransaction = new RelativeTransaction();
        $intSubTransactionID = $objRelativeTransaction->getRelativeByID($intMainTransactionID);
        */
        //dd($arrTransactionID );
       

            $objPointTransaction =  DB::table('pointtransactions')->WhereIn('id',$arrTransactionID)->update(['revoked' => 1]); 

                
     
         //
        return $objPointTransaction;
    }
    public function getPlanPointTransOfUserNotExpired($intUserID){
        $currentDateTime = date('Y-m-d H:i:s');
        //dd($currentDateTime);
        $objTransactionType = new TransactionType();
        $arrTransPointIDs = PointTransaction::select('id')->where('user_id',$intUserID)->where('transactionType_id',$objTransactionType->getTarnsactionID('Plan'))->where('revoked','!=',1)->where('fldexpirydate','>=',$currentDateTime)->get();
        //dd($arrTransPointIDs);
        return $arrTransPointIDs;

    }

    public function getPlanPointTransOfUserExpired($intUserID){
        $currentDateTime = date('Y-m-d H:i:s');
        $objTransactionType = new TransactionType();
        $arrTransPointIDs = PointTransaction::select('pkPointsTransactionID')->where('user_id',$intUserID)->where('transactionType_id',$objTransactionType->getTarnsactionID('Plan'))->where('fldexpirydate','<=',$currentDateTime)->get();
        return $arrTransPointIDs;

    }

    public function getMaxExpiredDateOfUserPlan($intUserID){
        //dd($intUserID);
        $currentDateTime = date('Y-m-d H:i:s');
        $objTransactionType = new TransactionType();
        $dtExpiryDate = PointTransaction::select('fldexpirydate')->where('transactionType_id',$objTransactionType->getTarnsactionID('Plan'))->where('user_id',$intUserID)
        ->where('fldexpirydate','>=',$currentDateTime)->max('fldexpirydate');

        return $dtExpiryDate;

    }

    public function getMyPlan($intUserID){
        $objTransactionType = new TransactionType();
        $obj = PointTransaction::where('transactionType_id',$objTransactionType->getTarnsactionID('Plan'))->where('user_id',$intUserID)->latest()->get();        

        $objPlan = DB::table('plans')->Where('fldplanpoints',$obj[0]->fldPoint)->first();
        return $objPlan;
    }

    public function getExpiredDateOfUserPlan($intUserID){
        //dd($intUserID);
        $currentDateTime = date('Y-m-d H:i:s');
        $objTransactionType = new TransactionType();
        $dtExpiryDate = PointTransaction::select('fldexpirydate')->where('transactionType_id',$objTransactionType->getTarnsactionID('Plan'))->where('user_id',$intUserID)
        ->where('fldexpirydate','>=',$currentDateTime)->first();

        return $dtExpiryDate;

    }

    
    public function extendExpiryDateOfBouns($intUserID){
         $date = date("Y-m-d");
         $ExpiryDate=date_create($date);
            //print_r($date);

        date_add($ExpiryDate ,date_interval_create_from_date_string("3 months"));

        $objPointTransaction =  DB::table('pointtransactions')->where('user_id',$intUserID)->Where('transactionType_id',10)->orwhere('transactionType_id',11)->update(['fldexpirydate' => $ExpiryDate]);  
          //print($objPointTransaction);     

        return true;
    }

    public function updatePlanPointsExpiryDate($intUserID,$arrTransPointIDs,$fldExpiryDate){

        foreach($arrTransPointIDs as $obj){
            //print($obj->pkPointsTransactionID.'-');
            $objPointTransaction =  DB::table('pointtransactions')->where('user_id', $intUserID)->where('user_id',$intUserID)->Where('id',$obj->pkPointsTransactionID)->update(['fldexpirydate' => $fldExpiryDate]);  
              //print($objPointTransaction);     
        }
        return true;
    }
    public function updatePlanPointsRevoked($intUserID,$arrTransPointIDs){
        foreach($arrTransactionID as $obj){
            $objPointTransaction =  DB::table('pointtransactions')->where('user_id', $intUserID)->Where('id',$obj->pkPointsTransactionID)->update(['revoked' => 1]);         
        }
        return true;
    }

    public function updateGroupPointTrans($arrMainTransactionID){
        //print_r($arrMainTransactionID);
        
        $arrTransactionID = $arrMainTransactionID;
         
        $updateResult =  DB::table('pointtransactions')->whereIn('id',$arrTransactionID)->update(['revoked' => 1]); 

        return $updateResult;
    }
}

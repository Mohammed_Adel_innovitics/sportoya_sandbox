<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Filesystem\Filesystem;

class Uploader extends Model
{
    //
    public function uploadFileOfType($file,$file_name,$type)
    {
    	switch ($type) {

    		case 'P':
	
						$filePath = '/users/' . $file_name;
						
    			break;
    		case 'S':
    					
						$filePath = '/sessions/Mobile/' . $file_name;

    			break;
    		default:
    			break;
    	}

    	$s3 = \Storage::disk('s3');
    	$response = $s3->put($filePath, file_get_contents($file), 'public');

    	return $response;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionType extends Model
{
    protected $table = 'transactiontypes';

    protected $fillable = ['id','fldtransactiontypename','updated_at','created_at'];

    public function getTarnsactionID($strTransactionTypeName){
    	$intTransactionTypeID = TransactionType::select('id')->where('fldtransactiontypename',$strTransactionTypeName)->first();
        return $intTransactionTypeID['id'];
    }
    


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class District extends Model
{
    //
    protected $table = 'districts';

	protected $fillable = ['id','flddistrictname','city_id','updated_at','created_at'];


    public function listDistrict(){
    	$arrDistrict = $this->get();
    	return $arrDistrict;
    }

    public function listDistrictAttachedToCity($intCityID){
        
    	$arrDistrict = District::select('id','flddistrictname','city_id')->where('city_id',$intCityID)->paginate(20);
    	return $arrDistrict;
    }


    public function deleteDistrict($intDistrictID){
        $objDistrict = DB::table('districts')->where('id',$intDistrictID)->delete();
        return $objDistrict;
    }
    /*public function updateDistrict($intDistrictID,$strDistrictName,$intCityID){
        $objDistrict =  DB::table('districts')->where('id', $intDistrictID)->update(['flddistrictname' => $strDistrictName,'city_id'=>$intCityID]);
        return $objDistrict;
    }*/

    public function City(){
        return $this->belongsTo('App\City','city_id','id');      
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    protected $table = 'promocodes';

    protected $fillable = ['id','fldpromocodepoints','session_id','fldpromocode','created_at','updated_at','fldpromocodePrice'];

    public function listPromoCode(){
        $arrPromoCode = $this->with('Session')->get();
        return $arrPromoCode;
    }
    public function getPromoCode($strPromoCode,$intSessionID){
        //dd($strPromoCode);die;
        $objPromoCode = PromoCode::select('id','fldpromocodepoints','session_id','fldpromocode','created_at','updated_at','fldpromocodePrice')->where('fldpromocode',$strPromoCode) ->whereIn('session_id', [$intSessionID,-1])->first();
       
        return $objPromoCode;
    }


    public function getPromoCodeByID($intPromoCode){ 
        //dd($strRegisterPromo);
        $objPromoCode = PromoCode::select('id','fldpromocodepoints','session_id','fldpromocode','created_at','updated_at','fldpromocodePrice')->where('id',$intPromoCode)->first();
        //dd($arrSession->toSql());
        return $objPromoCode;
    }

    public function deletePromoCode($intPromoCodeID){
        $objPromoCode = PromoCode::select('id','fldpromocodepoints','session_id','fldpromocode','created_at','updated_at')->where('id',$intPromoCodeID)->delete();
        return $objPromoCode;
    }
    public function updatePromoCode($intPromoCode,$strPromoCodePoints,$intSessionID,$strPromoCode){
        $objPromoCode =  DB::table('promocodes')->where('id',$intPromoCode)->update(['fldpromocodepoints' => $strPromoCodePoints,'session_id'=>$intSessionID,'fldpromocode'=>$strPromoCode]);
        return $objPromoCode;
    }

   public function Session(){
        return $this->belongsTo('App\Session','session_id','id');   
    }



}

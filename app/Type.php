<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'types';
	
	protected $fillable = ['id','fldtypename','created_at','updated_at'];

	public function getTypeID($strTypeName){

		$objType = Type::select('id')->where('fldtypename',$strTypeName)->first();
		
        return $objType['id'];
	}
}

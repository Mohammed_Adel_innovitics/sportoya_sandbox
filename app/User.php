<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends \TCG\Voyager\Models\User
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'name','email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getUserEmail($intUserID){
        //dd($intUserID);
        $user = User::select('email')->where('id',$intUserID)->first();
        return $user['email'];
    }

     public function getUserId($email){
        //dd($intUserID);
        $user = User::select('id')->where('email',$email)->first();
        return $user['id'];
    }

    public function getUser($intUserID){
        $user = $this->where('id',$intUserID)->first();
        return $user;
    }
    
}

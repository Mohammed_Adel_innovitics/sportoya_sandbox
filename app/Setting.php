<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'tbl_setting';

    protected $fillable = ['pkSettingID','fldSettingName','fldSettingDescription','fldIsEnabled','fldValue','updated_at','created_at'];


    public function checkRegPromoCodeEnable(){
    	$IsEnable = Setting::select('fldIsEnabled')->where('fldSettingName','RegisterPromo')->first();
    	/*print($IsEnable['fldIsEnabled']);
    	die;*/	
    	return $IsEnable['fldIsEnabled'];
    }

    public function getInvitationPoint(){
    	$Points = Setting::select('fldValue')->where('fldSettingName','InviteFriendPoints')->first();
    	/*print($IsEnable['fldIsEnabled']);
    	die;*/	
    	return $Points['fldValue'];

    }
    public function getMediaUrl(){
        $url = Setting::select('fldValue')->where('fldSettingName','MediaUrl')->first();
        
        /*print($IsEnable['fldIsEnabled']);
        die;*/  
        return $url['fldValue'];

    }

    static function getValue($strSettingName){
        $value = Setting::select('fldValue')->where('fldSettingName',$strSettingName)->first();
        
        /*print($IsEnable['fldIsEnabled']);
         die;*/
        return $value ['fldValue'];
    }
}

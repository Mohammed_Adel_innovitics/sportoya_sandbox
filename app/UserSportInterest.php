<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSportInterest extends Model
{
  
    protected $table = 'userinterestsports';

	protected $fillable = ['id','sport_id','user_id','updated_at','created_at'];

	public function getUserSportIDs($intUserID){
		$arrSportIDs = UserSportInterest::select('sport_id')->where('user_id',$intUserID)->get();

		$arrSportIDs1 = array();
		foreach($arrSportIDs as $obj){
			$arrSportIDs1[] = $obj['sport_id'];
		}
		return $arrSportIDs1;

	}

	public function deleteUserSportInterest($intUserID){
		$result = UserSportInterest::select('sport_id')->where('user_id',$intUserID)->delete();
		if($result){
			return true;
		}
		return false;
	}
	



}

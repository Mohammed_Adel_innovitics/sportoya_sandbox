<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class UserDetails extends Model
{
    protected $table = 'userdetails';
    
    protected $fillable = ['id','user_id','fldfullname','fldgender','fldbirthdate','city_id','fkFBID','plan_id','regpromo_id','created_at','updated_at','fldemergencyname','flduserphoto'];
    
    public function updateUserProfile($intUserID,$strFullName,$strGender,$strBirthDate,$intCityID,$strEmergencyName,$strUserPhoto,$intEmergencyNumber){
        if($strUserPhoto !== ''){
            $objUserDetails =  DB::table('userdetails')->where('user_id', $intUserID)->update(['fldfullName' => $strFullName,'fldgender'=>$strGender,'fldbirthdate'=>$strBirthDate,'city_id'=>$intCityID,'fldemergencyname'=>$strEmergencyName,'flduserphoto'=>'users/'.$strUserPhoto]);
            
            
        }else if($strUserPhoto == ''){
            $objUserDetails =  DB::table('userdetails')->where('user_id', $intUserID)->update(['fldfullname' => $strFullName,'fldgender'=>$strGender,'fldbirthdate'=>$strBirthDate,'city_id'=>$intCityID,'fldemergencyname'=>$strEmergencyName]);
            
        }
        
        return $objUserDetails;
        
    }
    
    public function getUserDetailsByID($intUserID){
        //dd($intUserID);
        $objUserDetails = UserDetails::select('id','user_id','fldfullname','fldgender','fldbirthdate','city_id','fkFBID','plan_id','regpromo_id','created_at','updated_at','fldemergencyname','flduserphoto')->where('user_id',$intUserID)->with('Phone')->first();
        $objSetting = new Setting();
        $objUser = new User();
        //dd($objSetting->getMediaUrl());
        if(!strpos($objUserDetails->flduserphoto,'graph')){
            $objUserDetails->flduserphoto = $objSetting->getMediaUrl().$objUserDetails->flduserphoto;
        }
        
        $objUserDetails->email = $objUser->getUserEmail($intUserID);
        return $objUserDetails;
    }
    
    public function checkFBID($fkID){
        $objUserDetails = UserDetails::select('id','user_id','fldfullname','fldgender','fldbirthdate','city_id','fkFBID','plan_id','regpromo_id','created_at','updated_at','fldemergencyname','flduserphoto')->where('fkFBID',$fkID)->with('Phone')->first();
        if($objUserDetails == null){
            return false;
        }else{
            return true;
        }
        
    }
    public function getUserDetailsByFBID($FkID){
        //dd($FkID);
        $objUserDetails = UserDetails::select('id','user_id','fldfullname','fldgender','fldbirthdate','city_id','fkFBID','plan_id','regpromo_id','created_at','updated_at','fldemergencyname','flduserphoto')->where('fkFBID',$FkID)->with('Phone')->first();
        $objSetting = new Setting();
        $objUser = new User();
        //dd($objSetting->getMediaUrl());
        //dd($objUserDetails->flduserphoto);
        //dd(strpos($objUserDetails->flduserphoto,"is"));
        if(!strpos($objUserDetails->flduserphoto,'graph')){
            $objUserDetails->flduserphoto = $objSetting->getMediaUrl().$objUserDetails->flduserphoto;
        }
        
        
        $objUserDetails->email = $objUser->getUserEmail($objUserDetails->user_id);
        return $objUserDetails;
    }
    
    public function getRegPromoID($intUserID){
        
        $objUserDetails = UserDetails::select('regpromo_id')->where('user_id',$intUserID)->first();
        
        //dd($intRegisterPromoID);
        
        return $objUserDetails['regpromo_id'];
    }
    
    public function Phone(){
        
        return $this->hasMany('App\Phone','user_id','user_id');
    }
    
}

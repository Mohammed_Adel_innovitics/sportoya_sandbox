<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Plan extends Model
{
    protected $table = 'plans';

    protected $fillable = ['id','fldplanname','fldplanpoints','fldplanbouns','fldplancostegp','fldfreezeday','fldexpirynumbermonth','fldexpirytext','regpromo_id','updated_at','created_at'];


    public function listPlan(){
    	$arrPlan = $this->where('regpromo_id',-1)->orderBy('fldPlanCostEGP','Asc')->get();
    	return $arrPlan;
    }


    public function loadPlanByID($intPlanID){
        $objPlan = DB::table('plans')->where('id',$intPlanID)->first();
        return $objPlan;
    }

   

    public function getPlanAttachedToRegPromo($intRegPromoID){
        //dd($intRegPromoID);
        $arrPlan = Plan::select('id','fldplanname','fldplanpoints','fldplancostegp','fldfreezeday','fldexpirynumbermonth','fldexpirytext','regpromo_id')->where('regpromo_id',$intRegPromoID)->with('RegisterPromo')->get();

        return $arrPlan;
        
    } 

    public function getPlanAttachedToRegPromoID($intRegPromoID){
        $objPlan = $this->where('regpromo_id',$intRegPromoID)->get();

        return $objPlan;
        
    }

    public function RegisterPromo(){

        return $this->belongsTo('App\RegisterPromo','regpromo_id','id');
    
    }


}

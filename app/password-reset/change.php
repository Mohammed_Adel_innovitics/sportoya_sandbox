<?php


$token = isset($_GET['uid']) ? $_GET['uid'] : 0;

$url= "http://beta.sportoya.com/api/api/checkValidationToken?fldToken=".$token;

$ch = curl_init();
// Disable SSL verification
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// Will return the response, if false it print the response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
curl_setopt($ch, CURLOPT_URL,$url);
// Execute
$result=curl_exec($ch);
// Closing
curl_close($ch);

// Will dump a beauty json :3
$data = json_decode($result, true);

if($data["status"]["code"] == 200)
{
	 
	echo '<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sporotya</title>
 
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container">

	<!-- Main navbar -->
	 <div class="heading" style="
    margin-left: 14%;
    margin-top: 2%;
    margin-bottom: -5%;
">
						<img src="assets/images/logo_demo.png" class="content-group mt-10" alt="" style="width: 190px;">  
					</div> 
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

						
					 

		<!-- Page content -->
		<div class="page-content">


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Password recovery -->
					
					<form action="reset.php"  method="post" enctype="multipart/form-data" >
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class=" "><i class=" "></i></div>
								<h1 class="content-group">Reset Password
								<br/>

								 <small class="display-block">We have logged you in automatically so you can reset your password here. Please type it twice to avoid any typos.</small></h1>
							</div>

							<div class="form-group has-feedback">
								<input type="password" id="password" name="password" class="form-control" placeholder="New Password" required>
								<div class="form-control-feedback">
									<i class="  text-muted"></i>
								</div>

								<input type="hidden" name="token" value="'.$token.'" />

							 <br/>
								<input type="password"id="confirm_password" class="form-control" placeholder="Confirm Password" required>
								<div class="form-control-feedback">
									<i class="  text-muted"></i>
								</div>
							</div>

							<button type="submit" class="btn bg-blue  ">Reset </button>
						</div>
					</form>
					<!-- /password recovery -->


					<!-- Footer -->
					 
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
';
}
else
{
	echo '<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sporotya</title>
 
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container">

	<!-- Main navbar -->
	 <div class="heading" style="
    margin-left: 14%;
    margin-top: 2%;
    margin-bottom: -5%;
">
						<img src="assets/images/logo_demo.png" class="content-group mt-10" alt="" style="width: 190px;">  
					</div> 
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

						
					 

		<!-- Page content -->
		<div class="page-content">


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Password recovery -->
					
					<form action="index.html">
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class=" "><i class=" "></i></div>
								<h1 class="content-group">Reset Password
								<br/>

								 <small class="display-block" style="color: #fd1100;
                                 ">Your password reset link is not valid, or already used.</small></h1>
							</div>

							 

							 
						</div>
					</form>
					<!-- /password recovery -->


					<!-- Footer -->
					 
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
';
}

?>

<script>
     var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
     </script>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sporotya</title>
 
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container">

	 <div class="heading" style="
							margin-left: 14%;
							margin-top: 2%;
							margin-bottom: -5%;
							">
						<img src="assets/images/logo_demo.png" class="content-group mt-10" alt="" style="width: 190px;">  
					</div> 


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Password recovery -->
					<form action="index.html">
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class=" "><i class=" "></i></div>
								<h1 class="content-group">Forgot Password
								<br/>

								<?php

$password = isset($_POST['password']) ? $_POST['password'] : 0;
$token = isset($_POST['token']) ? $_POST['token'] : 0;



$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"http://beta.sportoya.com/api/api/changePassword");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
            "fldPassword=".$password."&fldToken=".$token);

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec ($ch);

curl_close ($ch);


$data = json_decode($server_output, true);

if($data["status"]["code"] == 200) 
{
  echo "<div   >
            <strong> </strong> 
             <small class= 'display-block' style='color:  #000000;text-align: center;
' > Password Changed Sucessfully </small>
            </div></div>";	
}
else
{
  echo "<div   >
            <strong> </strong> 
             <small class= 'display-block' style='color:red;
    text-align: center;' > Please try again </small>
            </div></div>";
}

?>
							</div>

							 
						</div>
					</form>
					<!-- /password recovery -->


					<!-- Footer -->
					 
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>

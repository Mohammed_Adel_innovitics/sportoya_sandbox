<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Postmark\PostmarkClient;
use Postmark\Models\PostmarkAttachment;

class Mailer extends Model
{
    //

    public function send($senderMail,$recipientMail,$templateId,$data){
    	
    	$client = new PostmarkClient(env("POSTMARK_TOKEN",""));
    	//dd($client);
		// Make a request
		$sendResult = $client->sendEmailWithTemplate(
		  $senderMail,
		  $recipientMail, 
		  $templateId, 
		  $data,
		  true, //inline css
		  NULL, //tag
		  true //trackOpens
		);

		if($sendResult->message == 'OK')
		{
			return true;
		}
		
    }

   /* public function sendSES($senderMail,$subject,$recipientMail,$templateId,$data){
    	
    	 try {
		    
		      //send verification code
		      Mail::send(['html' => 'password-reset'], $data,
		                function ($message) use ($email) {
		                   $message->to($recipientMail)
		                   ->from($senderMail) //not sure why I have to add this
		                   ->subject($subject);
      });

      return true;

      } catch (Exception $ex) {
            return false;
      }  
		
    }*/

   
}

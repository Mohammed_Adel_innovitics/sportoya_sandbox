<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvitationCodeTrans extends Model
{
    protected $table = 'invitationcodetransactions';

    protected $fillable = ['id','owner_user_id','consumer_user_id','invitationCode_id','revoked','created_at','updated_at'];


    public function getInvitationCodeTrans($intConsumedUserID){
    	$objInvitationCodeTrans = $this->Where('consumer_user_id',$intConsumedUserID)->where('revoked',0)->first();
    	return  $objInvitationCodeTrans;
    }

    public function revokeInvitationCodeTrans($intConsumedUserID){
    	$result = $this->where('consumer_user_id', $intConsumedUserID)
          ->update(['revoked' =>1]);
        return $result;
    }
}
